<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Anak extends Model
{
    use SoftDeletes;

    protected $dates = ['tglahir_anak'];

    protected $guarded = [];

    // 1:N User
    public function user() {
        return $this->belongsTo('App\User');
    }

    // 1:N Anak
    public function pembuatan() {
        return $this->hasMany('App\Pembuatan');
    }
}
