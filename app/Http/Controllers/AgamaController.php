<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agama;

class AgamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agamas = Agama::all();
        return view('data-agama.index', compact('agamas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'input tidak valid!',
            'size' => 'Kode harus 6 digit!',
        ];
        
        $validasi = $request->validate([
            'nm_agama' => 'Required',
        ], $messages);

        $create = Agama::create([
            'nm_agama' => $request->nm_agama,
        ]);
    
        return redirect('/agama')->with('add', 'Data berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agama = Agama::find($id);
        return view('data-agama.edit', compact('agama'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'input tidak valid!',
        ];
        
        $validasi = $request->validate([
            'nm_agama' => 'Required',
        ], $messages);

        $agama = Agama::where('id', $id)->update([
            'nm_agama' => $request->nm_agama
        ]);

        return redirect('/agama')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agama::destroy($id);
        return redirect('/agama')->with('delete', 'Data berhasil dihapus..');
    }
}
