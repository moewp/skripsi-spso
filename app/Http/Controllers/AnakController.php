<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anak;
use App\Warga;
use App\User;
use Auth;

class AnakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anak = Anak::where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get();
        return view('anak.index', compact('anak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_anak' => 'Required',
            'tplahir_anak' => 'Required',
            'tglahir_anak' => 'Required',
            'jk' => 'Required',
            'anak_ke' => 'Required',
        ], $messages);

        if ($request->hasFile('file_sklrs')) {
            $imageNameSklrs = time().'.'.$request->file_sklrs->extension();
            $request->file_sklrs->move(public_path('images/sklrs'), $imageNameSklrs);
        }

        $create = Anak::create([
            'nm_anak' => $request->nm_anak,
            'user_id' => Auth::id(),
            'tplahir_anak' => $request->tplahir_anak,
            'tglahir_anak' => $request->tglahir_anak,
            'jk' => $request->jk,
            'anak_ke' => $request->anak_ke,
            'file_sklrs' => $imageNameSklrs,
        ]);

        return redirect('dashboardw')->with('add', 'Data anak berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anak = Anak::find($id);
        return view('anak.edit', compact('anak'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];

        $validasi = $request->validate([
            'nm_anak' => 'Required',
            'tplahir_anak' => 'Required',
            'tglahir_anak' => 'Required',
            'jk' => 'Required',
            'anak_ke' => 'Required',
        ], $messages);

        if($files = $request->file('file_sklrs')) {
            $imageNameSklrs = time().'.'.$files->extension();  
            $request->file_sklrs->move(public_path('images/sklrs'), $imageNameSklrs);
        } else {
            $imageNameSklrs = Anak::find($id);
            $imageNameSklrs = $imageNameSklrs->file_sklrs;
        }

        $create = Anak::where('id', $id)->update([
            'nm_anak' => $request->nm_anak,
            'user_id' => Auth::id(),
            'tplahir_anak' => $request->tplahir_anak,
            'tglahir_anak' => $request->tglahir_anak,
            'jk' => $request->jk,
            'anak_ke' => $request->anak_ke,
            'file_sklrs' => $imageNameSklrs,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Anak::destroy($id);
        return redirect('/dashboardw')->with('delete', 'Data anak berhasil dihapus..');
    }
}
