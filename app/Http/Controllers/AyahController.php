<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ayah;
use App\Warga;
use App\User;
use App\Agama;
use App\Pekerjaan;
use Auth;

class AyahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'nm_agama' => Agama::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('ayah.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!',
            'image' => ':attribute harus berupa jpeg, jpg, atau png maximal 2mb',
        ];
        
        $validasi = $request->validate([
            'nm_ay' => 'Required',
            'bin_ay' => 'Required',
            'nik_ay' => 'Required',
            'tplahir_ay' => 'Required',
            'tglahir_ay' => 'Required',
            'kwn_ay' => 'Required',
            'alamat_ay' => 'Required',
            'rt_ay' => 'Required|numeric',
            'rw_ay' => 'Required|numeric',
            'file_ktpay' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ], $messages);

        if ($request->hasFile('file_ktpay')) {
            $imageNameKtpay = time().'.'.$request->file_ktpay->extension();
            $request->file_ktpay->move(public_path('images/ktpay'), $imageNameKtpay);
        }

        $create = Ayah::create([
            'nm_ay' => $request->nm_ay,
            'bin_ay' => $request->bin_ay,
            'nik_ay' => $request->nik_ay,
            'tplahir_ay' => $request->tplahir_ay,
            'tglahir_ay' => $request->tglahir_ay,
            'kwn_ay' => $request->kwn_ay,
            'user_id' => Auth::id(),
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'alamat_ay' => $request->alamat_ay,
            'rt_ay' => $request->rt_ay,
            'rw_ay' => $request->rw_ay,
            'file_ktpay' => $imageNameKtpay,
        ]);

        return redirect('dashboardw')->with('add', 'Data ayah berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ayah = Ayah::find($id); //pake Eloquent
        return view('ayah.index', compact('ayah'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'ayah' => Ayah::find($id),
            'nm_agama' => Agama::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('ayah.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'min' => 'Mohon masukan :attribute dengan benar!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_ay' => 'Required',
            'bin_ay' => 'Required',
            'nik_ay' => 'Required',
            'tplahir_ay' => 'Required',
            'tglahir_ay' => 'Required',
            'kwn_ay' => 'Required',
            'alamat_ay' => 'Required',
            'rt_ay' => 'Required|numeric',
            'rw_ay' => 'Required|numeric',
            'file_ktpay' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ], $messages);

        if($files = $request->file('file_ktpay')) {
            $imageNameKtpay = time().'.'.$files->extension();  
            $request->file_ktpay->move(public_path('images/ktpay'), $imageNameKtpay);
        } else {
            $imageNameKtpay = Ayah::find($id);
            $imageNameKtpay = $imageNameKtpay->file_ktpay;
        }

        $ayah = Ayah::where('id', $id)->update([
            'nm_ay' => $request->nm_ay,
            'nik_ay' => $request->nik_ay,
            'bin_ay' => $request->bin_ay,
            'tplahir_ay' => $request->tplahir_ay,
            'tglahir_ay' => $request->tglahir_ay,
            'kwn_ay' => $request->kwn_ay,
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'alamat_ay' => $request->alamat_ay,
            'rt_ay' => $request->rt_ay,
            'rw_ay' => $request->rw_ay,
            'file_ktpay' => $imageNameKtpay,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ayah::destroy($id);
        return redirect('/ayah')->with('delete', 'Data ayah berhasil dihapus..');
    }
}
