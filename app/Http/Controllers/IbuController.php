<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ibu;
use App\Warga;
use App\User;
use App\Agama;
use App\Pekerjaan;
use Auth;

class IbuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $ibu = Ibu::where('warga_id', Auth::id())->orderBy('updated_at', 'DESC')->get();
		// return view('ibu.index', compact('ibu'));

        // $ibu = Ibu::find($id); //pake Eloquent
        // return view('ibu.index', compact('ibu'));

        // return view('ibu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'nm_agama' => Agama::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        
        // $id = Auth::id();
        // $warga = Warga::where('user_id', Auth::id());
        // $warga = Warga::find('id')->where('user_id', Auth::id());
        // $warga = Warga::find($id);
        // $warga = Warga::where('user_id', Auth::id())->value('id');
        // return view('ibu.create', compact('warga'))->with($data);
        return view('ibu.create', $data);
        // return view('ibu.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $warga = Warga::find($request->warga_id);

        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_ib' => 'Required',
            'nik_ib' => 'Required',
            'bin_ib' => 'Required',
            'tplahir_ib' => 'Required',
            'tglahir_ib' => 'Required',
            'kwn_ib' => 'Required',
            'nm_pkj' => 'Required|integer',
            'nm_agama' => 'Required',
            'alamat_ib' => 'Required',
            'rt_ib' => 'Required|numeric',
            'rw_ib' => 'Required|numeric',
        ], $messages);

        if ($request->hasFile('file_ktpib')) {
            $imageNameKtpib = time().'.'.$request->file_ktpib->extension();
            $request->file_ktpib->move(public_path('images/ktpib'), $imageNameKtpib);
        }

        $create = Ibu::create([
            'nm_ib' => $request->nm_ib,
            'nik_ib' => $request->nik_ib,
            'bin_ib' => $request->bin_ib,
            'tplahir_ib' => $request->tplahir_ib,
            'tglahir_ib' => $request->tglahir_ib,
            'kwn_ib' => $request->kwn_ib,
            'user_id' => Auth::id(),
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'alamat_ib' => $request->alamat_ib,
            'rt_ib' => $request->rt_ib,
            'rw_ib' => $request->rw_ib,
            'file_ktpib' => $imageNameKtpib,
        ]);

        return redirect('dashboardw')->with('add', 'Data ibu berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ibu = Ibu::find($id); //pake Eloquent
        return view('ibu.index', compact('ibu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'ibu' => Ibu::find($id),
            'nm_agama' => Agama::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('ibu.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $messages = [
            'required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'min' => 'Mohon masukan :attribute dengan benar!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_ib' => 'Required',
            'nik_ib' => 'Required',
            'bin_ib' => 'Required',
            'tplahir_ib' => 'Required',
            'tglahir_ib' => 'Required',
            'kwn_ib' => 'Required',
            'nm_pkj' => 'Required|integer',
            'nm_agama' => 'Required',
            'alamat_ib' => 'Required',
            'rt_ib' => 'Required|numeric',
            'rw_ib' => 'Required|numeric',
        ], $messages);

        if($files = $request->file('file_ktpib')) {
            $imageNameKtpib = time().'.'.$files->extension();  
            $request->file_ktpib->move(public_path('images/ktpib'), $imageNameKtpib);
        } else {
            $imageNameKtpib = Ibu::find($id);
            $imageNameKtpib = $imageNameKtpib->file_ktpib;
        }

        $ibu = Ibu::where('id', $id)->update([
            'nm_ib' => $request->nm_ib,
            'nik_ib' => $request->nik_ib,
            'bin_ib' => $request->bin_ib,
            'tplahir_ib' => $request->tplahir_ib,
            'tglahir_ib' => $request->tglahir_ib,
            'kwn_ib' => $request->kwn_ib,
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'alamat_ib' => $request->alamat_ib,
            'rt_ib' => $request->rt_ib,
            'rw_ib' => $request->rw_ib,
            'file_ktpib' => $imageNameKtpib,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ibu::destroy($id);
        return redirect('/ibu')->with('delete', 'Data ibu berhasil dihapus..');
    }
}
