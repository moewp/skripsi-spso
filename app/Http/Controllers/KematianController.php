<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kematian;
use App\Warga;
use App\User;
use App\Agama;
use Auth;

class KematianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'nm_agama' => Agama::all(),
        ];
        $kematian = Kematian::where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get();
        return view('kematian.index', $data, compact('kematian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_alm' => 'Required',
            'tplahir_alm' => 'Required',
            'tglahir_alm' => 'Required',
            'alamat_alm' => 'Required',
            'rt_alm' => 'Required',
            'rw_alm' => 'Required',
            'jk' => 'Required',
            'tpmgl_alm' => 'Required',
            'tgmgl_alm' => 'Required',
            'sebab_mgl' => 'Required',
            'hubungan' => 'Required',
        ], $messages);

        if ($request->hasFile('file_skmrs')) {
            $imageNameSkmrs = time().'.'.$request->file_skmrs->extension();
            $request->file_skmrs->move(public_path('images/skmrs'), $imageNameSkmrs);
        }

        $create = Kematian::create([
            'user_id' => Auth::id(),
            'nm_alm' => $request->nm_alm,
            'nik_alm' => $request->nik_alm,
            'tplahir_alm' => $request->tplahir_alm,
            'tglahir_alm' => $request->tglahir_alm,
            'agama_id' => $request->nm_agama,
            'alamat_alm' => $request->alamat_alm,
            'rt_alm' => $request->rt_alm,
            'rw_alm' => $request->rw_alm,
            'jk' => $request->jk,
            'tpmgl_alm' => $request->tpmgl_alm,
            'tgmgl_alm' => $request->tgmgl_alm,
            'sebab_mgl' => $request->sebab_mgl,
            'hubungan' => $request->hubungan,
            'file_skmrs' => $imageNameSkmrs,
        ]);

        return redirect('dashboardw')->with('add', 'Data kematian berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kematian = Kematian::find($id);
        return view('kematian.edit', compact('kematian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!',
            'image' => ':attribute harus berupa jpeg, jpg, atau png maximal 2mb',
        ];
        
        $request->validate([
            'hubungan' => 'Required',
            'nm_alm' => 'Required',
            'nik_alm' => 'Required',
            'tplahir_alm' => 'Required',
            'tglahir_alm' => 'Required',
            'jk' => 'Required',
            'alamat_alm' => 'Required',
            'rt_alm' => 'Required',
            'rw_alm' => 'Required',
            'tpmgl_alm' => 'Required',
            'tgmgl_alm' => 'Required',
            'sebab_mgl' => 'Required',
            'file_skmrs' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ], $messages);

        if($files = $request->file('file_skmrs')) {
            $imageNameSkmrs = time().'.'.$files->extension();
            $request->file_skmrs->move(public_path('images/skmrs'), $imageNameSkmrs);
        } else {
            $imageNameSkmrs = Kematian::find($id);
            $imageNameSkmrs = $imageNameSkmrs->file_skmrs;
        }

        $create = Kematian::where('id', $id)->update([
            'user_id' => Auth::id(),
            'nm_alm' => $request->nm_alm,
            'nik_alm' => $request->nik_alm,
            'tplahir_alm' => $request->tplahir_alm,
            'tglahir_alm' => $request->tglahir_alm,
            'agama_id' => $request->nm_agama,
            'alamat_alm' => $request->alamat_alm,
            'rt_alm' => $request->rt_alm,
            'rw_alm' => $request->rw_alm,
            'jk' => $request->jk,
            'tpmgl_alm' => $request->tpmgl_alm,
            'tgmgl_alm' => $request->tgmgl_alm,
            'sebab_mgl' => $request->sebab_mgl,
            'hubungan' => $request->hubungan,
            'file_skmrs' => $imageNameSkmrs,
        ]);

        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kematian::destroy($id);
        return redirect('/dashboardw')->with('delete', 'Data kematian berhasil dihapus..');
    }
}
