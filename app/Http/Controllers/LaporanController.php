<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembuatan;
use App\User;
use App\Warga;
use App\Surat;
use App;
use PDF;

class LaporanController extends Controller
{
    public function index(){
        $data = [
            'user' => User::find(auth()->user()->id),
            'nm_surat' => Surat::orderBy('nm_surat', 'ASC')->get(),
        ];
        return view('laporan.index', $data);
        // return view('laporan.index');
    }

    public function create(){
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $data = [
            'pembuatan' => Pembuatan::orderBy('created_at', 'DESC')->get()
        ];
        $pdf = PDF::loadView('pdf.laporan', $data);
        return $pdf->download('Laporan-pembuatan-surat.pdf');
    }
}
