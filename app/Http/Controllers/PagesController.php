<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warga;
use App\User;
use App\Surat;
use App\Pembuatan;
use Auth;

class PagesController extends Controller
{
    public function landing() {        
        return view('landing');
    }

    public function choose() {
        $surats = Surat::all();
        return view('chooseForm', compact('surats'));
    }

    public function login() {
        return view('forms.login');
    }
    
    public function regis() {
        return view('forms.regis');
    }

    public function dashboard() {
        $wargaCount = Warga::count();
        $pendingBuat = Pembuatan::where('status', '!=', 2)->count();
        $completeBuat = Pembuatan::where('status', 2)->count();
        return view('dashboard', compact('wargaCount', 'pendingBuat', 'completeBuat'));
    }

    public function dashboarda() {
        $pendingBuat = Pembuatan::where('status', '!=', 2)->count();
        $completeBuat = Pembuatan::where('status', 2)->count();
        return view('dashboarda', compact('completeBuat', 'pendingBuat'));
    }

    public function dashboardw() {
        $pendingBuat = Pembuatan::where('status', '!=', 2)->where('user_id', Auth::id())->count();
        $riwayatBuat = Pembuatan::where('status', 2)->where('user_id', Auth::id())->count();
        return view('dashboardw', compact('pendingBuat', 'riwayatBuat'));
    }
}
