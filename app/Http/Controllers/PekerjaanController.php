<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pekerjaan;

class PekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pekerjaans = Pekerjaan::all();
        return view('data-pekerjaan.index', compact('pekerjaans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'input tidak valid!',
            'size' => 'Kode harus 6 digit!',
        ];
        
        $validasi = $request->validate([
            'nm_pkj' => 'Required',            
        ], $messages);

        $create = Pekerjaan::create([
            'nm_pkj' => $request->nm_pkj,
        ]);
    
        return redirect('/pkj')->with('add', 'Data berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        return view('data-pekerjaan.edit', compact('pekerjaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'input tidak valid!',
        ];
        
        $validasi = $request->validate([
            'nm_pkj' => 'Required',            
        ], $messages);

        $pekerjaan = Pekerjaan::where('id', $id)->update([
            'nm_pkj' => $request->nm_pkj,
        ]);

        return redirect('/pkj')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pekerjaan::destroy($id);
        return redirect('/pkj')->with('delete', 'Data berhasil dihapus..');
    }
}
