<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warga;
use App\User;
use App\Surat;
use App\Pembuatan;
use App\Sutri;
use App\Ayah;
use App\Anak;
use App\Agama;
use App\Kematian;
use Carbon\Carbon;
use Auth;
use PDF;

class PembuatanController extends Controller
{
    //admin pending proses pembuatan
    public function indexPending()
    {
        $buats = Pembuatan::where('status', '!=' , 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.pending', compact('buats'));
    }

    //admin complete proses pembuatan
    //complete skbd
    public function completeSkbd() {
        $buat = Pembuatan::where('surat_id', 1)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skbd', compact('buat'));
    }
    //complete pktp
    public function completePktp() {
        $buat = Pembuatan::where('surat_id', 2)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.pktp', compact('buat'));
    }
    //complete skl
    public function completeSkl() {
        $buat = Pembuatan::where('surat_id', 3)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skl', compact('buat'));
    }
    //complete sksm
    public function completeSksm() {
        $buat = Pembuatan::where('surat_id', 4)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.sksm', compact('buat'));
    }
    //complete skksi
    public function completeSkksi() {
        $buat = Pembuatan::where('surat_id', 5)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skksi', compact('buat'));
    }
    //complete skk
    public function completeSkk() {
        $buat = Pembuatan::where('surat_id', 6)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skk', compact('buat'));
    }
    //complete skbm
    public function completeSkbm() {
        $buat = Pembuatan::where('surat_id', 7)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skbm', compact('buat'));
    }
    //complete skd
    public function completeSkd() {
        $buat = Pembuatan::where('surat_id', 8)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skd', compact('buat'));
    }
    //complete spp
    public function completeSpp() {
        $buat = Pembuatan::where('surat_id', 9)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.spp', compact('buat'));
    }
    //complete sku
    public function completeSku() {
        $buat = Pembuatan::where('surat_id', 10)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.sku', compact('buat'));
    }
    //complete skdu
    public function completeSkdu() {
        $buat = Pembuatan::where('surat_id', 11)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skdu', compact('buat'));
    }
    //complete skdmt
    public function completeSkdmt() {
        $buat = Pembuatan::where('surat_id', 12)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.skdmt', compact('buat'));
    }
    //complete sktm
    public function completeSktm() {
        $buat = Pembuatan::where('surat_id', 13)->where('status', 2)->orderBy('created_at', 'DESC')->get();
        return view('pembuatan.complete.sktm', compact('buat'));
    }

    //cetak generalisir laporan
    public function completeCetak()
    {
        $cetak = Pembuatan::where('status', 2)->orderBy('created_at', 'ASC')->get();
        return view('pdf.laporan', compact('cetak'));
    }

    //cetak skbd
    public function cetakSkbd(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skbd', compact('cetak', 'date'));
    }
    //cetak pktp
    public function cetakPktp(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.pktp', compact('cetak', 'date'));
    }
    //cetak skl
    public function cetakSkl(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skl', compact('cetak', 'date'));
    }
    //cetak sksm
    public function cetakSksm(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.sksm', compact('cetak', 'date'));
    }
    //cetak skksi
    public function cetakSkksi(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skksi', compact('cetak', 'date'));
    }
    //cetak skk
    public function cetakSkk(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skk', compact('cetak', 'date'));
    }
    //cetak skbm
    public function cetakSkbm(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skbm', compact('cetak', 'date'));
    }
    //cetak skd
    public function cetakSkd(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skd', compact('cetak', 'date'));
    }
    //cetak spp
    public function cetakSpp(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.spp', compact('cetak', 'date'));
    }
    //cetak sku
    public function cetakSku(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.sku', compact('cetak', 'date'));
    }
    //cetak skdu
    public function cetakSkdu(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skdu', compact('cetak', 'date'));
    }
    //cetak skdmt
    public function cetakSkdmt(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.skdmt', compact('cetak', 'date'));
    }
    //cetak sktm
    public function cetakSktm(Request $request, $id)
    {
        $cetak = Pembuatan::where('id', $request->id)->get();
        $date = Carbon::now();
        return view('pdf.sktm', compact('cetak', 'date'));
    }

    //warga pilih surat
    public function getBuat($id)
    {
        $surat = Surat::find($id);
        $data = [
            'nm_anak' => Anak::where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get(),
            'nm_alm' => Kematian::where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get(),
        ];
        $date = Carbon::now();
        return view('buatsurat', $data, compact('surat', 'date'));
    }

    //warga pilih surat
    public function postBuat(Request $request)
    {
        //tes
        $surat = Surat::find($request->surat_id);
        $imageNameSklrs = $imageNamePp = $imageNameKkps = $imageNameKtpay = $imageNameKtpib = $imageNameSkmt = $imageNameBn = $imageNameKtp = $imageNameSkmrs = NULL;
        // dd($request->all());
        // $messages = [
        //     'Required' => 'input tidak valid!',
        // ];
        
        // $validasi = $request->validate([            
        //     'alamat_dom' => 'Required',
        //     'file_kk' => 'Required|image|mimes:jpeg,png,jpg,pdf|max:2048',
        //     'file_kkps' => 'Required|image|mimes:jpeg,png,jpg,pdf|max:2048',
        //     'file_ktp' => 'Required|image|mimes:jpeg,png,jpg,pdf|max:2048'
        // ], $messages);
        
		// $imageNameKk = time().'.'.$request->file_kk->extension();  
        // $request->file_kk->move(public_path('images/kk'), $imageNameKk);

        $imageNameRtrw = time().'.'.$request->file_rtrw->extension();
        $request->file_rtrw->move(public_path('images/rtrw'), $imageNameRtrw);

        // tes pakai has file
        if ($request->hasFile('file_sklrs')) {
            $imageNameSklrs = time().'.'.$request->file_sklrs->extension();
            $request->file_sklrs->move(public_path('images/sklrs'), $imageNameSklrs);
        }

        if ($request->hasFile('file_pp')) {
            $imageNamePp = time().'.'.$request->file_pp->extension();
            $request->file_pp->move(public_path('images/pp'), $imageNamePp);
        }

        if ($request->hasFile('file_kkps')) {
            $imageNameKkps = time().'.'.$request->file_kkps->extension();
            $request->file_kkps->move(public_path('images/kkps'), $imageNameKkps);
        }

        if ($request->hasFile('file_ktpay')) {
            $imageNameKtpay = time().'.'.$request->file_ktpay->extension();
            $request->file_ktpay->move(public_path('images/ktpay'), $imageNameKtpay);
        }

        if ($request->hasFile('file_ktpib')) {
            $imageNameKtpib = time().'.'.$request->file_ktpib->extension();
            $request->file_ktpib->move(public_path('images/ktpib'), $imageNameKtpib);
        }

        if ($request->hasFile('file_skmt')) {
            $imageNameSkmt = time().'.'.$request->file_skmt->extension();
            $request->file_skmt->move(public_path('images/skmt'), $imageNameSkmt);
        }

        if ($request->hasFile('file_bn')) {
            $imageNameBn = time().'.'.$request->file_bn->extension();  
            $request->file_bn->move(public_path('images/bn'), $imageNameBn);
        }

        if ($request->hasFile('file_ktp')) {
            $imageNameKtp = time().'.'.$request->file_ktp->extension();
            $request->file_ktp->move(public_path('images/ktp'), $imageNameKtp);
        }

        if ($request->hasFile('file_skmrs')) {
            $imageNameSkmrs = time().'.'.$request->file_skmrs->extension();
            $request->file_skmrs->move(public_path('images/skmrs'), $imageNameSkmrs);
        }
        // $warga = Warga::find($request->warga_id);

        Pembuatan::create([
            'surat_id' => $request->surat_id,
            'anak_id' => $request->nm_anak,
            'mati_id' => $request->nm_alm,
            'user_id' => Auth::id(),
            'status' => $request->status = 0,
            'kpr_sktm' => $request->kpr_sktm,
            'alamat_dom' => $request->alamat_dom,
            'mantan_sutri' => $request->mantan_sutri,
            'jns_ush' => $request->jns_ush,
            'alamat_ush' => $request->alamat_ush,
            // 'file_kk' => $imageNameKk,
            'file_rtrw' => $imageNameRtrw,
            'file_sklrs' => $imageNameSklrs,
            'file_pp' => $imageNamePp,
            'file_kkps' => $imageNameKkps,
            'file_ktpay' => $imageNameKtpay,
            'file_ktpib' => $imageNameKtpib,
            'file_skmt' => $imageNameSkmt,
            'file_bn' => $imageNameBn,
            'file_ktp' => $imageNameKtp,
            'file_skmrs' => $imageNameSkmrs,
        ]);

		return redirect()->route('listBuat')->with('mohon', 'Pembuatan diterima silakan tunggu konfirmasi yaa..');
	}

    //warga riwayat
    public function riwayatBuat() 
    {
		$buats = Pembuatan::where('status', 2)->where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get();
		return view('pembuatan.riwayat', compact('buats'));
	}

    //warga daftar buat
    public function listBuat() 
    {
		$buats = Pembuatan::where('status', '!=', 2)->where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get();
		return view('pembuatan.daftar', compact('buats'));
	}

    //aktor detail pembuatan surat
    public function detailBuat($id) {
		$buat = Pembuatan::find($id);
		return view('pembuatan.detail', compact('buat'));
	}

    //warga cancel pembuatan surat
	public function cancelBuat(Request $request, $id) {
		$buat = Pembuatan::destroy($request->id);
		return redirect()->route('listBuat')->with('message', 'Pembuatan surat telah dibatalkan..');
	}

    //admin verif pembuatan
    public function verify(Request $request, $id) {
        if ($request->method == 'complete') {
            return $this->completetOrder($id);
        } elseif ($request->method == 'accept') {
            return $this->acceptOrder($id);
        } else  {
            return $this->cancelOrder($id);
        }
    }

    //admin verif completed
    private function completetOrder($id) {
        $order =Pembuatan::find($id);
        $order->status = 2;
        $order->save();
        return redirect()->route('indexPending')->with('message', 'Order has mark as completly order. Go to the Completly Order Page to see that.');
    }

    //admin verif acc
    private function acceptOrder($id) {
        $order =Pembuatan::find($id);
        $order->status = 1;
        $order->save();
        return redirect()->route('indexPending')->with('message', 'Order has been accepted.');
    }

    //admin verif tolak
    private function cancelOrder($id) {
        // $order =Pembuatan::destroy($id);
        $order =Pembuatan::find($id);
        $order->status = 3;
        $order->save();
        return redirect()->route('indexPending')->with('message', 'OK. Order has been canceled.');
    }

    //admin complete hapus
    public function remove($id) {
        $order = Pembuatan::destroy($id);
        return redirect()->route('indexPending')->with('delete', 'Data berhasil dihapus.');
    }

}
