<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendidikan;

class PendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pendidikans = Pendidikan::all();
        return view('data-pendidikan.index', compact('pendidikans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'input tidak valid!',
            'size' => 'Kode harus 6 digit!',
        ];
        
        $validasi = $request->validate([
            'nm_pdd' => 'Required',            
        ], $messages);

        $create = Pendidikan::create([
            'nm_pdd' => $request->nm_pdd,
        ]);
    
        return redirect('/pdd')->with('add', 'Data berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pendidikan = Pendidikan::find($id);
        return view('data-pendidikan.edit', compact('pendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'input tidak valid!',
        ];
        
        $validasi = $request->validate([
            'nm_pdd' => 'Required',            
        ], $messages);

        $pekerjaan = Pendidikan::where('id', $id)->update([
            'nm_pdd' => $request->nm_pdd,
        ]);

        return redirect('/pdd')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pendidikan::destroy($id);
        return redirect('/pdd')->with('delete', 'Data berhasil dihapus..');
    }
}
