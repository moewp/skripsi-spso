<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perusahaan;
use App\Warga;
use App\User;
use Auth;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perusahaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_ush' => 'Required',
            'alamat_ush' => 'Required',
            'jns_ush' => 'Required',
            'pj_ush' => 'Required',
            'sb_ush' => 'Required',
            'rt_ush' => 'Required',
            'rw_ush' => 'Required',
            'akta_ush' => 'Required',
            'jml_kry' => 'Required',
        ], $messages);

        if ($request->hasFile('file_pp')) {
            $imageNamePp = time().'.'.$request->file_pp->extension();
            $request->file_pp->move(public_path('images/pp'), $imageNamePp);
        }

        $create = Perusahaan::create([
            'nm_ush' => $request->nm_ush,
            'alamat_ush' => $request->alamat_ush,
            'jns_ush' => $request->jns_ush,
            'pj_ush' => $request->pj_ush,
            'sb_ush' => $request->sb_ush,
            'rt_ush' => $request->rt_ush,
            'rw_ush' => $request->rw_ush,
            'user_id' => Auth::id(),
            'akta_ush' => $request->akta_ush,
            'jml_kry' => $request->jml_kry,
            'file_pp' => $imageNamePp,
        ]);

        return redirect('dashboardw')->with('add', 'Data perusahaan berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $psh = Perusahaan::find($id); //pake Eloquent
        return view('perusahaan.index', compact('psh'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'perusahaan' => Perusahaan::find($id),
        ];
        return view('perusahaan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_ush' => 'Required',
            'alamat_ush' => 'Required',
            'jns_ush' => 'Required',
            'pj_ush' => 'Required',
            'sb_ush' => 'Required',
            'akta_ush' => 'Required',
            'jml_kry' => 'Required',
        ], $messages);

        if($files = $request->file('file_pp')) {
            $imageNamePp = time().'.'.$files->extension();
            $request->file_pp->move(public_path('images/pp'), $imageNamePp);
        } else {
            $imageNamePp = Perusahaan::find($id);
            $imageNamePp = $imageNamePp->file_pp;
        }

        $psh = Perusahaan::where('id', $id)->update([
            'nm_ush' => $request->nm_ush,
            'alamat_ush' => $request->alamat_ush,
            'jns_ush' => $request->jns_ush,
            'pj_ush' => $request->pj_ush,
            'sb_ush' => $request->sb_ush,
            'rt_ush' => $request->rt_ush,
            'rw_ush' => $request->rw_ush,
            'user_id' => Auth::id(),
            'akta_ush' => $request->akta_ush,
            'jml_kry' => $request->jml_kry,
            'file_pp' => $imageNamePp,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Perusahaan::destroy($id);
        return redirect('/dashboardw')->with('delete', 'Data perusahaan berhasil dihapus..');
    }
}
