<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Surat;
use Alert;

class SuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surats = Surat::all();
        return view('data-surat.index', compact('surats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'input tidak valid!',
        ];
        
        $validasi = $request->validate([
            'nm_surat' => 'Required',
            'kd_surat' => 'Required',
        ], $messages);

        $create = Surat::create([
            'nm_surat' => $request->nm_surat,
            'kd_surat' => $request->kd_surat
        ]);
    
        return redirect('/surat')->with('add', 'Data berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $surat = Surat::find($id);
        return view('data-surat.edit', compact('surat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'input tidak valid!',
            'kd_surat' => 'Required|size:6',
        ];
        
        $validasi = $request->validate([
            'nm_surat' => 'Required',
            'kd_surat' => 'Required|size:6',
        ], $messages);

        $surat = Surat::where('id', $id)->update([
            'nm_surat' => $request->nm_surat,
            'kd_surat' => $request->kd_surat
        ]);

        return redirect('/surat')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Surat::destroy($id);
        return redirect('/surat')->with('delete', 'Data berhasil dihapus..');
    }
}
