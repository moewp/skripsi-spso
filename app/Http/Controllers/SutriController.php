<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sutri;
use App\Warga;
use App\User;
use App\Agama;
use App\Pekerjaan;
use Auth;

class SutriController extends Controller
{
    /**
     * Displsutri a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'nm_agama' => Agama::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('sutri.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_sutri' => 'Required',
            'nik_sutri' => 'Required',
            'bin_sutri' => 'Required',
            'tplahir_sutri' => 'Required',
            'tglahir_sutri' => 'Required',
            'kwn_sutri' => 'Required',
            'nm_pkj' => 'Required|integer',
            'nm_agama' => 'Required',
            'alamat_sutri' => 'Required',
            'rt_sutri' => 'Required|numeric',
            'rw_sutri' => 'Required|numeric',
        ], $messages);

        if ($request->hasFile('file_bn')) {
            $imageNameBn = time().'.'.$request->file_bn->extension();
            $request->file_bn->move(public_path('images/bn'), $imageNameBn);
        }

        if ($request->hasFile('file_kkps')) {
            $imageNameKkps = time().'.'.$request->file_kkps->extension();
            $request->file_kkps->move(public_path('images/kkps'), $imageNameKkps);
        }

        if ($request->hasFile('file_ktpps')) {
            $imageNameKtpps = time().'.'.$request->file_ktpps->extension();
            $request->file_ktpps->move(public_path('images/ktpps'), $imageNameKtpps);
        }

        if ($request->hasFile('file_skmt')) {
            $imageNameSkmt = time().'.'.$request->file_skmt->extension();
            $request->file_skmt->move(public_path('images/skmt'), $imageNameSkmt);
        }

        $create = Sutri::create([
            'nm_sutri' => $request->nm_sutri,
            'nik_sutri' => $request->nik_sutri,
            'bin_sutri' => $request->bin_sutri,
            'tplahir_sutri' => $request->tplahir_sutri,
            'tglahir_sutri' => $request->tglahir_sutri,
            'kwn_sutri' => $request->kwn_sutri,
            'user_id' => Auth::id(),
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'alamat_sutri' => $request->alamat_sutri,
            'rt_sutri' => $request->rt_sutri,
            'rw_sutri' => $request->rw_sutri,
            'jk' => $request->jk,
            'tgl_nkh' => $request->tgl_nkh,
            'tpmgl_sutri' => $request->tpmgl_sutri,
            'tgmgl_sutri' => $request->tgmgl_sutri,
            'file_bn' => $imageNameBn,
            'file_kkps' => $imageNameKkps,
            'file_ktpps' => $imageNameKtpps,
            'file_skmt' => $imageNameSkmt,
        ]);

        return redirect('dashboardw')->with('add', 'Data pasanganmu berhasil ditambahkan..');
    }

    /**
     * Displsutri the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sutri = Sutri::find($id); //pake Eloquent
        return view('sutri.index', compact('sutri'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'sutri' => Sutri::find($id),
            'nm_agama' => Agama::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('sutri.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'min' => 'Mohon masukan :attribute dengan benar!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_sutri' => 'Required',
            'nik_sutri' => 'Required',
            'bin_sutri' => 'Required',
            'tplahir_sutri' => 'Required',
            'tglahir_sutri' => 'Required',
            'kwn_sutri' => 'Required',
            'nm_pkj' => 'Required|integer',
            'nm_agama' => 'Required',
            'alamat_sutri' => 'Required',
            'rt_sutri' => 'Required|numeric',
            'rw_sutri' => 'Required|numeric',
        ], $messages);

        if($files = $request->file('file_bn')) {
            $imageNameBn = time().'.'.$files->extension();
            $request->file_bn->move(public_path('images/bn'), $imageNameBn);
        } else {
            $imageNameBn = Sutri::find($id);
            $imageNameBn = $imageNameBn->file_bn;
        }

        if($files = $request->file('file_kkps')) {
            $imageNameKkps = time().'.'.$files->extension();  
            $request->file_kkps->move(public_path('images/kkps'), $imageNameKkps);
        } else {
            $imageNameKkps = Sutri::find($id);
            $imageNameKkps = $imageNameKkps->file_kkps;
        }

        if($files = $request->file('file_ktpps')) {
            $imageNameKtpps = time().'.'.$files->extension();  
            $request->file_ktpps->move(public_path('images/ktpps'), $imageNameKtpps);
        } else {
            $imageNameKtpps = Sutri::find($id);
            $imageNameKtpps = $imageNameKtpps->file_ktpps;
        }

        if($files = $request->file('file_skmt')) {
            $imageNameSkmt = time().'.'.$files->extension();  
            $request->file_skmt->move(public_path('images/skmt'), $imageNameSkmt);
        } else {
            $imageNameSkmt = Sutri::find($id);
            $imageNameSkmt = $imageNameSkmt->file_skmt;
        }

        $sutri = Sutri::where('id', $id)->update([
            'nm_sutri' => $request->nm_sutri,
            'nik_sutri' => $request->nik_sutri,
            'bin_sutri' => $request->bin_sutri,
            'tplahir_sutri' => $request->tplahir_sutri,
            'tglahir_sutri' => $request->tglahir_sutri,
            'kwn_sutri' => $request->kwn_sutri,
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'alamat_sutri' => $request->alamat_sutri,
            'rt_sutri' => $request->rt_sutri,
            'rw_sutri' => $request->rw_sutri,
            'jk' => $request->jk,
            'tgl_nkh' => $request->tgl_nkh,
            'tpmgl_sutri' => $request->tpmgl_sutri,
            'tgmgl_sutri' => $request->tgmgl_sutri,
            'file_bn' => $imageNameBn,
            'file_kkps' => $imageNameKkps,
            'file_ktpps' => $imageNameKtpps,
            'file_skmt' => $imageNameSkmt,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sutri::destroy($id);
        return redirect('/sutri')->with('delete', 'Data sutri berhasil dihapus..');
    }
}
