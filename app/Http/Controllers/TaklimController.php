<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Taklim;
use App\Warga;
use App\User;
use Auth;

class TaklimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('taklim.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        // $validasi = $request->validate([
        //     'nm_mt' => 'Required',
        //     'alamat_mt' => 'Required',
        //     'rt_mt' => 'Required',
        //     'rw_mt' => 'Required',
        //     'jns_mt' => 'Required',
        //     'sb_mt' => 'Required',
        //     'akta_mt' => 'Required'
        // ], $messages);

        $create = Taklim::create([
            'nm_mt' => $request->nm_mt,
            'alamat_mt' => $request->alamat_mt,
            'rt_mt' => $request->rt_mt,
            'rw_mt' => $request->rw_mt,
            'jns_mt' => $request->jns_mt,
            'sb_mt' => $request->sb_mt,
            'user_id' => Auth::id(),
            'akta_mt' => $request->akta_mt,
        ]);

        return redirect('dashboardw')->with('add', 'Data majlis taklim berhasil ditambahkan..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mjt = Taklim::find($id); //pake Eloquent
        return view('taklim.index', compact('mjt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'taklim' => Taklim::find($id),
        ];
        return view('taklim.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!'
        ];
        
        $validasi = $request->validate([
            'nm_mt' => 'Required',
            'alamat_mt' => 'Required',
            'rt_mt' => 'Required',
            'rw_mt' => 'Required',
            'jns_mt' => 'Required',
            'sb_mt' => 'Required',
            'akta_mt' => 'Required',
        ], $messages);

        $psh = Taklim::where('id', $id)->update([
            'nm_mt' => $request->nm_mt,
            'alamat_mt' => $request->alamat_mt,
            'rt_mt' => $request->rt_mt,
            'rw_mt' => $request->rw_mt,
            'jns_mt' => $request->jns_mt,
            'sb_mt' => $request->sb_mt,
            'user_id' => Auth::id(),
            'akta_mt' => $request->akta_mt,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Taklim::destroy($id);
        return redirect('/dashboardw')->with('delete', 'Data perusahaan berhasil dihapus..');
    }
}
