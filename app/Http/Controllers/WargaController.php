<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warga;
use App\User;
use App\Agama;
use App\Pendidikan;
use App\Pekerjaan;
use App\Pembuatan;
use Auth;

class WargaController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth');
    // }

    public function indexWarga()
    {
        $wargas = Warga::all();
        return view('warga.indexwarga', compact('wargas'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $wargas = Warga::where('user_id', Auth::id())->first();
        // $buats = Pembuatan::where('status', 2)->where('user_id', Auth::id())->orderBy('updated_at', 'DESC')->get();
        // return view('warga.index', compact('wargas', 'buats')); 1
        // return view('landing'); 2
        $wargas = Warga::all();
        return view('warga.index', compact('wargas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'nm_agama' => Agama::all(),
            'nm_pdd' => Pendidikan::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('warga.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $messages = [
            'Required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'integer' => ':attribute harus berupa bilangan bulat!',
            'image' => ':attribute harus berupa jpeg, jpg, atau png maximal 2mb',
        ];
        
        $request->validate([
            'nm_warga' => 'required|max:255',
            'nik' => 'required|min:16|max:16',
            'nkk' => 'required|min:16|max:16',
            'tmp_lahir' => 'required|max:255',
            'tgl_lahir' => 'required|max:255',
            'jk' => 'required|max:255',
            'alamat' => 'required|max:255',
            'rt' => 'required|numeric',
            'rw' => 'required|numeric',
            'kwn' => 'required|max:255',
            'status_nkh' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_kk' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_ktp' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ], $messages);

        if ($request->hasFile('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $imageName);
        }

        if ($request->hasFile('file_kk')) {
            $imageNameKk = time().'.'.$request->file_kk->extension();
            $request->file_kk->move(public_path('images/kk'), $imageNameKk);
        }

        if ($request->hasFile('file_ktp')) {
            $imageNameKtp = time().'.'.$request->file_ktp->extension();
            $request->file_ktp->move(public_path('images/ktp'), $imageNameKtp);
        }

        $create = Warga::create([
            'nm_warga' => $request->nm_warga,
            'nik' => $request->nik,
            'nkk' => $request->nkk,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jk' => $request->jk,
            'status_nkh' => $request->status_nkh,
            'kwn' => $request->kwn,
            'user_id' => Auth::id(),
            'pekerjaan_id' => $request->nm_pkj,
            'agama_id' => $request->nm_agama,
            'pendidikan_id' => $request->nm_pdd,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'image' => $imageName,
            'file_kk' => $imageNameKk,
            'file_ktp' => $imageNameKtp,
        ]);

        // return redirect('/warga/'.Auth::user()->warga->id)->with('add', 'Data berhasil ditambahkan..');
        return view('/landing');
        // return redirect()->route('/dashboardw');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warga = Warga::find($id); //pake Eloquent
        return view('warga.show', compact('warga'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'warga' => Warga::find($id),
            'nm_agama' => Agama::all(),
            'nm_pdd' => Pendidikan::all(),
            'nm_pkj' => Pekerjaan::all(),
        ];
        return view('warga.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        // dd($request->all());
        $messages = [
            'required' => 'input tidak valid!',
            'numeric' => 'Harus berupa angka!',
            'min' => 'Mohon masukan :attribute dengan benar!',
            'integer' => ':attribute harus berupa bilangan bulat!',
            'image' => ':attribute harus berupa jpeg, jpg, atau png maximal 2mb',
        ];
        
        $request->validate([
            'nm_warga' => 'required|max:255',
            'nik' => 'required|min:16|max:16',
            'nkk' => 'required|min:16|max:16',
            'tmp_lahir' => 'required|max:255',
            'tgl_lahir' => 'required|max:255',
            'jk' => 'required|max:255',
            'alamat' => 'required|max:255',
            'rt' => 'required|numeric',
            'rw' => 'required|numeric',
            'kwn' => 'required|max:255',
            'status_nkh' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_kk' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_ktp' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ], $messages);

        if($files = $request->file('image')) {
            $imageName = time().'.'.$files->extension();  
            $request->image->move(public_path('images'), $imageName);
        } else {
            $imageName = Warga::find($id);
            $imageName = $imageName->image;
        }

        if($files = $request->file('file_kk')) {
            $imageNameKk = time().'.'.$files->extension();  
            $request->file_kk->move(public_path('images/kk'), $imageNameKk);
        } else {
            $imageNameKk = Warga::find($id);
            $imageNameKk = $imageNameKk->file_kk;
        }

        if($files = $request->file('file_ktp')) {
            $imageNameKtp = time().'.'.$files->extension();  
            $request->file_ktp->move(public_path('images/ktp'), $imageNameKtp);
        } else {
            $imageNameKtp = Warga::find($id);
            $imageNameKtp = $imageNameKtp->file_ktp;
        }

        $warga = Warga::where('id', $id)->update([
            "nm_warga" => $request["nm_warga"],
            "nik" => $request["nik"],
            "nkk" => $request["nkk"],
            "tmp_lahir" => $request["tmp_lahir"],
            "tgl_lahir" => $request["tgl_lahir"],
            "jk" => $request["jk"],
            "status_nkh" => $request["status_nkh"],
            "kwn" => $request["kwn"],
            "pekerjaan_id" => $request["nm_pkj"],
            "agama_id" => $request["nm_agama"],
            "pendidikan_id" => $request["nm_pdd"],
            "alamat" => $request["alamat"],
            "rt" => $request["rt"],
            "rw" => $request["rw"],
            "image" => $imageName,
            'file_kk' => $imageNameKk,
            'file_ktp' => $imageNameKtp,
        ]);
        
        return redirect('dashboardw')->with('edit', 'Data berhasil diubah..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Warga::destroy($id);
        return redirect('/warga')->with('delete', 'Data berhasil dihapus..');
    }
}
