<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ibu extends Model
{
    use SoftDeletes;

    protected $dates = ['tglahir_ib'];

    protected $guarded = [];

    // 1:1 User
    public function user() {
        return $this->belongsTo('App\User');
    }

    // 1:1 Agama
    public function agama() {
        return $this->belongsTo('App\Agama');
    }

    // 1:1 Pekerjaan
    public function pekerjaan() {
        return $this->belongsTo('App\Pekerjaan');
    }
}
