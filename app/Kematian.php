<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kematian extends Model
{
    use SoftDeletes;

    protected $dates = ['tglahir_alm', 'tgmgl_alm'];
    
    protected $guarded = [];

    // 1:1 Agama
    public function agama() {
        return $this->belongsTo('App\Agama');
    }

    // 1:N User
    public function user() {
        return $this->belongsTo('App\User');
    }

    // 1:N Kematian
    public function pembuatan() {
        return $this->hasMany('App\Pembuatan');
    }
}
