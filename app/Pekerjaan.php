<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pekerjaan extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    
    // 1:1 Warga
    public function warga() {
        return $this->hasOne('App\Warga');
    }

    // 1:1 Ibu
    public function ibu() {
        return $this->hasOne('App\Ibu');
    }

    // 1:1 Ayah
    public function ayah() {
        return $this->hasOne('App\Ayah');
    }

    // 1:1 Sutri
    public function sutri() {
        return $this->hasOne('App\Sutri');
    }
}