<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Pembuatan extends Model
{
    protected $dates = ['tgl_lahir'];
    
    protected $guarded = [];
    // protected $fillable = [
    //     'file_kk', 'file_ktp'
    // ];

    // N:1 Surat
    public function surat() {
        return $this->belongsTo('App\Surat');
    }

    // N:1 Anak
    public function anak() {
        return $this->belongsTo('App\Anak');
    }

    // N:1 Kematian
    public function kematian() {
        return $this->belongsTo('App\Kematian', 'mati_id');
    }

    // N:1 User
    public function user() {
        return $this->belongsTo('App\User');
    }
}
