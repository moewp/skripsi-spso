<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pendidikan extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    
    // 1:1 Warga
    public function warga() {
        return $this->hasOne('App\Warga');
    }
}
