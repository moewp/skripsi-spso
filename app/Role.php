<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function user() {
        return $this->belongsToMany('App\User');
    }

    public function role_user() {
        return $this->hasOne('App\Role_User');
    }
}
