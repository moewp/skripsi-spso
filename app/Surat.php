<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Surat extends Model
{
    use SoftDeletes;

    // protected $guarded = [];
    protected $fillable = [
        'nm_surat', 'kd_surat'
    ];

    // 1:N Surat
    public function pembuatan() {
        return $this->hasMany('App\Pembuatan');
    }

}
