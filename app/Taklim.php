<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taklim extends Model
{
    use SoftDeletes;

    // protected $dates = ['tglahir_ib'];

    protected $guarded = [];

    // 1:1 User
    public function user() {
        return $this->belongsTo('App\User');
    }
}
