<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['tgl_lahir'];

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->belongsToMany('App\Role');
    }

    public function role_user() {
        return $this->hasOne('App\Role_User');
    }

    // 1:1 Warga
    public function warga() {
        return $this->hasOne('App\Warga');
    }

    // 1:N Pembuatan
    public function pembuatan()
    {
        return $this->hasMany('App\Pembuatan');
    }

    public function attachRole($role) {
        if (is_string($role)) {
            $role = Role::where('name', $role)->first();
        }
        return $this->role()->attach($role);
    }

    public function detachRole($role) {
        if (is_string($role)) {
            $role = Role::where('name', $role)->first();
        }
        return $this->role()->detach($role);
    }

    public function hasRole($name) {
        foreach ($this->role as $role) {
            if ($role->name === $name) {
                return true;
            }
        } return false;
    }

    public function hasAdmin() {
        if ($this->hasRole('Admin')) {
            return true;
        }
        return false;
    }

    public function createWarga() {
        // return Warga::create(['user_id' => $this->id]);
        return view('/warga/create');
    }

    // 1:1 Ibu
    public function ibu() {
        return $this->hasOne('App\Ibu');
    }

    // 1:1 Ayah
    public function ayah() {
        return $this->hasOne('App\Ayah');
    }

    // 1:1 Sutri
    public function sutri() {
        return $this->hasOne('App\Sutri');
    }

    // 1:N Perusahaan
    public function perusahaan() {
        return $this->hasOne('App\Perusahaan');
    }

    // 1:N Taklim
    public function taklim() {
        return $this->hasOne('App\Taklim');
    }

    // 1:N Anak
    public function anak() {
        return $this->hasOne('App\Anak');
    }

    // 1:N Kematian
    public function kematian() {
        return $this->hasOne('App\Kematian');
    }
}
