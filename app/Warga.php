<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warga extends Model
{
    protected $dates = ['tgl_lahir'];

    protected $guarded = [];
    // protected $fillable = ['*'];
    
    // 1:N Pembuatan
    public function pembuatan() {
        return $this->hasMany('App\Pembuatan');
    }

    // 1:1 User
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    // 1:1 Agama
    public function agama() {
        return $this->belongsTo('App\Agama');
    }

    // 1:1 Pekerjaan
    public function pekerjaan() {
        return $this->belongsTo('App\Pekerjaan');
    }

    // 1:1 Pendidikan
    public function pendidikan() {
        return $this->belongsTo('App\Pendidikan');
    }
}
