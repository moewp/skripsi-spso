<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wargas', function (Blueprint $table) {
            $table->id();
            $table->string('nm_warga', 225);
            $table->string('nik')->unique();
            $table->string('nkk');
            $table->string('tmp_lahir');
            $table->date('tgl_lahir');
            $table->enum('jk', ['laki-laki', 'perempuan']);
            $table->string('alamat');
            $table->char('rt', 3);
            $table->char('rw', 3);
            $table->string('kwn');

            // FK User
            $table->foreignId('user_id')->nullable()->constrained('users');
            // FK Agama
            $table->foreignId('agama_id')->nullable()->constrained('agamas');
            // FK Pendidikan
            $table->foreignId('pendidikan_id')->nullable()->constrained('pendidikans');
            // FK Pekerjaan
            $table->foreignId('pekerjaan_id')->nullable()->constrained('pekerjaans');

            $table->string('image')->nullable();
            $table->string('status_nkh', 20)->nullable()->default('text');
            $table->string('status_mgl', 20)->nullable()->default('hidup');
            $table->string('alamat_dom')->nullable();
            $table->string('mantan_sutri')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wargas');
    }
}
