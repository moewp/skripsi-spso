<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembuatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembuatans', function (Blueprint $table) {
            $table->id();

            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
            // FK Surat
            $table->foreignId('surat_id')->nullable()->constrained('surats');

            $table->dateTime('masuk');
            $table->dateTime('selesai');
            $table->dateTime('ambil');
            $table->tinyInteger('status')->nullable();
            $table->string('kpr_sktm', 225)->nullable();

            // file image
            $table->string('file-skmrs', 225)->nullable();
            $table->string('file-sklrs', 225)->nullable();
            $table->string('file-rtrw', 225)->nullable();
            $table->string('file-kk', 225)->nullable();
            $table->string('file-ktp', 225)->nullable();
            $table->string('file-bn', 225)->nullable();
            $table->string('file-pp', 225)->nullable();
            $table->string('file-kkps', 225)->nullable();
            $table->string('file-ktpay', 225)->nullable();
            $table->string('file-ktpib', 225)->nullable();
            $table->string('file-skmt', 225)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembuatans');
    }
}
