<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAlamatDomMantanSutriToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->string('alamat_dom')->nullable();
            $table->string('mantan_sutri')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dropColumn(['alamat_dom']);
            $table->dropForeign(['alamat_dom']);
            $table->dropColumn(['mantan_sutri']);
            $table->dropForeign(['mantan_sutri']);
        });
    }
}
