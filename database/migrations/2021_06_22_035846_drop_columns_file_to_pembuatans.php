<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFileToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dropColumn(['file_skmrs', 'file_sklrs', 'file_rtrw', 'file_kk', 'file_ktp', 'file_bn', 'file_pp', 'file_kkps', 'file_ktpay', 'file_ktpib', 'file_skmt']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->string('file_skmrs', 225)->nullable();
            $table->string('file_sklrs', 225)->nullable();
            $table->string('file_rtrw', 225)->nullable();
            $table->string('file_kk', 225)->nullable();
            $table->string('file_ktp', 225)->nullable();
            $table->string('file_bn', 225)->nullable();
            $table->string('file_pp', 225)->nullable();
            $table->string('file_kkps', 225)->nullable();
            $table->string('file_ktpay', 225)->nullable();
            $table->string('file_ktpib', 225)->nullable();
            $table->string('file_skmt', 225)->nullable();
        });
    }
}
