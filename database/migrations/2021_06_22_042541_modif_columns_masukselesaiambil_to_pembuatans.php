<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifColumnsMasukselesaiambilToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dateTime('masuk')->nullable()->change();
            $table->dateTime('selesai')->nullable()->change();
            $table->dateTime('ambil')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dateTime('masuk')->nullable();
            $table->dateTime('selesai')->nullable();
            $table->dateTime('ambil')->nullable();
        });
    }
}
