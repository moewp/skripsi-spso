<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsFilesToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->string('file_skmrs', 225)->nullable();
            $table->string('file_sklrs', 225)->nullable();
            $table->string('file_rtrw', 225)->nullable();
            $table->string('file_kk', 225)->nullable();
            $table->string('file_ktp', 225)->nullable();
            $table->string('file_bn', 225)->nullable();
            $table->string('file_pp', 225)->nullable();
            $table->string('file_kkps', 225)->nullable();
            $table->string('file_ktpay', 225)->nullable();
            $table->string('file_ktpib', 225)->nullable();
            $table->string('file_skmt', 225)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->string('file_skmrs', 225)->nullable();
            $table->string('file_sklrs', 225)->nullable();
            $table->string('file_rtrw', 225)->nullable();
            $table->string('file_kk', 225)->nullable();
            $table->string('file_ktp', 225)->nullable();
            $table->string('file_bn', 225)->nullable();
            $table->string('file_pp', 225)->nullable();
            $table->string('file_kkps', 225)->nullable();
            $table->string('file_ktpay', 225)->nullable();
            $table->string('file_ktpib', 225)->nullable();
            $table->string('file_skmt', 225)->nullable();
        });
    }
}
