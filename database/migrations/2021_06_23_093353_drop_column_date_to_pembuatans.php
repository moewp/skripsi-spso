<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnDateToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dropColumn(['masuk', 'selesai', 'ambil']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dateTime('masuk');
            $table->dateTime('selesai');
            $table->dateTime('ambil');
        });
    }
}
