<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NullColumnsWargaToWargas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wargas', function (Blueprint $table) {
            $table->string('nm_warga', 225)->nullable()->change();
            $table->dropUnique('wargas_nik_unique');
            $table->string('nik')->unique()->nullable()->change();
            $table->string('nkk')->nullable()->change();
            $table->string('tmp_lahir')->nullable()->change();
            $table->date('tgl_lahir')->nullable()->change();
            $table->string('jk', 20)->nullable()->change();
            $table->string('alamat')->nullable()->change();
            $table->string('rt', 3)->nullable()->change();
            $table->string('rw', 3)->nullable()->change();
            $table->string('kwn')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wargas', function (Blueprint $table) {
            $table->string('nm_warga', 225);
            $table->string('nik')->unique();
            $table->string('nkk');
            $table->string('tmp_lahir');
            $table->date('tgl_lahir');
            $table->enum('jk', ['laki-laki', 'perempuan']);
            $table->string('alamat');
            $table->char('rt', 3);
            $table->char('rw', 3);
            $table->string('kwn');
        });
    }
}
