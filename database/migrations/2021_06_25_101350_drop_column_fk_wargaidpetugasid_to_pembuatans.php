<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnFkWargaidpetugasidToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dropForeign('pembuatans_warga_id_foreign');
            $table->dropColumn('warga_id');
            $table->dropForeign('pembuatans_petugas_id_foreign');
            $table->dropColumn('petugas_id');
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
            $table->foreignId('petugas_id')->nullable()->constrained('users');
        });
    }
}
