<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIbusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ibus', function (Blueprint $table) {
            $table->id();

            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
             // FK Agama
            $table->foreignId('agama_id')->nullable()->constrained('agamas');
            // FK Pekerjaan
            $table->foreignId('pekerjaan_id')->nullable()->constrained('pekerjaans');

            $table->string('nm_ib', 225)->nullable();
            $table->string('bin_ib', 225)->nullable();
            $table->string('nik_ib', 191)->nullable();
            $table->string('tplahir_ib', 225)->nullable();
            $table->date('tglahir_ib')->nullable();
            $table->string('kwn_ib', 225)->nullable();
            $table->string('alamat_ib', 225)->nullable();
            $table->char('rt_ib', 3)->nullable();
            $table->char('rw_ib', 3)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ibus');
    }
}
