<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAyahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ayahs', function (Blueprint $table) {
            $table->id();

            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
             // FK Agama
            $table->foreignId('agama_id')->nullable()->constrained('agamas');
            // FK Pekerjaan
            $table->foreignId('pekerjaan_id')->nullable()->constrained('pekerjaans');

            $table->string('nm_ay', 225)->nullable();
            $table->string('bin_ay', 225)->nullable();
            $table->string('nik_ay', 191)->nullable();
            $table->string('tplahir_ay', 225)->nullable();
            $table->date('tglahir_ay')->nullable();
            $table->string('kwn_ay', 225)->nullable();
            $table->string('alamat_ay', 225)->nullable();
            $table->char('rt_ay', 3)->nullable();
            $table->char('rw_ay', 3)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ayahs');
    }
}
