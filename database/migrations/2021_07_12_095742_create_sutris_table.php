<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSutrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sutris', function (Blueprint $table) {
            $table->id();

            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
             // FK Agama
            $table->foreignId('agama_id')->nullable()->constrained('agamas');
            // FK Pekerjaan
            $table->foreignId('pekerjaan_id')->nullable()->constrained('pekerjaans');

            $table->string('nm_sutri', 225)->nullable();
            $table->string('bin_sutri', 225)->nullable();
            $table->string('nik_sutri', 191)->nullable();
            $table->string('tplahir_sutri', 225)->nullable();
            $table->date('tglahir_sutri')->nullable();
            $table->string('kwn_sutri', 225)->nullable();
            $table->string('alamat_sutri', 225)->nullable();
            $table->char('rt_sutri', 3)->nullable();
            $table->char('rw_sutri', 3)->nullable();
            $table->string('jk', 20)->nullable();
            $table->date('tgl_nkh')->nullable();
            $table->date('tgmgl_sutri')->nullable();
            $table->string('tpmgl_sutri', 225)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sutris');
    }
}
