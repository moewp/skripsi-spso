<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerusahaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perusahaans', function (Blueprint $table) {
            $table->id();

            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');

            $table->string('nm_ush', 225)->nullable();
            $table->string('alamat_ush', 225)->nullable();
            $table->string('jns_ush', 225)->nullable();
            $table->string('pj_ush', 225)->nullable();
            $table->string('sb_ush', 225)->nullable();
            $table->string('akta_ush', 225)->nullable();
            $table->string('jml_kry', 225)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaans');
    }
}
