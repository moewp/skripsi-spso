<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaklimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taklims', function (Blueprint $table) {
            $table->id();

            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');

            $table->string('nm_mt', 225)->nullable();
            $table->string('alamat_mt', 225)->nullable();
            $table->string('jns_mt', 225)->nullable();
            $table->string('sb_mt', 225)->nullable();
            $table->string('akta_mt', 225)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taklims');
    }
}
