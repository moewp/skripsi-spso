<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anaks', function (Blueprint $table) {
            $table->id();
            
            // FK Warga
            $table->foreignId('warga_id')->nullable()->constrained('wargas');

            $table->string('nm_anak', 225)->nullable();
            $table->string('tplahir_anak', 225)->nullable();
            $table->date('tglahir_anak')->nullable();
            $table->string('jk', 20)->nullable();
            $table->char('anak_ke', 3)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anaks');
    }
}
