<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnFkWargaidToIbus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ibus', function (Blueprint $table) {
            $table->dropForeign('ibus_warga_id_foreign');
            $table->dropColumn('warga_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ibus', function (Blueprint $table) {
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
        });
    }
}
