<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnFkWargaidToAyahs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ayahs', function (Blueprint $table) {
            $table->dropForeign('ayahs_warga_id_foreign');
            $table->dropColumn('warga_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ayahs', function (Blueprint $table) {
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
        });
    }
}
