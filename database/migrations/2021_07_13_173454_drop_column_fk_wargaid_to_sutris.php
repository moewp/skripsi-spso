<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnFkWargaidToSutris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sutris', function (Blueprint $table) {
            $table->dropForeign('sutris_warga_id_foreign');
            $table->dropColumn('warga_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sutris', function (Blueprint $table) {
            $table->foreignId('warga_id')->nullable()->constrained('wargas');
        });
    }
}
