<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsAlamatAndJenisUsahaToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->string('alamat_ush')->nullable();
            $table->string('jns_ush')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dropColumn(['alamat_ush']);
            $table->dropColumn(['jns_ush']);
        });
    }
}
