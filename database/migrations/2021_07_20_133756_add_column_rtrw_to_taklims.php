<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRtrwToTaklims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taklims', function (Blueprint $table) {
            $table->string('rt_mt')->nullable();
            $table->string('rw_mt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taklims', function (Blueprint $table) {
            $table->dropColumn(['rt_mt']);
            $table->dropColumn(['rw_mt']);
        });
    }
}
