<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKematiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kematians', function (Blueprint $table) {
            $table->id();
            $table->string('nm_alm', 225)->nullable();
            $table->string('nik_alm', 225)->nullable();
            $table->string('tplahir_alm', 225)->nullable();
            $table->date('tglahir_alm')->nullable();
            $table->string('jk', 20)->nullable();
            $table->string('alamat_alm', 225)->nullable();
            $table->char('rt_alm', 3)->nullable();
            $table->char('rw_alm', 3)->nullable();

            $table->string('tpmgl_alm', 225)->nullable();
            $table->date('tgmgl_alm')->nullable();
            $table->string('sebab_mgl', 225)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kematians');
    }
}
