<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFkAgamaidToKematians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kematians', function (Blueprint $table) {
            $table->foreignId('agama_id')->nullable()->constrained('agamas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kematians', function (Blueprint $table) {
            $table->dropForeign('kematians_agama_id_foreign');
            $table->dropColumn('agama_id');
        });
    }
}
