<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnMatiidToPembuatans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->dropForeign('pembuatans_mati_id_foreign');
            $table->dropColumn('mati_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembuatans', function (Blueprint $table) {
            $table->foreignId('mati_id')->nullable()->constrained('kematians');
        });
    }
}
