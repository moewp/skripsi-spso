<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFileToSutris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sutris', function (Blueprint $table) {
            $table->string('file_kkps', 225)->nullable();
            $table->string('file_ktpps', 225)->nullable();
            $table->string('file_bn', 225)->nullable();
            $table->string('file_skmt', 225)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sutris', function (Blueprint $table) {
            //
        });
    }
}
