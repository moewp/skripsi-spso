;(function ($) { $.fn.datepicker.language['idnd'] = {
    days: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
    daysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
    daysMin: ['Mn', 'Sn', 'Sl', 'Ra', 'Ka', 'Ju', 'Sa'],
    months: ['Januari','Februari','Maret','April','Mei','Juni', 'Juli','Agustus','September','Oktober','November','Desember'],
    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'DD',
    timeFormat: 'hh:ii aa',
    firstDay: 0
}; })(jQuery);