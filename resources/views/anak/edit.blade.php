@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Anak</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Anak -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Data Anak</h3>
                  </div>
                  
                  <form method="POST" action="/anak/{{$anak->id}}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Nama Anak -->
                              <div class="form-group">
                                    <label for="nm_anak">Nama Anak</label>
                                    <input type="text" value="{{ old('nm_anak', $anak->nm_anak) }}" class="form-control @error('nm_anak') is-invalid @enderror" name="nm_anak" id="nm_anak" placeholder="Masukan nama anak">
                                    @error('nm_anak')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir Anak -->
                              <div class="form-group">
                                    <label for="tplahir_anak">Tempat Lahir Anak</label>
                                    <input type="text" value="{{ old('tplahir_anak', $anak->tplahir_anak) }}" class="form-control @error('tplahir_anak') is-invalid @enderror" name="tplahir_anak" id="tplahir_anak" placeholder="Masukan nama anak">
                                    @error('tplahir_anak')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir Anak -->
                              <div class="form-group">
                                    <label for="tglahir_anak">Tanggal Lahir Anak</label>
                                    <input class="form-control col-sm-2 @error('tglahir_anak') is-invalid @enderror" name="tglahir_anak" id="tglahir_anak" value="{{ old('tglahir_anak', $anak->tglahir_anak->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tglahir_anak', $anak->tglahir_anak) }}" class="form-control col-sm-3 @error('tglahir_anak') is-invalid @enderror" name="tglahir_anak" id="tglahir_anak" placeholder="Masukan nama anak" required>
                                    @error('tglahir_anak')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jenis Kelamin Anak -->
                              <div class="form-group">
                                    <label for="jk">Jenis Kelamin Anak</label>
                                    <select class="form-control col-sm-3 @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="{{ old('jk', $anak->jk) }}">{{ old('jk', $anak->jk) }}</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    @error('jk')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Anak Ke- -->
                              <div class="form-group">
                                    <label for="anak_ke">Anak Ke-</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('anak_ke', $anak->anak_ke) }}" class="form-control col-sm-3 @error('anak_ke') is-invalid @enderror" name="anak_ke" id="anak_ke" maxlength="3" required>
                                    @error('anak_ke')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <hr>
                              <!-- SKLRS -->
                              <div class="form-group row mt-5">
                                    <label for="file_sklrs" class="col-md-3 col-form-label text-md-right">SK. Kelahiran RS.</label>
                                    <div class="col-md-9">
                                          <img src="/images/sklrs/{{ $anak->file_sklrs }}" width="70px" alt="no file||">
                                          File Name : {{ $anak->file_sklrs }}
                                          <input id="file_sklrs" type="file" class="form-control" name="file_sklrs" autofocus>
                                          @error('file_sklrs')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/anak">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>

@endsection