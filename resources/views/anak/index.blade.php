@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Anak</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Anak -->
                  <div class="card-header">
                        <h3 class="card-title">Tambah Data Anak</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/anak') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::id()}}">
                        <div class="card-body">
                              <!-- Nama Anak -->
                              <div class="form-group">
                                    <label for="nm_anak">Nama Anak</label>
                                    <input type="text" value="{{ old('nm_anak') }}" class="form-control @error('nm_anak') is-invalid @enderror" name="nm_anak" id="nm_anak" placeholder="Masukan nama anak">
                                    @error('nm_anak')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir -->
                              <div class="form-group">
                                    <label for="tplahir_anak">Tempat Lahir</label>
                                    <input type="text" value="{{ old('tplahir_anak') }}" class="form-control @error('tplahir_anak') is-invalid @enderror" name="tplahir_anak" id="tplahir_anak" placeholder="Tempat lahir anak">
                                    @error('tplahir_anak')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir -->
                              <div class="form-group col-sm-3">
                                    <label for="tglahir_anak">Tanggal Lahir</label>
                                    <input type="date" value="{{ old('tglahir_anak') }}" class="form-control @error('tglahir_anak') is-invalid @enderror" name="tglahir_anak" id="tglahir_anak" placeholder="tanggal lahir anak">
                                    @error('tglahir_anak')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jenis Kelamin -->
                              <div class="form-group">
                                    <label for="jk">Jenis Kelamin</label>
                                    <div class="col-sm-3">
                                          <select class="form-control @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="">-- Silakan Pilih --</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    </div>
                              </div>
                              <!-- Anak Ke- -->
                              <div class="form-group col-sm-3">
                                    <label for="anak_ke">Anak Ke-</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('anak_ke') }}" class="form-control @error('anak_ke') is-invalid @enderror" name="anak_ke" id="anak_ke" placeholder="contoh: 3">
                                    @error('anak_ke')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <hr>
                              <!-- SK. Kelahiran RS -->
                              <div class="form-group row">
                                    <label for="file_sklrs" class="col-sm-2 col-form-label">SK. Kelahiran RS</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_sklrs') is-invalid @enderror" id="file_sklrs" name="file_sklrs" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_sklrs')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                  </form>
            </div>

            <br>

            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              
                              <div class="card-header">
                              <h3 class="card-title"></h3>

                              <!-- SEARCH -->
                              <!-- <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div>
                              </div> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Anak</th>
                                                <th>Anak Ke</th>
                                                <th>Tanggal Lahir</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $anak as $anak )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $anak->nm_anak }}</td>
                                                <td>{{ $anak->anak_ke }}</td>
                                                <td>{{ $anak->tglahir_anak->format('d M, Y')}}</td>
                                                
                                                <td>
                                                      <a href="/anak/{{$anak->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/anak/{{$anak->id}}" method="POST" id="delete{{ $anak->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $anak->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="4" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Anak
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection