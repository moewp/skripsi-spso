@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Ayah</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Warga -->
                  <div class="card-header">
                        <h3 class="card-title">Isi Data Ayahmu dengan Benar!</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/ayah') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::id()}}">
                  
                        <div class="card-body">
                              <!-- Nama Lengkap -->
                              <div class="form-group row">
                                    <label for="nm_ay" class="col-sm-2 col-form-label">Nama Ayah</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('nm_ay') }}" class="form-control @error ('nm_ay') is-invalid @enderror" id="nm_ay" placeholder="Nama Lengkap Ayah" name="nm_ay" required>
                                          @error('nm_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- NIK BIN -->
                              <div class="form-group row">
                                    <!-- NIK -->
                                    <label for="nik_ay" class="col-sm-2 col-form-label">NIK Ayah</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik_ay') }}" class="form-control @error ('nik_ay') is-invalid @enderror" id="nik_ay" placeholder="NIK" name="nik_ay" required>
                                          @error('nik_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- BIN -->
                                    <label for="bin_ay" class="col-sm-2 col-form-label">Bin Ayah</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="{{ old('bin_ay') }}" class="form-control @error ('bin_ay') is-invalid @enderror" id="bin_ay" placeholder="Bin dari Ayah" name="bin_ay" required>
                                          @error('bin_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              
                              <!-- TTL -->
                              <div class="form-group row">
                                    <!-- Tempat Lahir -->
                                    <label for="tplahir_ay" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('tplahir_ay') }}" class="form-control @error ('tplahir_ay') is-invalid @enderror" id="tplahir_ay" name="tplahir_ay" placeholder="Tempat Lahir" required>
                                          @error('tplahir_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Tanggal Lahir -->
                                    <label for="tglahir_ay" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="date" class="form-control @error ('tglahir_ay') is-invalid @enderror"  name="tglahir_ay" id="tglahir_ay" placeholder="Tanggal Lahir" required/>
                                          @error('tglahir_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Kewarganegaraan -->
                              <div class="form-group row">
                                    <label for="kwn_ay" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="Indonesia" class="form-control @error ('kwn_ay') is-invalid @enderror" name="kwn_ay" id="kwn_ay" placeholder="Kewarganegaraan" required>
                                          @error('kwn_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Pekerjaan -->
                                    <label for="nm_pkj" class="col-sm-2 col-form-label">Pekerjaan Ayah</label>
                                    <div class="col-sm-3">
                                          <select name="nm_pkj" required class="form-control @error ('nm_pkj') is-invalid @enderror" {{ count($nm_pkj) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_pkj) == 0)						
                                                      <option>Pilihan tidak ada</option>
                                                @else										
                                                      <option value="">--Silakan Pilih--</option>			
                                                      @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                      @endforeach
                                                @endif
                                                @error('nm_pkj')
                                                      <p class="text-danger">{{ $message }}</p>
                                                @enderror	
                                          </select>
                                    </div>
                              </div>

                              <!-- Agama -->
                              <div class="form-group row">
                                    <label for="nm_agama" class="col-sm-2 col-form-label">Agama Ayah</label>
                                    <div class="col-sm-3">
                                          <select name="nm_agama" required class="form-control @error('nm_agama') is-invalid @enderror" {{ count($nm_agama) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_agama) == 0)							
                                                      <option>Pilihan tidak ada</option>
                                                @else											
                                                      <option value="">--Silakan Pilih--</option>				
                                                      @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                      @endforeach
                                                @endif	
                                          </select>
                                          <span class="text-danger">@error('nm_agama') {{ $message }} @enderror</span>
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat_ay" class="col-sm-2 col-form-label">Alamat Ayah</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat_ay') }}" class="form-control @error ('alamat_ay') is-invalid @enderror" id="alamat_ay" placeholder="Alamat" name="alamat_ay" required>
                                          @error('alamat_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt_ay" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_ay') }}" class="form-control @error ('rt_ay') is-invalid @enderror" id="rt_ay" placeholder="contoh: 012" name="rt_ay" required>
                                          @error('rt_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw_ay" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_ay') }}" class="form-control @error ('rw_ay') is-invalid @enderror" id="rw_ay" placeholder="contoh: 005" name="rw_ay" required>
                                          @error('rw_ay')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <hr>
                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file_ktpay" class="col-sm-2 col-form-label">KTP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_ktpay') is-invalid @enderror" id="file_ktpay" name="file_ktpay" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_ktpay')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                  </form>
            </div>
      </div>
</div>

@endsection