@extends('partials.master')

@section('content')

<div class="row mb-4"></div>
<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Profile -->
                  <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Data Ayah</h3>
                  </div>
                  <form method="POST" action="/ayah/{{ $ayah->id }}" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                        <div class="card-body">
                              <!-- Nama Ayah -->
                              <div class="form-group">
                                    <label for="nm_ay">Nama Ayah</label>
                                    <input type="text" value="{{ old('nm_ay', $ayah->nm_ay) }}" class="form-control @error('nm_ay') is-invalid @enderror" name="nm_ay" id="nm_ay" required>
                                    @error('nm_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- NIK -->
                              <div class="form-group">
                                    <label for="nik_ay">NIK</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik_ay', $ayah->nik_ay) }}" class="form-control @error('nik_ay') is-invalid @enderror" name="nik_ay" id="nik_ay" required>
                                    @error('nik_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- BIN -->
                              <div class="form-group">
                                    <label for="bin_ay">Bin dari Ayah</label>
                                    <input type ="text" maxlength="16" value="{{ old('bin_ay', $ayah->bin_ay) }}" class="form-control @error('bin_ay') is-invalid @enderror" name="bin_ay" id="bin_ay" required>
                                    @error('bin_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir -->
                              <div class="form-group">
                                    <label for="tplahir_ay">Tempat Lahir</label>
                                    <input type="text" value="{{ old('tplahir_ay', $ayah->tplahir_ay) }}" class="form-control @error('tplahir_ay') is-invalid @enderror" name="tplahir_ay" id="tplahir_ay" required>
                                    @error('tplahir_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir -->
                              <div class="form-group">
                                    <label for="tglahir_ay">Tanggal Lahir</label>
                                    <input class="form-control col-sm-2 @error('tglahir_ay') is-invalid @enderror" name="tglahir_ay" id="tglahir_ay" value="{{ old('tglahir_ay', $ayah->tglahir_ay->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tglahir_ay', $ayah->tglahir_ay) }}" class="form-control @error('tglahir_ay') is-invalid @enderror" name="tglahir_ay" id="tglahir_ay" required>
                                    @error('tglahir_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Kewarganegaraan -->
                              <div class="form-group">
                                    <label for="kwn_ay">Kewarganegaraan</label>
                                    <input type="text" value="{{ old('kwn_ay', $ayah->kwn_ay) }}" class="form-control @error('kwn_ay') is-invalid @enderror" name="kwn_ay" id="kwn_ay" required>
                                    @error('kwn_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Pekerjaan -->
                              <div class="form-group">
                                    <label for="nm_pkj">Pekerjaan</label>
                                    <select class="form-control @error ('nm_pkj') is-invalid @enderror" id="nm_pkj" name="nm_pkj" required>
                                                <option value="{{ old('nm_pkj', $ayah->pekerjaan->id) }}">{{ old('nm_pkj', $ayah->pekerjaan->nm_pkj) }}</option>
                                                @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_pkj')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Agama -->
                              <div class="form-group">
                                    <label for="nm_agama">Agama</label>
                                    <select class="form-control @error ('nm_agama') is-invalid @enderror" id="nm_agama" name="nm_agama" required>
                                                <option value="{{ old('nm_agama', $ayah->agama->id) }}">{{ old('nm_agama', $ayah->agama->nm_agama) }}</option>
                                                @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_agama')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat -->
                              <div class="form-group">
                                    <label for="alamat_ay">Alamat Ayah</label>
                                    <input type="text" value="{{ old('alamat_ay', $ayah->alamat_ay) }}" class="form-control @error('alamat_ay') is-invalid @enderror" name="alamat_ay" id="alamat_ay" required>
                                    @error('alamat_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT -->
                              <div class="form-group">
                                    <label for="rt_ay">RT</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_ay', $ayah->rt_ay) }}" class="form-control @error('rt_ay') is-invalid @enderror" name="rt_ay" id="rt_ay" maxlength="3" required>
                                    @error('rt_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW -->
                              <div class="form-group">
                                    <label for="rw_ay">RW</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_ay', $ayah->rw_ay) }}" class="form-control @error('rw_ay') is-invalid @enderror" name="rw_ay" id="rw_ay" maxlength="3" required>
                                    @error('rw_ay')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- KTP -->
                              <div class="form-group row mt-5">
                                    <label for="file_ktpay" class="col-md-3 col-form-label text-md-right">KTP</label>
                                    <div class="col-md-9">
                                          <img src="/images/ktpay/{{ $ayah->file_ktpay }}" width="70px">
                                          File Name : {{ $ayah->file_ktpay }}
                                          <input id="file_ktpay" type="file" class="form-control" name="file_ktpay" autofocus>
                                          @error('file_ktpay')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/ayah/{{$ayah->id}}">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection