@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="col-sm-6">
                  @if (Auth::user()->ayah != NULL)
                  
                  @else
                  <a href="{{ url('ayah/create') }}" class="btn btn-outline-primary btn-sm btn-rounded">
                        <i class="mdi mdi-plus-circle"></i> {{ __('Tambah Data') }}
                  </a>
                  @endif
            </div>
      </div>
</section>

<div class="container-fluid">
      <div class="form-group row">
            <div class="col-6 ml-3">
                  @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                  @endif
                  <div class="card">
                        <div class="card-header bg-secondary">
                              <h3 class="card-title text-white">Data Ayah</h3>
                        </div>
                        @if (Auth::user()->ayah != NULL)
                        <div class="card-body">
                              <h3 class="card-title text-muted mr-2">NIK Ayah: </h3>
                              <h3 class="card-title mr-5">{{ $ayah->nik_ay }}</h3>
                              <h3 class="card-text"><strong>{{ $ayah->nm_ay }}</strong></h3>
                              <p class="card-title">Bin, {{ $ayah->bin_ay }}</p>
                              <p class="card-text">{{ $ayah->tplahir_ay }}, {{ $ayah->tglahir_ay->format('d M Y') }}</p>
                              <hr>
                              <h3 class="card-title text-muted mr-2">agama: </h3>
                              <h3 class="card-title mr-5">{{ $ayah->agama->nm_agama }}</h3>
                              <h3 class="card-title text-muted mr-2">pekerjaan: </h3>
                              <h3 class="card-title">{{ $ayah->pekerjaan->nm_pkj }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">kewarganegaraan: </h3>
                              <h3 class="card-title">{{ $ayah->kwn_ay }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">alamat ayah: </h3>
                              <h3 class="card-title">{{ $ayah->alamat_ay }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">rt: </h3>
                              <h3 class="card-title mr-5">{{ $ayah->rt_ay }}</h3>
                              <h3 class="card-title text-muted mr-2">rw: </h3>
                              <h3 class="card-title">{{ $ayah->rw_ay }}</h3>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                              <a href="/ayah/{{$ayah->id}}/edit" class="btn btn-dark mr-2">edit</a>
                              <!-- <a class="btn btn-light shadow-sm " href="/choose">Batal</a> -->
                        </div>
                        @else
                        <h5 class="card-text text-center"><strong>Tidak ada data</strong></h5>
                        <p class="card-text text-center">Silakan pilih tambah data</p>
                        @endif
                        <!-- /.card-footer-->
                  </div>
                  <div class="col-12">
                        <div class="card mt-md-5">
                        </div>
                  </div>
            </div>
      </div>
</div>

@endsection