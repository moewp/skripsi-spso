@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>{{$surat->nm_surat}}</h1>
                        <small>Tanggal: {{ $date->format('d M Y') }}</small>
                  </div>
            </div>
      </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<div class="container">
      <div class="container">
            <div class="card card-info">
                  <div class="card-header bg-warning">
                        <h3 class="card-title">Isi Data dengan Benar dan Sesuai</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form class="form-horizontal" method="POST" action="{{ route('postBuat', $surat->id) }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="surat_id" value="{{ $surat->id }}">
                  <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                  
                        <div class="card-body">
                        @if (($surat->id) == 1)
                              <center>Isi lampiran Pengantar RT/RW di bawah.</center>
                        @elseif (($surat->id) == 2)
                              <center>Isi lampiran Pengantar RT/RW di bawah.</center>
                        @elseif (($surat->id) == 3)
                              <center>Pastikan kamu sudah mengisi data <u>Pasangan</u> dan <u>Anak</u> di Profilmu.</center>
                              <label for="nm_anak" class="col-sm-2 col-form-label pt-3">Pilih Data Anak</label>
                              <div class="col-sm-3">
                                    <select name="nm_anak" required class="form-control @error('nm_anak') is-invalid @enderror" {{ count($nm_anak) == 0 ? 'disabled' : '' }}>
                                          @if(count($nm_anak) == 0)							
                                                <option>--Pilihan tidak ada--</option>
                                          @else											
                                                <option value="">--Silakan Pilih--</option>				
                                                @foreach($nm_anak as $value) 			
                                                      <option value="{{ $value->id }}">{{ $value->nm_anak }}</option>
                                                @endforeach
                                          @endif	
                                    </select>
                                    <span class="text-danger">@error('nm_anak') {{ $message }} @enderror</span>
                              </div>
                        @elseif (($surat->id) == 4)
                              <center>Pastikan kamu sudah mengisi data Pasangan di Profilmu.</center>
                        @elseif (($surat->id) == 5)
                              <center>Pastikan kamu sudah mengisi data Pasangan di Profilmu.</center>
                              <center>Dan telah mengisi kolom untuk file sk. kematian, tempat meninggal, dan tanggal meninggal pasangan.</center>
                        @elseif (($surat->id) == 6)
                              <center>Pastikan kamu sudah mengisi data Kematian di Profilmu.</center>
                              <label for="nm_alm" class="col-sm-2 col-form-label pt-3">Pilih Data Kematian</label>
                              <div class="col-sm-3">
                                    <select name="nm_alm" required class="form-control @error('nm_alm') is-invalid @enderror" {{ count($nm_alm) == 0 ? 'disabled' : '' }}>
                                          @if(count($nm_alm) == 0)							
                                                <option>--Pilihan tidak ada--</option>
                                          @else											
                                                <option value="">--Silakan Pilih--</option>				
                                                @foreach($nm_alm as $value) 			
                                                      <option value="{{ $value->id }}">{{ $value->nm_alm }}</option>
                                                @endforeach
                                          @endif	
                                    </select>
                                    <span class="text-danger">@error('nm_alm') {{ $message }} @enderror</span>
                              </div>
                        @elseif (($surat->id) == 7)
                              <center>Pastikan kamu sudah mengisi data Ayah di Profilmu.</center>
                        @elseif (($surat->id) == 8)
                              <div class="form-group row">Domisili saat ini: </div>
                              <!-- Alamat Domisili -->
                              <div class="form-group row">
                                    <label for="alamat_dom" class="col-sm-2 col-form-label">Alamat Domisili</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat_dom') is-invalid @enderror" id="alamat_dom" placeholder="Alamat Domisili" name="alamat_dom">
                                          <input type="text" class="form-control" placeholder="Desa Sasakpanjang Kec. Tajurhalang Kab. Bogor" disabled="">
                                    </div>
                              </div>
                        @elseif (($surat->id) == 9)
                              <center>Pastikan kamu sudah mengisi data Ibu dan Ayah di Profilmu.</center>
                              <!-- Nama Suami/Istri -->
                              <div class="form-group row" style="padding: 20px 0px 0px 0px;">
                                    <label for="mantan_sutri" class="col-sm-2 col-form-label">Nama Mantan Suami/Istri</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('mantan_sutri') is-invalid @enderror" name="mantan_sutri" id="mantan_sutri" placeholder="Nama Mantan Suami/Istri">
                                          <small class="text-info">Kosongkan jika tidak memiliki.</small>
                                    </div>
                              </div>
                        @elseif (($surat->id) == 10)
                              <!-- <div class="form-group row">Isi data berikut dengan benar!</div> -->
                              <!-- Jenis Usaha -->
                              <div class="form-group row">
                                    <label for="jns_ush" class="col-sm-2 col-form-label">Jenis Usaha</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('jns_ush') is-invalid @enderror" id="jns_ush" placeholder="Jenis Usaha" name="jns_ush">
                                    </div>
                              </div>
                              <!-- Alamat Usaha -->
                              <div class="form-group row">
                                    <label for="alamat_ush" class="col-sm-2 col-form-label">Alamat Usaha</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat_ush') is-invalid @enderror" id="alamat_ush" placeholder="Alamat Usaha" name="alamat_ush">
                                          <input type="text" class="form-control" placeholder="Desa Sasakpanjang Kec. Tajurhalang Kab. Bogor" disabled="">
                                    </div>
                              </div>
                        @elseif (($surat->id) == 11)
                              <center>Pastikan kamu sudah mengisi data Perusahaan di Profilmu.</center>
                        @elseif (($surat->id) == 12)
                              <center>Pastikan kamu sudah mengisi data Majlis Taklim di Profilmu.</center>
                        @else
                              <!-- Keperluan -->
                              <div class="form-group row">
                                    <label for="kpr_sktm" class="col-sm-2 col-form-label">Keperluan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('kpr_sktm') is-invalid @enderror" id="kpr_sktm" name="kpr_sktm" placeholder="Keperluan">
                                    </div>
                              </div>
                        @endif
                        <hr>
                        <div class="form-group row" style="padding: 20px 0px 0px 0px;">Lampiran: </div>

                              <!-- Pengantar RT/RW -->
                              <div class="form-group row">
                                    <label for="file_rtrw" class="col-sm-2 col-form-label">Pengantar RT/RW</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_rtrw') is-invalid @enderror" id="file_rtrw" name="file_rtrw" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_rtrw')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                              <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                        <!-- /.card-footer -->
                  </form>
            </div>
      </div>
</div>
<!-- /.content -->
@endsection