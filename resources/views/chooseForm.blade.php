<!DOCTYPE html>
<html lang='en'>

<head>
      <meta class="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <title>Pilih Surat</title>
      <link rel="shortcut icon" href="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png') }}">
      <!-- Don't forget to add your metadata here -->
      <link rel='stylesheet' href="{{ asset('/evie/css/style.min.css')}}" />
      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

</head>

<body>
      <!-- Steps -->
      <div class="steps landing__section">
            <div class="container">
                  <a href="{{ url('/')}}">
                        <span style="display: inline-block; margin-top: 10px;"><img src="{{ asset('/evie/images/Kabupaten Bogor 160x160.png')}}" alt="Kabupaten Bogor Logo" class="brand-image" style="height: 65px;"></span>
                  </a>
                  <p style="margin-top: -5px;">Pelayanan Terpadu Desa Sasakpanjang</p>
                  <hr>
                  <h2>Berikut jenis-jenis surat yang ada di Desa Sasakpanjang</h2>
                  <p>Pilihlah salah satu surat yang diinginkan.</p>

            </div>
            
            <div class="container">
                  <!-- <div class="steps__inner">
                        <div class="step">
                              <div class="step__media">
                                    <a href="{{ url('fpktp')}}" class="button button__accent">Formulir Permohonan KTP</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('skk')}}" class="button button__accent">Surat Kematian</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('skbm')}}" class="button button__accent">Surat Keterangan Belum Menikah</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('skbd')}}" class="button button__accent">Surat Keterangan Bersih Diri</a>
                              </div>
                        </div>
                        <div class="step">
                              <div class="step__media">
                                    <a href="{{ url('skdmt')}}" class="button button__accent">Surat Keterangan Domisili Majlis Taklim</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('sktm')}}" class="button button__accent">Surat Keterangan Tidak Mampu</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('skdu')}}" class="button button__accent">Surat Keterangan Domisili Usaha</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('skd')}}" class="button button__accent">Surat Keterangan Domisili</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('skl')}}" class="button button__accent">Surat Keterangan Kelahiran</a>
                              </div>
                        </div>
                        <div class="step">
                              <div class="step__media">
                                    <a href="{{ url('skksi')}}" class="button button__accent">Surat Keterangan Kematian Suami-Istri</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('sksm')}}" class="button button__accent">Surat Keterangan Sudah Menikah</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('sku')}}" class="button button__accent">Surat Keterangan Usaha</a>
                              </div>
                              <div class="step__media">
                                    <a href="{{ url('spp')}}" class="button button__accent">Surat Pengantar Perkawinan</a>
                              </div>
                        </div>

                  </div> -->
                  <div class="container">
                        <div class="row">
                              @foreach ($surats as $surat)
                              <div class="col-sm-4 mb-4">
                                    <div class="card border border-info" style="height: 100px;">
                                          <div class="card-body">
                                                <p class="card-title">-{{ $surat->nm_surat }}-</p>
                                                <p class="card-text"></p>
                                                <a href="{{ route('getBuat', $surat->id) }}" class="btn btn-secondary btn-sm">Pilih</a>
                                          </div>
                                    </div>
                              </div>
                              @endforeach
                        </div>
                  </div>
            </div>

      </div>

      <script src="{{ asset('/evie/js/app.min.js')}}"></script>
      <!-- Option 1: Bootstrap Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>

</html>