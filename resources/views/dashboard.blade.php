@extends('partials.master')

<!-- Start Content -->
@section('content')

<div class="content-header">
            <div class="container-fluid">
                  <div class="row mb-2">
                        <div class="col-sm-6">
                              <h1 class="m-0">Dashboard</h1>
                        </div><!-- /.col -->
                  </div><!-- /.row -->
            </div><!-- /.container-fluid -->
</div>

<section class="content">
      <div class="container-fluid">
            <div class="row">
                  <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                        <div class="inner">
                        <h3>{{ $pendingBuat }}</h3>

                        <p>Pending Surat</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-ionic"></i>
                        </div>
                        <a href="{{ url('/pending') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                        <div class="inner">
                        <h3>{{ $completeBuat }}</h3>

                        <p>Completed Surat</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-archive"></i>
                        </div>
                        <a onclick="return complete()" href="#complete" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                        
                        <div class="small-box bg-info">
                        <div class="inner">
                        <h3>{{ $wargaCount }}</h3>

                        <p>Warga</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-person-stalker"></i>
                        </div>
                        <a href="{{ url('/indexwarga') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div>
                  <!-- ./col -->
                  <!-- UNIQUE VISITOR -->
                  <!-- <div class="col-lg-3 col-6">
                        <div class="small-box bg-danger">
                        <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div> -->
                  <!-- ./col -->
            </div>
      </div>
</section>

@endsection