@extends('partials.master')

<!-- Start Content -->
@section('content')

<div class="content-header">
      @if (session('edit'))
            <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <i class="icon fas fa-check"></i>
                  {{ session('edit') }}
            </div>
      @endif
      @if (session('delete'))
            <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <i class="icon fas fa-check"></i>
                  {{ session('delete') }}
            </div>
      @endif
      @if (session('add'))
            <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <i class="icon fas fa-check"></i>
                  {{ session('add') }}
            </div>
      @endif
            <div class="container-fluid">
                  <div class="row mb-2">
                        <div class="col-sm-6">
                              <h1 class="m-0">Dashboardku</h1>
                        </div><!-- /.col -->
                  </div><!-- /.row -->
            </div><!-- /.container-fluid -->
</div>

<section class="content">
      <div class="container-fluid">
            <div class="row">
                  <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-light">
                        <div class="inner">
                        <h3>{{ $pendingBuat }}</h3>

                        <p>Pending Surat</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-ionic"></i>
                        </div>
                        <a href="{{ url('/daftarbuat') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-light">
                        <div class="inner">
                        <h3>{{ $riwayatBuat }}</h3>

                        <p>Riwayat Surat</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-archive"></i>
                        </div>
                        <a href="{{ url('/riwayat') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div>
                  <!-- ./col -->

                  <!-- Profil -->
                  <!-- <div class="col-lg-3 col-6">
                        <div class="small-box bg-light">
                        <div class="inner">
                        <h3>44</h3>

                        <p>Profilku</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-person"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div> -->

                  <!-- ./col -->
                  <!-- Unique Visitors -->
                  <!-- <div class="col-lg-3 col-6">
                        <div class="small-box bg-danger">
                        <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                  </div> -->
                  <!-- ./col -->
            </div>
      </div>
</section>

@endsection