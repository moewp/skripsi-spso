@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Agama</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Agama -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Data Agama</h3>
                  </div>
                  
                  
                  <form method="POST" action="/agama/{{$agama->id}}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Nama Agama -->
                              <div class="form-group">
                                    <label for="nm_agama">Nama Surat</label>
                                    <input type="text" value="{{ old('nm_agama', $agama->nm_agama) }}" class="form-control @error('nm_agama') is-invalid @enderror" name="nm_agama" id="nm_agama" placeholder="Masukan nama agama">
                                    @error('nm_agama')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/agama">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection