@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Agama</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Agama -->
                  <div class="card-header">
                        <h3 class="card-title">Agama</h3>
                  </div>
                  
                  
                  <form method="POST" action="{{ url('/agama') }}" enctype="multipart/form-data">
                  @csrf
                        <div class="card-body">
                              <!-- Nama Agama -->
                              <div class="form-group">
                                    <label for="nm_agama">Nama Agama</label>
                                    
                                    <input type="text" value="{{ old('nm_agama') }}" class="form-control @error('nm_agama') is-invalid @enderror" name="nm_agama" id="nm_agama" placeholder="Masukan nama agama">
                                    @error('nm_agama')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                  </form>
            </div>

            <br>

            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success">
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success">
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success">
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              
                              <div class="card-header">
                              <h3 class="card-title"></h3>

                              <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div>
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Agama</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1;
                                    @endphp
                                    @forelse( $agamas as $agama )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $agama->nm_agama }}</td>
                                                <td class="col-4">
                                                      <a href="/agama/{{$agama->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/agama/{{$agama->id}}" method="POST" id="delete{{ $agama->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $agama->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="4" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Agama
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection