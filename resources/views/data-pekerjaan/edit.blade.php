@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Pekerjaan</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Pekerjaan -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Pekerjaan</h3>
                  </div>
                  
                  
                  <form method="POST" action="/pkj/{{$pekerjaan->id}}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Nama Pekerjaan -->
                              <div class="form-group">
                                    <label for="nm_pkj">Nama Pekerjaan</label>
                                    <input type="text" value="{{ old('nm_pkj', $pekerjaan->nm_pkj) }}" class="form-control @error('nm_pkj') is-invalid @enderror" name="nm_pkj" id="nm_pkj" placeholder="Masukan nama pekerjaan">
                                    @error('nm_pkj')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/pkj">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection