@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Pendidikan</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Pendidikan -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Pendidikan</h3>
                  </div>
                  
                  
                  <form method="POST" action="/pdd/{{$pendidikan->id}}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Nama Pendidikan -->
                              <div class="form-group">
                                    <label for="nm_pdd">Nama Pendidikan</label>
                                    <input type="text" value="{{ old('nm_pdd', $pendidikan->nm_pdd) }}" class="form-control @error('nm_pdd') is-invalid @enderror" name="nm_pdd" id="nm_pdd" placeholder="Masukan nama pendidikan">
                                    @error('nm_pdd')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/pdd">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection