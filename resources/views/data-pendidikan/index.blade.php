@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Pendidikan</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Pendidikan -->
                  <div class="card-header">
                        <h3 class="card-title">Pendidikan</h3>
                  </div>
                  
                  
                  <form method="POST" action="{{ url('/pdd') }}" enctype="multipart/form-data">
                  @csrf
                        <div class="card-body">
                              <!-- Nama Pendidikan -->
                              <div class="form-group">
                                    <label for="nm_pdd">Nama Pendidikan</label>
                                    
                                    <input type="text" value="{{ old('nm_pdd') }}" class="form-control @error('nm_pdd') is-invalid @enderror" name="nm_pdd" id="nm_pdd" placeholder="Masukan nama pendidikan">
                                    @error('nm_pdd')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                  </form>
            </div>

            <br>

            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              
                              <div class="card-header">
                              <h3 class="card-title"></h3>

                              <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div>
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Pendidikan</th>
                                                <th>Dibuat</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $pendidikans as $pendidikan )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $pendidikan->nm_pdd }}</td>
                                                <td>{{ $pendidikan->created_at->format('d M, Y')}}</td>
                                                
                                                <td>
                                                      <a href="/pdd/{{$pendidikan->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/pdd/{{$pendidikan->id}}" method="POST" id="delete{{ $pendidikan->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $pendidikan->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="4" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Pendidikan
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection