@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Surat</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Surat -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Surat</h3>
                  </div>
                  
                  
                  <form method="POST" action="/surat/{{$surat->id}}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Nama Surat -->
                              <div class="form-group">
                                    <label for="nm_surat">Nama Surat</label>
                                    
                                    <input type="text" value="{{ old('nm_surat', $surat->nm_surat) }}" class="form-control @error('nm_surat') is-invalid @enderror" name="nm_surat" id="nm_surat" placeholder="Masukan nama surat">
                                    @error('nm_surat')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Kode Surat -->
                              <div class="form-group">
                                    <label for="kd_surat">Kode Surat</label>
                                    <input type="text" value="{{ old('kd_surat', $surat->kd_surat) }}" class="form-control @error('kd_surat') is-invalid @enderror" name="kd_surat" id="kd_surat" placeholder="Masukan kode surat">
                                    @error('kd_surat')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/surat">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection