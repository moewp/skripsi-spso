@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Surat</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Surat -->
                  <div class="card-header">
                        <h3 class="card-title">Tambah Surat</h3>
                  </div>
                  
                  
                  <form method="POST" action="{{ url('/surat') }}" enctype="multipart/form-data">
                  @csrf
                        <div class="card-body">
                              <!-- Nama Surat -->
                              <div class="form-group">
                                    <label for="nm_surat">Nama Surat</label>
                                    
                                    <input type="text" value="{{ old('nm_surat') }}" class="form-control @error('nm_surat') is-invalid @enderror" name="nm_surat" id="nm_surat" placeholder="Masukan nama surat">
                                    @error('nm_surat')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Kode Surat -->
                              <div class="form-group">
                                    <label for="kd_surat">Kode Surat</label>
                                    <input type="text" value="{{ old('kd_surat') }}" class="form-control @error('kd_surat') is-invalid @enderror" name="kd_surat" id="kd_surat" placeholder="Masukan kode surat" maxlength="6">
                                    @error('kd_surat')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                  </form>
            </div>

            <br>

            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              
                              <div class="card-header">
                              <h3 class="card-title"></h3>

                              <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div>
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Surat</th>
                                                <th>Kode Surat</th>
                                                <th>Dibuat</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $surats as $surat )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $surat->nm_surat }}</td>
                                                <td>{{ $surat->kd_surat }}</td>
                                                <td>{{ $surat->created_at->format('d M, Y')}}</td>
                                                
                                                <td>
                                                      <a href="/surat/{{$surat->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/surat/{{$surat->id}}" method="POST" id="delete{{ $surat->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $surat->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="4" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Surat
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection