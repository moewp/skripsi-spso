@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Formulir Permohonan KTP</h1>
                  </div>
                  <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">Formulir Permohonan KTP</li>
                        </ol>
                  </div>
            </div>
      </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<div class="container">
      <div class="container">
            <div class="card card-info">
                  <div class="card-header bg-warning">
                        <h3 class="card-title">Isi Data Diri dengan Benar Sesuai KK</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form class="form-horizontal" method="POST" action="{{ url('/fpktp') }}" enctype="multipart/form-data">
                  @csrf
                        <div class="card-body">
                        
                              <!-- NIK NO KK -->
                              <!-- <div class="form-group row"> -->
                                    <!-- NIK -->
                                    <!-- <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input type="number" value="{{ old('nik') }}" class="form-control @error ('nik') is-invalid @enderror" id="nik" placeholder="NIK" name="nik" required>
                                          @error('nik')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div> -->
                                    <!-- NO KK -->
                                    <!-- <label for="nkk" class="col-sm-2 col-form-label">No.KK</label>
                                    <div class="col-sm-3">
                                          <input type="number" value="{{ old('nkk') }}" class="form-control @error ('nkk') is-invalid @enderror" id="nkk" placeholder="No KK" name="nkk" required>
                                          @error('nkk')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div> -->

                              <!-- Nama Lengkap -->
                              <!-- <div class="form-group row">
                                    <label for="nm_warga" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('nm_warga') }}" class="form-control @error ('nm_warga') is-invalid @enderror" id="nm_warga" placeholder="Nama Lengkap" name="nm_warga" required>
                                          @error('nm_warga')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div> -->
                              
                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat_dom" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat_dom') }}" class="form-control @error ('alamat_dom') is-invalid @enderror" id="alamat_dom" placeholder="Alamat" name="alamat_dom" required>
                                          @error('alamat_dom')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <!-- <div class="form-group row"> -->
                                    <!-- RT -->
                                    <!-- <div class="col-sm-2"></div>
                                    <label for="rt" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="{{ old('rt') }}" class="form-control @error ('rt') is-invalid @enderror" id="rt" placeholder="contoh: 012" name="rt" maxlength="3" required>
                                          @error('rt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div> -->
                                    <!-- RW -->
                                    <!-- <div class="col-sm-1"></div>                                    
                                    <label for="rw" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="{{ old('rw') }}" class="form-control @error ('rw') is-invalid @enderror" id="rw" placeholder="contoh: 005" name="rw" maxlength="3" required>
                                          @error('rw')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              

                              <br><hr> -->
                              <div class="form-group row">Lampiran: </div>
                              <!-- KK -->
                              <div class="form-group row">
                                    <label for="file_kk" class="col-sm-2 col-form-label">KK</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_kk') is-invalid @enderror" id="file_kk" name="file_kk" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_kk')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- SKLRS -->
                              <div class="form-group row">
                                    <label for="file_sklrs" class="col-sm-2 col-form-label">SKLRS</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_sklrs') is-invalid @enderror" id="file_sklrs" name="file_sklrs" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_sklrs')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- KKPS -->
                              <div class="form-group row">
                                    <label for="file_kkps" class="col-sm-2 col-form-label">KKPS</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_kkps') is-invalid @enderror" id="file_kkps" name="file_kkps" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_kkps')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- KTP Ay -->
                              <div class="form-group row">
                                    <label for="file_ktpay" class="col-sm-2 col-form-label">KTP Ay</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_ktpay') is-invalid @enderror" id="file_ktpay" name="file_ktpay" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_ktpay')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- KTP Ib -->
                              <div class="form-group row">
                                    <label for="file_ktpib" class="col-sm-2 col-form-label">KTP Ib</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_ktpib') is-invalid @enderror" id="file_ktpib" name="file_ktpib" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_ktpib')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- SKMT -->
                              <div class="form-group row">
                                    <label for="file_skmt" class="col-sm-2 col-form-label">SKMT</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_skmt') is-invalid @enderror" id="file_skmt" name="file_skmt" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_skmt')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- PP -->
                              <div class="form-group row">
                                    <label for="file_pp" class="col-sm-2 col-form-label">PP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_pp') is-invalid @enderror" id="file_pp" name="file_pp" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_pp')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Buku Nikah -->
                              <div class="form-group row">
                                    <label for="file_bn" class="col-sm-2 col-form-label">Buku Bikah</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_bn') is-invalid @enderror" id="file_bn" name="file_bn" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_bn')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file_ktp" class="col-sm-2 col-form-label">KTP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_ktp') is-invalid @enderror" id="file_ktp" name="file_ktp" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_ktp')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Pengantar RT/RW -->
                              <div class="form-group row">
                                    <label for="file_rtrw" class="col-sm-2 col-form-label">Pengantar RT/RW</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_rtrw') is-invalid @enderror" id="file_rtrw" name="file_rtrw" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_rtrw')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- SKMRS -->
                              <div class="form-group row">
                                    <label for="file_skmrs" class="col-sm-2 col-form-label">Skmrs</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_skmrs') is-invalid @enderror" id="file_skmrs" name="file_skmrs" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_skmrs')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Pengantar RT/RW -->
                              <!-- <div class="form-group row">
                                    <label for="file_rtrw" class="col-sm-2 col-form-label">Pengantar RT/RW</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_rtrw') is-invalid @enderror" id="file_rtrw" name="file_rtrw" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_rtrw')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div> -->
                        
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                              <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                        <!-- /.card-footer -->
                  </form>
            </div>
      </div>
</div>
<!-- /.content -->
@endsection