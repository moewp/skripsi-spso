<html lang="en">

<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>AdminLTE 3 | Log in</title>

      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
      <!-- icheck bootstrap -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css')}}">
</head>

<body class="login-page" style="min-height: 496.396px;">
      <div class="login-box">
            <div class="login-logo">
                  <a href="{{ url('landing')}}">
                        <div class="image">
                              <img src="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png')}}" class="" style="height: 60px;" alt="User Image">
                        </div>
                  </a>
                  desa<b> Sasakpanjang</b>
                  <p style="font-size: 14pt">sistem informasi pembuatan surat</p>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                  <div class="card-body login-card-body">
                        <p class="login-box-msg">Login</p>
                        <form action="{{ route('login') }}" method="POST">
                        @csrf
                              <div class="input-group mb-3" for="email">
                                    <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                          <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                          </span>
                                    @enderror
                                    <div class="input-group-append">
                                          <div class="input-group-text">
                                                <span class="fas fa-envelope"></span>
                                          </div>
                                    </div>
                              </div>
                              <div class="input-group mb-3" for="password">
                                    <input type="password" placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    @error('password')
                                          <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                          </span>
                                    @enderror
                                    <div class="input-group-append">
                                          <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                          </div>
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-8">
                                          <div class="icheck-primary">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label lass="form-check-label" for="remember">
                                                      Remember Me
                                                </label>
                                          </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-4">
                                          <button type="submit" class="btn btn-primary btn-block">Login</button>

                                          @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                                </a>
                                          @endif
                                    </div>
                                    <!-- /.col -->
                              </div>
                        </form>

                        

                        <p class="mb-1">
                              <a href="forgot-password.html">Lupa password</a>
                        </p>
                        <p class="mb-0">
                              <a href="{{ url('regis')}}" class="text-center">Daftar pengguna baru</a>
                        </p>
                  </div>
                  <!-- /.login-card-body -->
            </div>
      </div>
      <!-- /.login-box -->

      <!-- jQuery -->
      <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap 4 -->
      <script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
      <!-- AdminLTE App -->
      <script src="{{ asset('/adminlte/dist/js/adminlte.min.js')}}"></script>


</body>

</html>