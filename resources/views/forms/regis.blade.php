<html lang="en">
      <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>AdminLTE 3 | Registration Page</title>

      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
      <!-- icheck bootstrap -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css')}}">
      </head>

      <body class="register-page" style="min-height: 570.396px;">
            <div class="register-box">
                  <div class="register-logo">
                        <a href="{{ url('/')}}">
                              <div class="image">
                                    <img src="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png')}}" class="" style="height: 60px;" alt="User Image">
                              </div>
                        </a>
                        desa<b> Sasakpanjang</b>
                        <p style="font-size: 14pt">sistem informasi pembuatan surat</p>
                  </div>

                  <div class="card">
                        <div class="card-body register-card-body">
                              <p class="login-box-msg">Registrasi akun baru</p>
                              <form action="{{ route('register') }}" method="POST">
                              @csrf
                                    <div class="input-group mb-3" for="name">
                                          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">
                                          @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                          <div class="input-group-append">
                                                <div class="input-group-text">
                                                      <span class="fas fa-user"></span>
                                                </div>
                                          </div>
                                    </div>
                              
                                    <div class="input-group mb-3" for="email">
                                          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                                          @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                          <div class="input-group-append">
                                                <div class="input-group-text">
                                                      <span class="fas fa-envelope"></span>
                                                </div>
                                          </div>
                                    </div>

                                    <div class="input-group mb-3" for="password">
                                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
                                          @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                          <div class="input-group-append">
                                                <div class="input-group-text">
                                                      <span class="fas fa-lock"></span>
                                                </div>
                                          </div>
                                    </div>

                                    <div class="input-group mb-3" for="password-confirm">
                                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm password">
                                          <div class="input-group-append">
                                                <div class="input-group-text">
                                                      <span class="fas fa-lock"></span>
                                                </div>
                                          </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                          <div class="col-4">
                                                <button type="submit" class="btn btn-primary btn-block float-right">submit</button>
                                          </div>
                                    </div>
                                    <br>
                                    Sudah punya akun?
                                    <a href="{{ route('login') }}" class="text-center">login di sini</a>
                              </form>
                        </div>
                  <!-- /.form-box -->
                  </div><!-- /.card -->
            </div>
            <!-- /.register-box -->

            <!-- jQuery -->
      <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap 4 -->
      <script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
      <!-- AdminLTE App -->
      <script src="{{ asset('/adminlte/dist/js/adminlte.min.js')}}"></script>

      </body>
</html>