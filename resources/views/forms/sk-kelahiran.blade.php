@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Surat Keterangan Kelahiran</h1>
                  </div>
                  <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">Surat Keterangan Kelahiran</li>
                        </ol>
                  </div>
            </div>
      </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<div class="container">
      <div class="container">
            <div class="card card-info">
                  <div class="card-header bg-warning">
                        <h3 class="card-title">Isi Data Diri dengan Benar Sesuai KTP</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form class="form-horizontal">
                        <div class="card-body">
                              <div class="form-group row">1. Pemohon</div>

                              <!-- NIK -->
                              <div class="form-group row">
                                    <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik') is-invalid @enderror" id="nik" placeholder="NIK" name="nik">
                                    </div>
                              </div>

                              <!-- Nama Lengkap -->
                              <div class="form-group row">
                                    <label for="nm_wg" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_wg') is-invalid @enderror" id="nm_wg" placeholder="Nama Lengkap" name="nm_wg">
                                    </div>
                              </div>

                              <!-- TTL -->
                              <div class="form-group row">

                                    <!-- Tempat Lahir -->
                                    <label for="tmp_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tmp_lahir') is-invalid @enderror" id="tmp_lahir" name="tmp_lahir" placeholder="Tempat Lahir">
                                    </div>

                                    <!-- Tanggal Lahir -->
                                    <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tgl_lahir') is-invalid @enderror"  name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>
                              
                              <!-- Kwn Agama -->
                              <div class="form-group row">
                                    <!-- Kewarganegaraan -->
                                    <label for="kwn" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kwn') is-invalid @enderror" name="kwn" id="kwn" placeholder="Kewarganegaraan">
                                    </div>

                                    <!-- Agama -->
                                    <label for="nm_agama" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="nm_agama" name="nm_agama">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan -->
                              <div class="form-group row">
                                    <label for="nm_pkj" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_pkj') is-invalid @enderror" name="nm_pkj" id="nm_pkj" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat">
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt') is-invalid @enderror" id="rt" placeholder="contoh: 015" name="rt" maxlength="3">
                                    </div>

                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw') is-invalid @enderror" id="rw" placeholder="contoh: 005" name="rw" maxlength="3">
                                    </div>
                              </div>
                        </div>

                        <hr>
                        <div class="card-body">
                              <div class="form-group row">2. Pasangan</div>

                              <!-- NIK Sutri -->
                              <div class="form-group row">
                                    <label for="nik_sutri" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik_sutri') is-invalid @enderror" id="nik_sutri" placeholder="NIK" name="nik_sutri">
                                    </div>
                              </div>

                              <!-- Nama Lengkap Sutri -->
                              <div class="form-group row">
                                    <label for="nm_sutri" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_sutri') is-invalid @enderror" id="nm_sutri" placeholder="Nama Lengkap" name="nm_sutri">
                                    </div>
                              </div>

                              <!-- TTL Sutri -->
                              <div class="form-group row">

                                    <!-- Tempat Lahir -->
                                    <label for="tplahir_sutri" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tplahir_sutri') is-invalid @enderror" id="tplahir_sutri" name="tplahir_sutri" placeholder="Tempat Lahir">
                                    </div>

                                    <!-- Tanggal Lahir -->
                                    <label for="tglahir_sutri" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tglahir_sutri') is-invalid @enderror"  name="tglahir_sutri" id="tglahir_sutri" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>
                              
                              <!-- Kwn Agama Sutri-->
                              <div class="form-group row">

                                    <!-- Kewarganegaraan -->
                                    <label for="kwn_sutri" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kwn_sutri') is-invalid @enderror" name="kwn_sutri" id="kwn_sutri" placeholder="Kewarganegaraan">
                                    </div>

                                    <!-- Agama -->
                                    <label for="agama_sutri" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="agama_sutri" name="agama_sutri">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan Sutri -->
                              <div class="form-group row">
                                    <label for="pkj_sutri" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('pkj_sutri') is-invalid @enderror" name="pkj_sutri" id="pkj_sutri" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat Sutri-->
                              <div class="form-group row">
                                    <label for="alamat_sutri" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat_sutri') is-invalid @enderror" id="alamat_sutri" placeholder="Alamat Pasangan" name="alamat">
                                    </div>
                              </div>

                              <!-- RT RW Sutri-->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt_sutri" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt_sutri') is-invalid @enderror" id="rt_sutri" placeholder="contoh: 015" name="rt_sutri" maxlngth="3">
                                    </div>

                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw_sutri" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw_sutri') is-invalid @enderror" id="rw_sutri" placeholder="contoh: 005" name="rw_sutri" maxlength="3">
                                    </div>
                              </div>

                        <hr>
                        <div class="card-body">
                              <div class="form-group row">3. Anak</div>

                              <!-- Nama Lengkap Anak -->
                              <div class="form-group row">
                                    <label for="nm_anak" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_anak') is-invalid @enderror" id="nm_anak" placeholder="Nama Lengkap" name="nm_anak">
                                    </div>
                              </div>

                              <!-- Jenis Kelamin Anak-->
                              <div class="form-group row">
                                    <label label for="jk_anak" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="jk_anak" name="jk_anak">
                                                <option value="">Silakan Pilih</option>
                                                <option value="laki-laki">Laki-Laki</option>
                                                <option value="perempuan">Perempuan</option>
                                          </select>
                                    </div>

                                    <!-- Anak ke -->
                                    <label for="anak_ke" class="col-sm-2 col-form-label">Anak Ke-</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('anak_ke') is-invalid @enderror" name="anak_ke" id="anak_ke" placeholder="contoh: 2" maxlength="2">
                                    </div>
                              </div>
                              
                              <!-- TTL Anak -->
                              <div class="form-group row">

                                    <!-- Tempat Lahir -->
                                    <label for="tplahir_anak" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tplahir_anak') is-invalid @enderror" id="tplahir_anak" name="tplahir_anak" placeholder="Tempat Lahir">
                                    </div>

                                    <!-- Tanggal Lahir -->
                                    <label for="tglahir_anak" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tglahir_anak') is-invalid @enderror"  name="tglahir_anak" id="tglahir_anak" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>
                              
                              <br><hr>
                              <div class="form-group row">Lampiran: </div>

                              <!-- KK -->
                              <div class="form-group row">
                                    <label for="file-kk" class="col-sm-2 col-form-label">KK</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-kk" name="file-kk">
                                          <label class="custom-file-label" for="file-kk">Pilih file</label>
                                    </div>
                              </div>

                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file-ktp" class="col-sm-2 col-form-label">KTP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-ktp" name="file-ktp">
                                          <label class="custom-file-label" for="file-ktp">Pilih file</label>
                                    </div>
                              </div>

                              <!-- Pengantar RT/RW -->
                              <div class="form-group row">
                                    <label for="file-rtrw" class="col-sm-2 col-form-label">Pengantar RT/RW</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-rtrw" name="file-rtrw">
                                          <label class="custom-file-label" for="file-rtrw">Pilih file</label>
                                    </div>
                              </div>

                              <!-- Buku Nikah -->
                              <div class="form-group row">
                                    <label for="file-bn" class="col-sm-2 col-form-label">Buku Nikah</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-bn" name="file-bn">
                                          <label class="custom-file-label" for="file-bn">Pilih file</label>
                                    </div>
                              </div>

                              <!-- Surat Keterangan RS -->
                              <div class="form-group row">
                                    <label for="file-sklrs" class="col-sm-2 col-form-label">Surat Keterangan Kelahiran RS</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-sklrs" name="file-sklrs">
                                          <label class="custom-file-label" for="file-sklrs">Pilih file</label>
                                    </div>
                              </div>
                        </div>

                        <!-- /.card-body -->
                        <div class="card-footer">
                              <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                        <!-- /.card-footer -->
                  </form>
            </div>
      </div>
</div>
<!-- /.content -->
@endsection