@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Surat Pengantar Perkawinan</h1>
                  </div>
                  <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">Surat Pengantar Perkawinan</li>
                        </ol>
                  </div>
            </div>
      </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<div class="container">
      <div class="container">
            <div class="card card-info">
                  <div class="card-header bg-warning">
                        <h3 class="card-title">Isi Data Diri dengan Benar Sesuai KTP</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form class="form-horizontal">
                        <div class="card-body">
                        
                              <!-- NIK -->
                              <div class="form-group row">
                                    <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik') is-invalid @enderror" id="nik" placeholder="NIK" name="nik">
                                    </div>
                              </div>

                              <!-- Nama Lengkap -->
                              <div class="form-group row">
                                    <label for="nm_wg" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_wg') is-invalid @enderror" id="nm_wg" placeholder="Nama Lengkap" name="nm_wg">
                                    </div>
                              </div>

                              <!-- TTL -->
                              <div class="form-group row">

                                    <!-- Tempat Lahir -->
                                    <label for="tmp_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tmp_lahir') is-invalid @enderror" id="tmp_lahir" name="tmp_lahir" placeholder="Tempat Lahir">
                                    </div>

                                    <!-- Tanggal Lahir -->
                                    <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tgl_lahir') is-invalid @enderror"  name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>

                              <!-- Kwn Agama -->
                              <div class="form-group row">
                                    <!-- Kewarganegaraan -->
                                    <label for="kwn" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kwn') is-invalid @enderror" name="kwn" id="kwn" placeholder="Kewarganegaraan">
                                    </div>

                                    <!-- Agama -->
                                    <label for="nm_agama" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="nm_agama" name="nm_agama">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan -->
                              <div class="form-group row">
                                    <label for="nm_pkj" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_pkj') is-invalid @enderror" name="nm_pkj" id="nm_pkj" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat">
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt') is-invalid @enderror" id="rt" placeholder="Harus Angka" name="rt" maxlength="3">
                                    </div>

                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw') is-invalid @enderror" id="rw" placeholder="Harus Angka" name="rw" maxlength="3">
                                    </div>
                              </div>

                              <!-- Status Pernikahan -->
                              <div class="form-group row">
                                    <label for="status_nkh" class="col-sm-2 col-form-label">Status Pernikahan</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="status_nkh" name="status_nkh">
                                                <option value="">Silakan Pilih</option>
                                                <option value="jejaka">Jejaka</option>
                                                <option value="perawan">Perawan</option>
                                                <option value="duda">Duda</option>
                                                <option value="janda">Janda</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Nama Suami/Istri Terdahulu -->
                              <div class="form-group row">
                                    <label for="mtn_sutri" class="col-sm-2 col-form-label">Nama Suami/Istri Terdahulu</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('mtn_sutri') is-invalid @enderror" name="mtn_sutri" id="mtn_sutri" placeholder="Kosongkan jika pilih jejaka/perawan">
                                    </div>
                              </div>
                        </div>

                        <!-- Ayah Ibu -->
                        <hr>

                        <!-- Ayah -->
                        <div class="card-body">
                              <div class="form-group row">2. AYAH</div>

                              <!-- NIK Ayah -->
                              <div class="form-group row">
                                    <label for="nik_ay" class="col-sm-2 col-form-label">NIK Ayah</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik_ay') is-invalid @enderror" id="nik_ay" placeholder="NIK Ayah" name="nik_ay">
                                    </div>
                              </div>

                              <!-- Nama Lengkap Ayah -->
                              <div class="form-group row">
                                    <label for="nm_ay" class="col-sm-2 col-form-label">Nama Ayah</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_ay') is-invalid @enderror" id="nm_ay" placeholder="Nama Ayah" name="nm_ay">
                                    </div>
                              </div>

                              <!-- Bin Ayah -->
                              <div class="form-group row">
                                    <label for="bin_ay" class="col-sm-2 col-form-label">Bin dari Ayah</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('bin_ay') is-invalid @enderror" id="bin_ay" name="bin_ay" placeholder="Bin dari Ayah">
                                    </div>
                              </div>
                              
                              <!-- TTL -->
                              <div class="form-group row">

                                    <!-- Tempat Lahir -->
                                    <label for="tplahir_ay" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tplahir_ay') is-invalid @enderror" id="tplahir_ay" name="tplahir_ay" placeholder="Tempat Lahir">
                                    </div>

                                    <!-- Tanggal Lahir -->
                                    <label for="tglahir_ay" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tglahir_ay') is-invalid @enderror"  name="tglahir_ay" id="tglahir_ay" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>

                              <!-- Kwn Agama -->
                              <div class="form-group row">

                                    <!-- Kewarganegaraan -->
                                    <label for="kwn_ay" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kwn_ay') is-invalid @enderror" name="kwn_ay" id="kwn_ay" placeholder="Kewarganegaraan">
                                    </div>

                                    <!-- Agama -->
                                    <label for="agama_ay" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="agama_ay" name="agama_ay">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan Ayah -->
                              <div class="form-group row">
                                    <label for="pkj_ay" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('pkj_ay') is-invalid @enderror" name="pkj_ay" id="pkj_ay" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat Ayah -->
                              <div class="form-group row">
                                    <label for="alamat_ay" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat_ay') is-invalid @enderror" id="alamat_ay" placeholder="Alamat" name="alamat_ay">
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt_ay" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt_ay') is-invalid @enderror" id="rt_ay" placeholder="contoh: 015" name="rt_ay" maxlength="3">
                                    </div>

                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw_ay" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw_ay') is-invalid @enderror" id="rw_ay" placeholder="contoh: 005" name="rw_ay" maxlength="3">
                                    </div>
                              </div>
                        </div>

                        <div class="card-body">
                              <div class="form-group row">3. IBU</div>

                              <!-- NIK Ibu -->
                              <div class="form-group row">
                                    <label for="nik_ib" class="col-sm-2 col-form-label">NIK Ibu</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik_ib') is-invalid @enderror" id="nik_ib" placeholder="NIK Ibu" name="nik_ib">
                                    </div>
                              </div>

                              <!-- Nama Lengkap Ibu -->
                              <div class="form-group row">
                                    <label for="nm_ib" class="col-sm-2 col-form-label">Nama Ibu</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nm_ib') is-invalid @enderror" id="nm_ib" placeholder="Nama Ibu" name="nm_ib">
                                    </div>
                              </div>

                              <!-- Bin Ibu -->
                              <div class="form-group row">
                                    <label for="bin_ib" class="col-sm-2 col-form-label">Bin dari Ibu</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('bin_ib') is-invalid @enderror" id="bin_ib" name="bin_ib" placeholder="Bin dari Ibu">
                                    </div>
                              </div>
                              
                              <!-- TTL -->
                              <div class="form-group row">

                                    <!-- Tempat Lahir -->
                                    <label for="tplahir_ib" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tplahir_ib') is-invalid @enderror" id="tplahir_ib" name="tplahir_ib" placeholder="Tempat Lahir">
                                    </div>

                                    <!-- Tanggal Lahir -->
                                    <label for="tglahir_ib" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tglahir_ib') is-invalid @enderror"  name="tglahir_ib" id="tglahir_ib" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>

                              <!-- Kwn Agama -->
                              <div class="form-group row">

                                    <!-- Kewarganegaraan -->
                                    <label for="kwn_ib" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kwn_ib') is-invalid @enderror" name="kwn_ib" id="kwn_ib" placeholder="Kewarganegaraan">
                                    </div>

                                    <!-- Agama -->
                                    <label for="agama_ib" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="agama_ib" name="agama_ib">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan Ibu -->
                              <div class="form-group row">
                                    <label for="pkj_ib" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('pkj_ib') is-invalid @enderror" name="pkj_ib" id="pkj_ib" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat Ibu -->
                              <div class="form-group row">
                                    <label for="alamat_ib" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat_ib') is-invalid @enderror" id="alamat_ib" placeholder="Alamat" name="alamat_ib">
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt_ib" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt_ib') is-invalid @enderror" id="rt_ib" placeholder="contoh: 015" name="rt_ib" maxlength="3">
                                    </div>

                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw_ib" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw_ib') is-invalid @enderror" id="rw_ib" placeholder="contoh: 005" name="rw_ib" maxlength="3">
                                    </div>
                              </div>
                              
                              <hr>

                              <div class="form-group row">Lampiran: </div>

                              <!-- KK -->
                              <div class="form-group row">
                                    <label for="file-kk" class="col-sm-2 col-form-label">KK</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-kk" name="file-kk">
                                          <label class="custom-file-label" for="file-kk">Pilih file</label>
                                    </div>
                              </div>

                              <!-- KK pasangan -->
                              <div class="form-group row">
                                    <label for="file-kkps" class="col-sm-2 col-form-label">KK Calon Pasangan</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-kkps" name="file-kkps">
                                          <label class="custom-file-label" for="file-kkps">Pilih file</label>
                                    </div>
                              </div>

                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file-ktp" class="col-sm-2 col-form-label">KTP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-ktp" name="file-ktp">
                                          <label class="custom-file-label" for="file-ktp">Pilih file</label>
                                    </div>
                              </div>

                              <!-- Pengantar RT/RW -->
                              <div class="form-group row">
                                    <label for="file-rtrw" class="col-sm-2 col-form-label">Pengantar RT/RW</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-rtrw" name="file-rtrw">
                                          <label class="custom-file-label" for="file-rtrw">Pilih file</label>
                                    </div>
                              </div>
                              

                              <!-- KTP Ortu -->
                              <br>
                              <div class="form-group row">Jika tidak satu KK dengan orangtua lampirkan KTP orangtua: </div>
                              
                              <!-- KTP Ayah -->
                              <div class="form-group row">
                                    <label for="file-ktpay" class="col-sm-2 col-form-label">KTP Ayah</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-ktpay" name="file-ktpay">
                                          <label class="custom-file-label" for="file-ktpay">Pilih file</label>
                                    </div>
                              </div>

                              <!-- KTP Ibu -->
                              <div class="form-group row">
                                    <label for="file-ktpib" class="col-sm-2 col-form-label">KTP Ibu</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-ktpib" name="file-ktpib">
                                          <label class="custom-file-label" for="file-ktpib">Pilih file</label>
                                    </div>
                              </div>
                        </div>      

                        <!-- /.card-body -->
                        <div class="card-footer">
                              <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                        <!-- /.card-footer -->
                  </form>
            </div>
      </div>
</div>
<!-- /.content -->
@endsection