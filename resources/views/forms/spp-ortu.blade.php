                              
                              <!-- Nama Suami/Istri Terdahulu -->
                              <div class="form-group row">
                                    <label for="nama-sutri" class="col-sm-2 col-form-label">Nama Suami/Istri Terdahulu</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control" name="nama-sutri" id="nama-sutri" placeholder="Kosongkan jika pilih jejaka/perawan">
                                    </div>
                              </div>
                              

                              <!-- AYAH IBU -->
                              <div class="form-group row">2. AYAH</div>
                              <!-- NIK Ayah -->
                              <div class="form-group row">
                                    <label for="nik-ayah" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik-ayah') is-invalid @enderror" id="nik-ayah" placeholder="NIK" name="nik-ayah">
                                    </div>
                              </div>

                              <!-- Nama Lengkap Ayah -->
                              <div class="form-group row">
                                    <label for="nama-ayah" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nama-ayah') is-invalid @enderror" id="nama-ayah" placeholder="Nama Lengkap" name="nama-ayah">
                                    </div>
                              </div>

                              <!-- Bin Ayah -->
                              <div class="form-group row">
                                    <label for="bin-ayah" class="col-sm-2 col-form-label">Bin</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('bin-ayah') is-invalid @enderror" id="bin-ayah" name="bin-ayah" placeholder="Bin/Nama Ayah">
                                    </div>
                              </div>
                              
                              <!-- TTL Ayah -->
                              <div class="form-group row">
                                    <!-- Tempat Lahir -->
                                    <label for="tempat-lahir-ayah" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tempat-lahir-ayah') is-invalid @enderror" id="tempat-lahir-ayah" name="tempat-lahir-ayah" placeholder="Tempat Lahir">
                                    </div>
                                    <!-- Tanggal Lahir -->
                                    <label for="tanggal-lahir-ayah" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tanggal-lahir-ayah') is-invalid @enderror"  name="tanggal-lahir-ayah" id="tanggal-lahir-ayah" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>

                              <!-- Kwn Agama Ayah-->
                              <div class="form-group row">
                                    <!-- Kewarganegaraan Ayah -->
                                    <label for="kewarganegaraan-ayah" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kewarganegaraan-ayah') is-invalid @enderror" name="kewarganegaraan-ayah" id="kewarganegaraan-ayah" placeholder="Kewarganegaraan">
                                    </div>
                                    <!-- Agama Ayah-->
                                    <label for="agama-ayah" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="agama-ayah" name="agama-ayah">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan Ayah -->
                              <div class="form-group row">
                                    <label for="pekerjaan-ayah" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('pekerjaan-ayah') is-invalid @enderror" name="pekerjaan-ayah" id="pekerjaan-ayah" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat Ayah -->
                              <div class="form-group row">
                                    <label for="alamat-ayah" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat-ayah') is-invalid @enderror" id="alamat-ayah" placeholder="Alamat" name="alamat-ayah">
                                    </div>
                              </div>

                              <!-- RT RW Ayah-->
                              <div class="form-group row">
                                    <!-- RT Ayah-->
                                    <div class="col-sm-2"></div>
                                    <label for="rt-ayah" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt-ayah') is-invalid @enderror" id="rt-ayah" placeholder="Harus Angka" name="rt-ayah" maxlength="3">
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- RW Ayah -->
                                    <label for="rw-ayah" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw-ayah') is-invalid @enderror" id="rw-ayah" placeholder="Harus Angka" name="rw-ayah" maxlength="3">
                                    </div>
                              </div>
                        </div>

                        <hr>
                        <div class="card-body">
                              <div class="form-group row">3. IBU</div>
                              <!-- NIK Ibu -->
                              <div class="form-group row">
                                    <label for="nik-ibu" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input type="number" class="form-control @error ('nik-ibu') is-invalid @enderror" id="nik-ibu" placeholder="NIK" name="nik-ibu">
                                    </div>
                              </div>

                              <!-- Nama Lengkap Ibu -->
                              <div class="form-group row">
                                    <label for="nama-ibu" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('nama-ibu') is-invalid @enderror" id="nama-ibu" placeholder="Nama Lengkap" name="nama-ibu">
                                    </div>
                              </div>

                              <!-- Bin Ibu -->
                              <div class="form-group row">
                                    <label for="bin-ibu" class="col-sm-2 col-form-label">Bin</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('bin-ibu') is-invalid @enderror" id="bin-ibu" name="bin-ibu" placeholder="Bin/Nama Ayah">
                                    </div>
                              </div>
                              
                              <!-- TTL Ibu -->
                              <div class="form-group row">
                                    <!-- Tempat Lahir -->
                                    <label for="tempat-lahir-ibu" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" class="form-control @error ('tempat-lahir-ibu') is-invalid @enderror" id="tempat-lahir-ibu" name="tempat-lahir-ibu" placeholder="Tempat Lahir">
                                    </div>
                                    <!-- Tanggal Lahir -->
                                    <label for="tanggal-lahir-ibu" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="datepicker-here form-control @error ('tanggal-lahir-ibu') is-invalid @enderror"  name="tanggal-lahir-ibu" id="tanggal-lahir-ibu" placeholder="Tanggal Lahir" data-language='id' data-multiple-dates="1" data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                              </div>

                              <!-- Kwn Agama Ibu-->
                              <div class="form-group row">
                                    <!-- Kewarganegaraan Ayah -->
                                    <label for="kewarganegaraan-ibu" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('kewarganegaraan-ibu') is-invalid @enderror" name="kewarganegaraan-ibu" id="kewarganegaraan-ibu" placeholder="Kewarganegaraan">
                                    </div>
                                    <!-- Agama Ayah-->
                                    <label for="agama-ibu" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select class="form-control" id="agama-ibu" name="agama-ibu">
                                                <option>Islam</option>
                                                <option>Kristen Protestan</option>
                                                <option>Kristen Katolik</option>
                                                <option>Hindu</option>
                                                <option>Buddha</option>
                                                <option>Konghucu</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Pekerjaan Ibu -->
                              <div class="form-group row">
                                    <label for="pekerjaan-ibu" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('pekerjaan-ibu') is-invalid @enderror" name="pekerjaan-ibu" id="pekerjaan-ibu" placeholder="Pekerjaan">
                                    </div>
                              </div>

                              <!-- Alamat Ibu -->
                              <div class="form-group row">
                                    <label for="alamat-ibu" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                          <input type="text" class="form-control @error ('alamat-ibu') is-invalid @enderror" id="alamat-ibu" placeholder="Alamat" name="alamat-ibu">
                                    </div>
                              </div>

                              <!-- RT RW Ibu-->
                              <div class="form-group row">
                                    <!-- RT Ibu-->
                                    <div class="col-sm-2"></div>
                                    <label for="rt-ibu" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rt-ibu') is-invalid @enderror" id="rt-ibu" placeholder="Harus Angka" name="rt-ibu" maxlength="3">
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- RW Ibu -->
                                    <label for="rw-ibu" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input type="text" class="form-control @error ('rw-ibu') is-invalid @enderror" id="rw-ibu" placeholder="Harus Angka" name="rw-ayah" maxlength="3">
                                    </div>


                              <!-- KK -->
                              <div class="form-group row">Lampiran: </div>
                              <!-- KK pihak laki-laki -->
                              <div class="form-group row">
                                    <label for="file-kklk" class="col-sm-2 col-form-label">KK pihak laki-laki</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-kklk" name="file-kklk">
                                          <label class="custom-file-label" for="file-kklk">Pilih file</label>
                                    </div>
                              </div>
                              <!-- KK pihak Perempuan -->
                              <div class="form-group row">
                                    <label for="file-kkpr" class="col-sm-2 col-form-label">KK pihak perempuan</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-kkpr" name="file-kkpr">
                                          <label class="custom-file-label" for="file-kkpr">Pilih file</label>
                                    </div>
                              </div>

                              <!-- KTP Ortu -->
                              <br>
                              <div class="form-group row">Jika tidak satu KK dengan orangtua lampirkan KTP orangtua: </div>
                              <!-- KTP Ayah -->
                              <div class="form-group row">
                                    <label for="file-ktpa" class="col-sm-2 col-form-label">KTP Ayah</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-ktpa" name="file-ktpa">
                                          <label class="custom-file-label" for="file-ktpa">Pilih file</label>
                                    </div>
                              </div>
                              <!-- KTP Ibu -->
                              <div class="form-group row">
                                    <label for="file-ktpi" class="col-sm-2 col-form-label">KTP Ibu</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control" id="file-ktpi" name="file-ktpi">
                                          <label class="custom-file-label" for="file-ktpi">Pilih file</label>
                                    </div>
                              </div>