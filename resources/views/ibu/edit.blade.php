@extends('partials.master')

@section('content')

<div class="row mb-4"></div>
<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Profile -->
                  <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Data Ibu</h3>
                  </div>
                  <form method="POST" action="/ibu/{{ $ibu->id }}" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                        <div class="card-body">
                              <!-- Nama Ibu -->
                              <div class="form-group">
                                    <label for="nm_ib">Nama Ibu</label>
                                    <input type="text" value="{{ old('nm_ib', $ibu->nm_ib) }}" class="form-control @error('nm_ib') is-invalid @enderror" name="nm_ib" id="nm_ib" required>
                                    @error('nm_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- NIK -->
                              <div class="form-group">
                                    <label for="nik_ib">NIK</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik_ib', $ibu->nik_ib) }}" class="form-control @error('nik_ib') is-invalid @enderror" name="nik_ib" id="nik_ib" required>
                                    @error('nik_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- BIN -->
                              <div class="form-group">
                                    <label for="bin_ib">Bin dari Ibu</label>
                                    <input type ="text" maxlength="16" value="{{ old('bin_ib', $ibu->bin_ib) }}" class="form-control @error('bin_ib') is-invalid @enderror" name="bin_ib" id="bin_ib" required>
                                    @error('bin_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir -->
                              <div class="form-group">
                                    <label for="tplahir_ib">Tempat Lahir</label>
                                    <input type="text" value="{{ old('tplahir_ib', $ibu->tplahir_ib) }}" class="form-control @error('tplahir_ib') is-invalid @enderror" name="tplahir_ib" id="tplahir_ib" required>
                                    @error('tplahir_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir -->
                              <div class="form-group">
                                    <label for="tglahir_ib">Tanggal Lahir</label>
                                    <input class="form-control col-sm-2 @error('tglahir_ib') is-invalid @enderror" name="tglahir_ib" id="tglahir_ib" value="{{ old('tglahir_ib', $ibu->tglahir_ib->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tglahir_ib', $ibu->tglahir_ib) }}" class="form-control @error('tglahir_ib') is-invalid @enderror" name="tglahir_ib" id="tglahir_ib" required>
                                    @error('tglahir_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Kewarganegaraan -->
                              <div class="form-group">
                                    <label for="kwn_ib">Kewarganegaraan</label>
                                    <input type="text" value="{{ old('kwn_ib', $ibu->kwn_ib) }}" class="form-control @error('kwn_ib') is-invalid @enderror" name="kwn_ib" id="kwn_ib" required>
                                    @error('kwn_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Pekerjaan -->
                              <div class="form-group">
                                    <label for="nm_pkj">Pekerjaan</label>
                                    <select class="form-control @error ('nm_pkj') is-invalid @enderror" id="nm_pkj" name="nm_pkj" required>
                                                <option value="{{ old('nm_pkj', $ibu->pekerjaan->id) }}">{{ old('nm_pkj', $ibu->pekerjaan->nm_pkj) }}</option>
                                                @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_pkj')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Agama -->
                              <div class="form-group">
                                    <label for="nm_agama">Agama</label>
                                    <select class="form-control @error ('nm_agama') is-invalid @enderror" id="nm_agama" name="nm_agama" required>
                                                <option value="{{ old('nm_agama', $ibu->agama->id) }}">{{ old('nm_agama', $ibu->agama->nm_agama) }}</option>
                                                @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_agama')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat -->
                              <div class="form-group">
                                    <label for="alamat_ib">Alamat Ibu</label>
                                    <input type="text" value="{{ old('alamat_ib', $ibu->alamat_ib) }}" class="form-control @error('alamat_ib') is-invalid @enderror" name="alamat_ib" id="alamat_ib" required>
                                    @error('alamat_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT -->
                              <div class="form-group">
                                    <label for="rt_ib">RT</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_ib', $ibu->rt_ib) }}" class="form-control @error('rt_ib') is-invalid @enderror" name="rt_ib" id="rt_ib" maxlength="3" required>
                                    @error('rt_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW -->
                              <div class="form-group">
                                    <label for="rw_ib">RW</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_ib', $ibu->rw_ib) }}" class="form-control @error('rw_ib') is-invalid @enderror" name="rw_ib" id="rw_ib" maxlength="3" required>
                                    @error('rw_ib')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- KTP -->
                              <div class="form-group row mt-5">
                                    <label for="file_ktpib" class="col-md-3 col-form-label text-md-right">KTP</label>
                                    <div class="col-md-9">
                                          <img src="/images/ktpib/{{ $ibu->file_ktpib }}" width="70px">
                                          File Name : {{ $ibu->file_ktpib }}
                                          <input id="file_ktpib" type="file" class="form-control" name="file_ktpib" autofocus>
                                          @error('file_ktpib')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/ibu/{{$ibu->id}}">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection