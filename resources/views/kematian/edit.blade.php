@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Kematian</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Kematian -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Data Kematian</h3>
                  </div>
                  
                  <form method="POST" action="/kmt/{{$kematian->id}}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Hubungan dengan Almarhum -->
                              <div class="form-group">
                                    <label for="hubungan">Hubungan dengan Almarhum</label>
                                    <select class="form-control col-sm-3 @error ('hubungan') is-invalid @enderror" id="hubungan" name="hubungan" required>
                                                <option value="{{ old('hubungan', $kematian->hubungan) }}">{{ old('hubungan', $kematian->hubungan) }}</option>
                                                <option value="Orang tua">Orang tua</option>
                                                <option value="Anak">Anak</option>
                                                <option value="Suami">Suami</option>
                                                <option value="Istri">Istri</option>
                                                <option value="Saudara">Saudara</option>
                                                <option value="Kerabat">Kerabat</option>
                                          </select>
                                    @error('hubungan')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Sebab Meninggal -->
                              <div class="form-group">
                                    <label for="sebab_mgl">Sebab Meninggal</label>
                                    <input type="text" value="{{ old('sebab_mgl', $kematian->sebab_mgl) }}" class="form-control @error('sebab_mgl') is-invalid @enderror" name="sebab_mgl" id="sebab_mgl" placeholder="Masukan nama alm.">
                                    @error('sebab_mgl')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Nama Almarhum -->
                              <div class="form-group">
                                    <label for="nm_alm">Nama Almarhum</label>
                                    <input type="text" value="{{ old('nm_alm', $kematian->nm_alm) }}" class="form-control @error('nm_alm') is-invalid @enderror" name="nm_alm" id="nm_alm" placeholder="Masukan nama alm.">
                                    @error('nm_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- NIK Almarhum -->
                              <div class="form-group">
                                    <label for="nik_alm">NIK Almarhum</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik_alm', $kematian->nik_alm) }}" class="form-control @error('nik_alm') is-invalid @enderror" name="nik_alm" id="nik_alm" placeholder="Masukan nama alm.">
                                    @error('nik_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir Almarhum -->
                              <div class="form-group">
                                    <label for="tplahir_alm">Tempat Lahir Almarhum</label>
                                    <input type="text" value="{{ old('tplahir_alm', $kematian->tplahir_alm) }}" class="form-control @error('tplahir_alm') is-invalid @enderror" name="tplahir_alm" id="tplahir_alm" placeholder="Masukan nama anak">
                                    @error('tplahir_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir Almarhum -->
                              <div class="form-group">
                                    <label for="tglahir_alm">Tanggal Lahir Almarhum</label>
                                    <input class="form-control col-sm-2 @error('tglahir_alm') is-invalid @enderror" name="tglahir_alm" id="tglahir_alm" value="{{ old('tglahir_alm', $kematian->tglahir_alm->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tglahir_alm', $kematian->tglahir_alm) }}" class="form-control col-sm-3 @error('tglahir_alm') is-invalid @enderror" name="tglahir_alm" id="tglahir_alm" placeholder="Masukan nama anak" required>
                                    @error('tglahir_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jenis Kelamin Almarhum -->
                              <div class="form-group">
                                    <label for="jk">Jenis Kelamin Almarhum</label>
                                    <select class="form-control col-sm-3 @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="{{ old('jk', $kematian->jk) }}">{{ old('jk', $kematian->jk) }}</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    @error('jk')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat -->
                              <div class="form-group">
                                    <label for="alamat_alm">Alamat Almarhum</label>
                                    <input type="text" value="{{ old('alamat_alm', $kematian->alamat_alm) }}" class="form-control @error('alamat_alm') is-invalid @enderror" name="alamat_alm" id="alamat_alm" required>
                                    @error('alamat_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT -->
                              <div class="form-group">
                                    <label for="rt_alm">RT</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_alm', $kematian->rt_alm) }}" class="form-control @error('rt_alm') is-invalid @enderror" name="rt_alm" id="rt_alm" maxlength="3" required>
                                    @error('rt_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW -->
                              <div class="form-group">
                                    <label for="rw_alm">RW</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_alm', $kematian->rw_alm) }}" class="form-control @error('rw_alm') is-invalid @enderror" name="rw_alm" id="rw_alm" maxlength="3" required>
                                    @error('rw_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <hr>
                              <!-- Tempat Meninggal -->
                              <div class="form-group">
                                    <label for="tpmgl_alm">Tempat Meninggal</label>
                              @if (Auth::user()->kematian->tpmgl_alm != NULL)
                                    <input type="text" value="{{ old('tpmgl_alm', $kematian->tpmgl_alm) }}" class="form-control @error('tpmgl_alm') is-invalid @enderror" name="tpmgl_alm" id="tpmgl_alm">
                                    @error('tpmgl_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              @else
                                    <input type="text" class="form-control @error ('tpmgl_alm') is-invalid @enderror"  name="tpmgl_alm" id="tpmgl_alm" placeholder="Tanggal Meninggal"/>
                                    @error('tpmgl_alm')
                                          <p class="text-danger">{{ $message }}</p>
                                    @enderror
                              @endif
                              </div>
                              <!-- Tanggal Meninggal -->
                              <div class="form-group">
                                    <label for="tgmgl_alm">Tanggal Meninggal</label>
                              @if (Auth::user()->kematian->tgmgl_alm != NULL)
                                    <input class="form-control col-sm-2 @error('tgmgl_alm') is-invalid @enderror" name="tgmgl_alm" id="tgmgl_alm" value="{{ old('tgmgl_alm', $kematian->tgmgl_alm->format('d M Y')) }}">
                                    <input type="date" value="{{ old('tgmgl_alm', $kematian->tgmgl_alm) }}" class="form-control col-sm-3 @error('tgmgl_alm') is-invalid @enderror" name="tgmgl_alm" id="tgmgl_alm">
                                    @error('tgmgl_alm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              @else
                                    <input type="date" class="form-control col-sm-3 @error ('tgmgl_alm') is-invalid @enderror"  name="tgmgl_alm" id="tgmgl_alm" placeholder="Tanggal Meninggal"/>
                                    @error('tgmgl_alm')
                                          <p class="text-danger">{{ $message }}</p>
                                    @enderror
                              @endif
                              </div>
                              <hr>
                              <!-- SKLRS -->
                              <div class="form-group row mt-5">
                                    <label for="file_skmrs" class="col-md-3 col-form-label text-md-right">SK. Kelahiran RS.</label>
                                    <div class="col-md-9">
                                          <img src="/images/skmrs/{{ $kematian->file_skmrs }}" width="70px" alt="no file||">
                                          File Name : {{ $kematian->file_skmrs }}
                                          <input id="file_skmrs" type="file" class="form-control @error ('file_skmrs') is-invalid @enderror" name="file_skmrs" autofocus>
                                          @error('file_skmrs')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/anak">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>

@endsection