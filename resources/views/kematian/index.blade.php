@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Kematian</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Kematian -->
                  <div class="card-header">
                        <h3 class="card-title">Tambah Data Kematian</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/kmt') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::id()}}">
                        <div class="card-body">
                              <!-- Hubungan -->
                              <div class="form-group row">
                                    <label for="hubungan" class="col-sm-2 col-form-label">Hubungan</label>
                                    <div class="col-sm-3">
                                          <select class="form-control @error ('hubungan') is-invalid @enderror" id="hubungan" name="hubungan" required>
                                                <option value="">--Silakan Pilih--</option>
                                                <option value="Orang tua">Orang tua</option>
                                                <option value="Anak">Anak</option>
                                                <option value="Suami">Suami</option>
                                                <option value="Istri">Istri</option>
                                                <option value="Saudara">Saudara</option>
                                                <option value="Kerabat">Kerabat</option>
                                          </select>
                                    </div>
                              </div>
                              <!-- Nama -->
                              <div class="form-group row">
                                    <label for="nm_alm" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-6">
                                          <input type="text" class="form-control @error ('nm_alm') is-invalid @enderror" id="nm_alm" placeholder="Nama" name="nm_alm" required>
                                    </div>
                              </div>
                              <!-- NIK -->
                              <div class="form-group row">
                                    <label for="nik_alm" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-6">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" class="form-control @error ('nik_alm') is-invalid @enderror" id="nik_alm" placeholder="NIK" name="nik_alm" required>
                                    </div>
                              </div>
                              <!-- Tempat Lahir -->
                              <div class="form-group row">
                                    <label for="tplahir_alm" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-6">
                                          <input type="text" class="form-control @error ('tplahir_alm') is-invalid @enderror" id="tplahir_alm" placeholder="Tempat Lahir" name="tplahir_alm" required>
                                    </div>
                              </div>
                              <!-- Tanggal Lahir -->
                              <div class="form-group row">
                                    <label for="tglahir_alm" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-6">
                                          <input type="date" class="form-control @error ('tglahir_alm') is-invalid @enderror" id="tglahir_alm" placeholder="Tanggal Lahir" name="tglahir_alm" required>
                                    </div>
                              </div>
                              <!-- Agama -->
                              <div class="form-group row">
                                    <label for="nm_agama" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select name="nm_agama" required class="form-control @error('nm_agama') is-invalid @enderror" {{ count($nm_agama) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_agama) == 0)							
                                                      <option>Pilihan tidak ada</option>
                                                @else											
                                                      <option value="">--Silakan Pilih--</option>				
                                                      @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                      @endforeach
                                                @endif	
                                          </select>
                                          <span class="text-danger">@error('nm_agama') {{ $message }} @enderror</span>
                                    </div>
                              </div>
                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat_alm" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat_alm') }}" class="form-control @error ('alamat_alm') is-invalid @enderror" id="alamat_alm" placeholder="Alamat" name="alamat_alm" required>
                                          @error('alamat_alm')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt_alm" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_alm') }}" class="form-control @error ('rt_alm') is-invalid @enderror" id="rt_alm" placeholder="contoh: 012" name="rt_alm" required>
                                          @error('rt_alm')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw_alm" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_alm') }}" class="form-control @error ('rw_alm') is-invalid @enderror" id="rw_alm" placeholder="contoh: 005" name="rw_alm" required>
                                          @error('rw_alm')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <!-- Jenis Kelamin -->
                              <div class="form-group row">
                                    <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-3">
                                          <select class="form-control @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="">-- Silakan Pilih --</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    </div>
                              </div>
                              <hr>
                              <!-- Tempat mgl -->
                              <div class="form-group row">
                                    <label for="tpmgl_alm" class="col-sm-2 col-form-label">Tempat Meninggal</label>
                                    <div class="col-sm-6">
                                          <input type="text" class="form-control @error ('tpmgl_alm') is-invalid @enderror" id="tpmgl_alm" placeholder="Tempat Meninggal" name="tpmgl_alm">
                                    </div>
                              </div>
                              <!-- Tanggal mgl -->
                              <div class="form-group row">
                                    <label for="tgmgl_alm" class="col-sm-2 col-form-label">Tanggal Meninggal</label>
                                    <div class="col-sm-6">
                                          <input type="date" class="form-control @error ('tgmgl_alm') is-invalid @enderror" id="tgmgl_alm" placeholder="Tanggal Meninggal" name="tgmgl_alm">
                                    </div>
                              </div>
                              <!-- Sebab -->
                              <div class="form-group row">
                                    <label for="sebab_mgl" class="col-sm-2 col-form-label">Sebab Meninggal</label>
                                    <div class="col-sm-6">
                                          <input type="text" class="form-control @error ('sebab_mgl') is-invalid @enderror" id="sebab_mgl" placeholder="Sebab Meninggal" name="sebab_mgl">
                                    </div>
                              </div>
                              <hr>
                              <!-- SK. Kematian RS -->
                              <div class="form-group row">
                                    <label for="file_skmrs" class="col-sm-2 col-form-label">SK. Kematian RS</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_skmrs') is-invalid @enderror" id="file_skmrs" name="file_skmrs" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_skmrs')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                  </form>
            </div>

            <br>

            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              
                              <div class="card-header">
                              <h3 class="card-title"></h3>

                              <!-- SEARCH -->
                              <!-- <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div>
                              </div> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Hubungan</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th>Tanggal Meninggal</th>
                                                <th>Sebab Meninggal</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $kematian as $kmt )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $kmt->hubungan }}</td>
                                                <td>{{ $kmt->nm_alm }}</td>
                                                <td>{{ $kmt->nik_alm }}</td>
                                                <td>{{ $kmt->tgmgl_alm->format('d M Y') }}</td>
                                                <td>{{ $kmt->sebab_mgl }}</td>
                                                
                                                <td>
                                                      <a href="/kmt/{{$kmt->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/kmt/{{$kmt->id}}" method="POST" id="delete{{ $kmt->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $kmt->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="7" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Kematian
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection