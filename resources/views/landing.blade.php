<!DOCTYPE html>
<html lang='en'>

<head>
      <meta class="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <title>SPSO Desa Sasakpanjang</title>
      <link rel="shortcut icon" href="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png') }}">
      <!-- Don't forget to add your metadata here -->
      <link rel='stylesheet' href="{{ asset('/evie/css/style.min.css')}}" />

</head>

<body>
      <!-- Hero(extended) navbar -->
      <div class="navbar navbar--extended">
            <nav class="nav__mobile"></nav>
            <div class="container">
                  <div class="navbar__inner">
                        <a href="{{ url('landing') }}" class="navbar__logo">
                              <span style="display: inline-block; margin-top: 10px;"><img src="{{ asset('/evie/images/s.png')}}" alt="Kabupaten Bogor Logo" class="brand-image" style="height: 35px;"></span>
                              <!-- <span style="display: block;">Desa</span> -->
                        </a>
                        <nav class="navbar__menu">
                              <ul>
                              @guest
                                    <li><a onclick="return kontak()" href="#kontak">Kontak</a></li>
                                    <li><a href="{{ route('login') }}">Masuk</a></li>
                                    @if (Route::has('register'))
                                    <li class="nav-item">
                                          <a href="{{ route('register') }}">Registrasi</a>
                                    </li>
                                    @endif
                              @else
                                    <!-- <li><a href="{{ route('warga.show', Auth::user()->id) }}">{{ ucfirst(Auth::user()->name) }}</a></li> -->
                                    @if (Auth::user()->warga != NULL)
                                          <li><a href="{{ url('dashboardw') }}">{{ ucfirst(Auth::user()->name) }}</a></li>
                                    @elseif (Auth::user()->role->firstWhere('id', 1))
                                          <li><a href="{{ url('dashboard') }}">{{ ucfirst(Auth::user()->name) }}</a></li>
                                    @else 
                                          <li><a href="{{ url('dashboarda') }}">{{ ucfirst(Auth::user()->name) }}</a></li>
                                    @endif
                                    
                                    <li class="nav-item">
                                          <a class="dropdown-item" href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                          </a>
                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                          </form>
                                    </li>
                              @endguest
                              </ul>
                        </nav>
                        <div class="navbar__menu-mob"><a href="" id='toggle'><svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                          <path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z" class=""></path>
                                    </svg></a></div>
                  </div>
            </div>
      </div>
      <!-- Hero unit -->
      <div class="hero">
            <div class="hero__overlay hero__overlay--gradient"></div>
            <div class="hero__mask"></div>
            <div class="hero__inner">
                  <div class="container">
                        <div class="hero__content">
                              <div class="hero__content__inner" id='navConverter'>
                        @guest
                                    <h1 class="hero__title">Selamat Datang di Pembuatan Surat <i>Online</i> Desa Sasakpanjang</h1>
                        @else
                              @if (Auth::user()->warga != NULL)
                                    <h1 class="hero__title">Haloo {{ ucfirst(Auth::user()->name) }}, Selamat Datang di Pembuatan Surat <i>Online</i> Desa Sasakpanjang</h1>
                                    <a href="{{ url('choose')}}" class="button button__accent">Dapatkan Surat</a>
                              @else 
                                    <h1 class="hero__title">Haloo Admin, Selamat Datang di Pembuatan Surat <i>Online</i> Desa Sasakpanjang</h1>
                                    <a href="#" class="button button__accent">Haloo Admin..</a>      
                              @endif      
                        @endguest

                              
                                    
                              
                                    
                              </div>
                        </div>
                  </div>
            </div>
      </div>
      <div class="hero__sub">
            <span id="scrollToNext" class="scroll">
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" class='hero__sub__down' fill="currentColor" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <path d="M256,298.3L256,298.3L256,298.3l174.2-167.2c4.3-4.2,11.4-4.1,15.8,0.2l30.6,29.9c4.4,4.3,4.5,11.3,0.2,15.5L264.1,380.9c-2.2,2.2-5.2,3.2-8.1,3c-3,0.1-5.9-0.9-8.1-3L35.2,176.7c-4.3-4.2-4.2-11.2,0.2-15.5L66,131.3c4.4-4.3,11.5-4.4,15.8-0.2L256,298.3z" />
                  </svg>
            </span>
      </div>
      <!-- Steps -->
      <div class="steps landing__section">
            <div class="container">
                  <p class="text-center">Situs <i>web</i> khusus untuk warga Desa Sasakpanjang dalam mengajukan pembuatan surat secara <i>online</i>. </p>
                  <h2>Bagaimana prosedur mendapat surat melalui <i>website</i>?</h2>
                  <p>Berikut cara mendapatkan surat yang diinginkan.</p>
            </div>
            <div class="container">
                  <div class="steps__inner">
                        <div class="step">
                              <div class="step__media">
                                    <img src="{{ asset('/evie/images/undraw_designer.svg')}}" class="step__image">
                              </div>
                              <h4>Jenis Surat</h4>
                              <p class="step__text">Pemohon memilih jenis surat yang akan dibuat.</p>
                        </div>
                        <div class="step" style="background-color: #f7f7f7;">
                              <div class="step__media">
                                    <img src="{{ asset('/evie/images/undraw_responsive.svg')}}" class="step__image">
                              </div>
                              <h4>Data Diri</h4>
                              <p class="step__text">Isi data diri dengan benar sesuai jenis surat yang dipilih. </p>
                        </div>
                        <div class="step">
                              <div class="step__media">
                                    <img src="{{ asset('/evie/images/undraw_Mail_sent.svg')}}" class="step__image">
                              </div>
                              <h4> Pemberitahuan</h4>
                              <p class="step__text">Pemberitahuan surat yang diinginkan akan dikirim melalui akun pemohon. </p>
                        </div>
                        <div class="step" style="background-color: #f7f7f7;">
                              <div class="step__media">
                                    <img src="{{ asset('/evie/images/undraw_creation.svg')}}" class="step__image">
                              </div>
                              <h4>Dapat Surat</h4>
                              <p class="step__text">Setelah mendapat pemberitahuan, warga dipersilahkan datang ke kantor desa serta membawa KTP asli pemohon untuk validasi diri pemohon. </p>
                        </div>
                  </div>
            </div>
      </div>
      <!-- Expanded sections -->
      <div class="expanded landing__section">
            <div class="container">
                  <div class="expanded__inner">
                        <div class="expanded__media">
                              <img src="{{ asset('/evie/images/undraw_browser.svg')}}" class="expanded__image">
                        </div>
                        <div class="expanded__content">
                              <h2 class="expanded__title">Jenis pelayanan surat di Desa Sasakpanjang</h2>
                              <p class="expanded__text">Formulir Permohonan KTP, Surat Kematian, Surat Keterangan Belum Menikah, Surat Keterangan Bersih Diri, Surat Keterangan Domisili Majlis Taklim, Surat Keterangan Tidak Mampu, Surat Keterangan Domisili Usaha, Surat Keterangan Domisili, Surat Keterangan Kelahiran, Surat Keterangan Kematian Suami-Istri, Surat Keterangan Sudah Menikah, Surat Keterangan Usaha, Surat Pengantar Perkawinan.</p>
                        </div>
                  </div>
            </div>
      </div>
      <div class="expanded landing__section">
            <div class="container">
                  <div class="expanded__inner">
                        <div class="expanded__media">
                              <img src="{{ asset('/evie/images/undraw_online_resume.svg')}}" class="expanded__image">
                        </div>
                        <div class="expanded__content">
                              <h2 class="expanded__title">Mengapa memilih pelayanan pembuatan surat secara <i>online</i>? </h2>
                              <p class="expanded__text">1. Warga tidak perlu antri untuk mendapat surat yang diinginkan.</p>
                              <p class="expanded__text">2. Efisiensi pelayanan surat di Desa Sasakpanjang.</p>
                              <p class="expanded__text">3. Dapat mengurangi kerumunan di kantor desa untuk mencegah penyebaran COVID-19.</p>
                        </div>
                  </div>
            </div>
      </div>
      <div class="expanded landing__section">
            <div class="container">
                  <div class="expanded__inner">
                        <div class="expanded__media">
                              <img src="{{ asset('/evie/images/together.svg')}}" class="expanded__image">
                        </div>
                        <div class="expanded__content">
                              <h2 class="expanded__title">Sistem Usulan Berupa Prototype Untuk Tugas Akhir</h2>
                              <p class="expanded__text">Aplikasi berbasis <i>website</i> untuk keperluan sistem informasi administrasi kependudukan khusus untuk pembuatan surat warga Desa Sasakpanjang</p>
                        </div>
                  </div>
            </div>
      </div>

      <!-- Footer -->
      <div class="footer footer--dark">
            <div class="container">
                  <div class="footer__inner" id="kontak">
                        <p class="footer__textLogo">Kontak kami</p>
                        <div class="footer__data">
                              <div class="footer__data__item">
                                    <div class="footer__row">
                                          <b>Alamat Kantor Desa</b>
                                    </div>
                                    <div class="footer__row">
                                          Jl. AMD RT.01/08 Desa Sasakpanjang Desa Sasakpanjang Kecamatan Tajurhalang Kabupaten Bogor Provinsi Jawa Barat, 16320
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            <script src="{{ asset('/evie/js/app.min.js')}}"></script>

</body>

</html>