@extends('partials.master')

@section('content')
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Laporan Pembuatan Surat</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">      
                        <div class="card">
                              <div class="card-body">
                                    <div class="alert alert-light">Buat laporan pembuatan surat, semua data pembuatan akan direkap dan dibuat laporannya..</div>
                                    <a href="{{ url('laporan/create') }}" class="btn btn-sm btn-outline-primary btn-rounded">Buat Laporan</a>                              
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>
@endsection