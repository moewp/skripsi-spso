@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h6>Laporan Pembuatan Surat</h6>
                        <h4 class="text-center bg-gray-dark">Surat Keterangan Kematian Suami/Istri</h4>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-light">
                              
                              <div class="card-header">
                              <h3 class="card-title"></h3>

                              <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div>
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Tanggal</th>
                                                <th>Nomor Surat</th>
                                                <th>Nama</th>
                                                <th>Rt/Rw</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>28 Juni 2021</td>
                                                <td>402.1/2205/24Juni2021/003</td>
                                                <td>Jajang Nurjana</td>
                                                <td>005/005</td>
                                                <td>
                                                      <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-detail" data-route="#">
                                                                  <i class="fa fa-eye"></i> DETAIL
                                                      </button>
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection