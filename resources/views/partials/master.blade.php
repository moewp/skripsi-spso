<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>SPSO Desa Sasakpanjang</title>
      <link rel="shortcut icon" href="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png') }}">
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- overlayScrollbars -->
      <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css')}}">
      <!-- Google Font: Source Sans Pro -->
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <!-- Date Range Picker -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/daterangepicker/daterangepicker.css')}}">
      <!-- Tempus Dominus Bootstrap 4 -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
      <!-- BS Stepper -->
      <link rel="stylesheet" href="{{ asset('/adminlte/plugins/bs-stepper/css/bs-stepper.min.css')}}">
      <!-- Date Picker -->
      <link rel="stylesheet" href="{{ asset('/air-datepicker/dist/css/datepicker.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('/adminlter/dist/css/adminlte.min.css')}}">
      <!-- Sweet Alert -->
      <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
      @yield('styles')
</head>

<body class="hold-transition sidebar-mini layout-navbar-fixed">
      <!-- Site wrapper -->
      <div class="wrapper">

            
            <!-- Navbar -->
            @include('partials.navbar')

            <!-- Sidebar -->
            @include('partials.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                  @yield('content')
            </div>
            <!-- /.content-wrapper -->

            <!-- Footer -->
            <footer class="main-footer">
                  <div class="float-right d-none d-sm-block">
                        <b>Version</b> 3.0.5
                  </div>
                  <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
                  reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                  <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
      </div>
      <!-- ./wrapper -->

      <script>
		@yield('sweet')

		@yield('js')
	</script>
      <!-- jQuery -->
      <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap 4 -->
      <script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
      <!-- AdminLTE App -->
      <script src="{{ asset('/adminlte/dist/js/adminlte.min.js')}}"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="{{ asset('/adminlte/dist/js/demo.js')}}"></script>
      <!-- Date Picker NPM -->
      <script src="{{ asset('/air-datepicker/dist/js/datepicker.min.js')}}"></script>
      <script src="{{ asset('/air-datepicker/dist/js/i18n/datepicker.id.js')}}"></script>
      <script src="{{ asset('/air-datepicker/dist/js/i18n/datepicker.idn.js')}}"></script>
      <script src="{{ asset('/air-datepicker/dist/js/i18n/datepicker.idnt.js')}}"></script>
      <script src="{{ asset('/air-datepicker/dist/js/i18n/datepicker.idnd.js')}}"></script>
      <!-- bs-custom-file-input -->
      <script src="{{ asset('/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
      <script>
            $(function () {
            bsCustomFileInput.init();
            });
      </script>
</body>

</html>