<nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
            <li class="nav-item">
                  <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            @if (Auth::user()->warga != NULL)
            <li class="nav-item d-none d-sm-inline-block">
                  <a href="{{ url('choose')}}" class="nav-link">Pilih Surat</a>
            </li>
            @else
            <li class="nav-item d-none d-sm-inline-block">
                  <small class="nav-link"></small>
            </li>
            @endif
      </ul>

      <!-- SEARCH FORM -->
      <!-- <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                  <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                              <i class="fas fa-search"></i>
                        </button>
                  </div>
            </div>
      </form> -->

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown bg-secondary">
                  <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-th-large"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header"><h5>{{ ucfirst(Auth::user()->name) }}</h5></span>
                        <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                              </a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                              </form>
                        <div class="dropdown-divider"></div>
                        <p class="dropdown-item dropdown-header">...</p>
                  </div>
            </li>
      </ul>
</nav>
<!-- /.navbar -->