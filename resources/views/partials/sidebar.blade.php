<aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{ url('landing')}}" class="brand-link">
            <img src="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">DESA SASAKPANJANG</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                  @if (Auth::user()->warga != NULL)
                        <img src="/images/{{ Auth::user()->warga->image }}" alt="Profile Image">
                  @else
                        <img src="{{ asset('/adminlte/dist/img/avatar6.png')}}" class="img-circle elevation-2" alt="User Image">
                  @endif
                  </div>
                  <div class="info">
                  @if (Auth::user()->warga != NULL)
                        <a href="#" class="d-block"><b>{{ ucwords(Auth::user()->name) }}</b></a>
                  @elseif (Auth::user()->role != 'Warga')
                        <a href="#" class="d-block"><b>{{ ucwords(Auth::user()->name) }}</b></a>
                  @else
                        <a href="#" class="d-block"><b>Warga Baru</b></a>
                  @endif
                        <small class="d-block" style="color: #7e7e7e">{{ Auth::user()->role_user->role->name }}</small>
                  </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <!-- DASHBOARD -->
                  @if (Auth::user()->warga != NULL)
                  <!-- WARGA -->
                        <li class="nav-item has-treeview">
                              <a href="{{ url('dashboardw') }}" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                          Dashboard
                                    </p>
                              </a>
                        </li>
                  @elseif (Auth::user()->role->firstWhere('id', 1))
                  <!-- SUPER ADMIN -->
                        <li class="nav-item has-treeview">
                              <a href="{{ url('dashboard') }}" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                          Dashboard
                                    </p>
                              </a>
                        </li>
                  @else
                  <!-- ADMIN -->
                        <li class="nav-item has-treeview">
                              <a href="{{ url('dashboarda') }}" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                          Dashboard
                                    </p>
                              </a>
                        </li>
                  @endif

                  <!-- DATA - Super Admin -->
                  @if (Auth::user()->warga != NULL)
                        <li class="nav-header">.</li>
                  @elseif (Auth::user()->role->firstWhere('id', 1))
                        <li class="nav-header">Data</li>
                        <!-- MASTER -->
                        <li class="nav-item">
                              <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>
                                          Master
                                          <i class="fas fa-angle-left right"></i>
                                    </p>
                              </a>
                              <ul class="nav nav-treeview bg-indigo" style="display: none;">
                                    <li class="nav-item">
                                          <a href="{{ url('/agama') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Agama</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/pkj') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Pekerjaan</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/pdd') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Pendidikan</p>
                                          </a>
                                    </li>
                              </ul>
                        </li>
                        <!-- ADMIN -->                        
                        <li class="nav-item">
                              <a href="{{ url('/petugas') }}" class="nav-link">
                                    <i class="nav-icon far fa-user"></i>
                                    <p>
                                          Admin
                                    </p>
                              </a>
                        </li>
                        <!-- WARGA -->
                        <li class="nav-item">
                              <a href="{{ url('/indexwarga') }}" class="nav-link">
                                    <i class="nav-icon far fa-user"></i>
                                    <p>
                                          Warga
                                    </p>
                              </a>
                        </li>
                        <!-- SURAT -->
                        <li class="nav-item">
                              <a href="{{ url('/surat') }}" class="nav-link">
                                    <i class="nav-icon far fa-envelope"></i>
                                    <p>
                                          Surat
                                    </p>
                              </a>
                        </li>
                  @else
                        <li class="nav-header">.</li>
                  @endif

                  <!-- WARGA -->
                  @if (Auth::user()->warga != NULL)
                        <li class="nav-header">Profil</li>
                        <li class="nav-item">
                              <a href="{{ url('warga/'.Auth::user()->warga->id) }}" class="nav-link">
                                    <i class="nav-icon far fa-user"></i>
                                    <p>
                                          Profil
                                    </p>
                              </a>
                        </li>
                        <!-- <li class="nav-item has-treeview">
                              <a href="#" class="nav-link">
                                    <i class="nav-icon far fa-star"></i>
                                    <p>
                                          Dataku
                                          <i class="fas fa-angle-left right"></i>
                                    </p>
                              </a>
                              <ul class="nav nav-treeview bg-indigo">
                                    <li class="nav-item">
                                          <a href="#" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Ayah</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="#" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Ibu</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="#" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Anak</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="#" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Majlis Taklim</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="#" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Perusahaan</p>
                                          </a>
                                    </li>
                              </ul>
                        </li> -->
                  @elseif (Auth::user()->role != 'Warga')
                        <li class="nav-header">.</li>
                  @else
                        <li class="nav-header">Warga baru</li>
                  @endif

                  <!-- PEMBUATAN -->
                        <li class="nav-header">Proses</li>
                        <li class="nav-item has-treeview">
                              <a href="#" class="nav-link">
                                    <i class="nav-icon far fa-clock"></i>
                                    <p>
                                          Pembuatan
                                          <i class="fas fa-angle-left right"></i>
                                    </p>
                              </a>
                              <ul class="nav nav-treeview bg-indigo">
                                    <!-- Opsi Buat Admin -->
                              @if (Auth::user()->warga != NULL)
                                    <!-- Opsi Buat warga -->
                                    <li class="nav-item">
                                          <a href="{{ url('/daftarbuat') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Daftar Buat</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/riwayat') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Riwayat</p>
                                          </a>
                                    </li>
                              @else
                                    <li class="nav-item">
                                          <a href="{{ url('/pending') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Pending</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="#" id="complete" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>
                                                      Complete
                                                      <i class="fas fa-angle-left right"></i>
                                                </p>
                                          </a>
                                          <ul class="nav nav-treeview bg-black">
                                                <!-- skbd -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skbd') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>SK. Bersih Diri</p>
                                                      </a>
                                                </li>
                                                <!-- pktp -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/pktp') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Permohonan KTP</p>
                                                      </a>
                                                </li>
                                                <!-- skl -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skl') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Sk. Kelahiran</p>
                                                      </a>
                                                </li>
                                                <!-- sksm -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/sksm') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Sk. Sudah menikah</p>
                                                      </a>
                                                </li>
                                                <!-- skksi -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skksi') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>SK. Kematian 'Sutri'</p>
                                                      </a>
                                                </li>
                                                <!-- skk -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skk') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Sk. Kematian</p>
                                                      </a>
                                                </li>
                                                <!-- skbm -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skbm') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>SK. Belum Menikah</p>
                                                      </a>
                                                </li>
                                                <!-- skd -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skd') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Sk. Domisili</p>
                                                      </a>
                                                </li>
                                                <!-- spp -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/spp') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>SP. Pernikahan</p>
                                                      </a>
                                                </li>
                                                <!-- sku -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/sku') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Sk. Usaha</p>
                                                      </a>
                                                </li>
                                                <!-- skdu -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skdu') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>SK. Domisili Usaha</p>
                                                      </a>
                                                </li>
                                                <!-- skdmt -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/skdmt') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>SKD. Majlis Taklim</p>
                                                      </a>
                                                </li>
                                                <!-- sktm -->
                                                <li class="nav-item">
                                                      <a href="{{ url('/complete/sktm') }}" class="nav-link">
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p>Sk. Tidak Mampu</p>
                                                      </a>
                                                </li>
                                          </ul>
                                    </li>
                              @endif
                              </ul>
                        </li>

                  <!-- LAPORAN -->
                  @if (Auth::user()->warga == NULL)
                        <li class="nav-header">Laporan</li>
                        <li class="nav-item">
                              <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-book"></i>
                                    <p>
                                    Laporan
                                    <i class="fas fa-angle-left right"></i>
                                    </p>
                              </a>
                              <ul class="nav nav-treeview bg-indigo">
                                    <li class="nav-item">
                                          <a href="{{ url('cetak') }}" target="_blank" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Generalisasi Laporan</p>
                                          </a>
                                    </li>
                                    <!-- <li class="nav-item">
                                          <a href="{{ url('/lap-fpktp') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Pembuatan KTP</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skk') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Surat Kematian</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skksi') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Kematian Suami/Istri</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skbm') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Belum Menikah</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-sksm') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Sudah Menikah</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skd') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Domisili</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skdmt') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Domisili Majlis Taklim</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skdu') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Domisili Usaha</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-sku') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Usaha</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-sktm') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Tidak Mampu</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skbd') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Bersih Diri</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-skl') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SK. Kelahiran</p>
                                          </a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="{{ url('/lap-spp') }}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>SP. Perkawinan</p>
                                          </a>
                                    </li> -->
                              </ul>
                        </li>
                        <li class="nav-header"><small>SK: Surat Keterangan<br>SP: Surat Pengantar</small></li>
                  @else
                        <li class="nav-header">.</li>
                  @endif
                        <li class="nav-header">Sistem Informasi Administrasi <br>Kependudukan<br>Pembuatan Surat Berbasis <i>Website</i></li>
                  </ul>
            </nav>
            <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
</aside>