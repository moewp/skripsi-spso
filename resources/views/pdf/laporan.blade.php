<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<title>Laporan</title>

</head>
<body>

<style>
      .page-break{
      page-break-after:always;
      }
      .text-center{
      text-align:center;
      }
      .text-header {
      font-size:1.1rem;
      }
      .size2 {
      font-size:1.4rem;
      }
      .border-bottom {
      border-bottom:1px black solid;
      }
      .border {
      border: 2px block solid;
      }
      .border-top {
      border-top:1px black solid;
      }
      .float-right {
      float:right;
      }
      .mt-4 {
      margin-top:4px;
      }
      .mx-1 {
      margin:1rem 0 1rem 0;
      }
      .mr-1 {
      margin-right:1rem;
      }
      .mt-1 {
      margin-top:1rem;
      }
      ml-2 {
      margin-left:2rem;
      }
      .ml-min-5 {
      margin-left:-5px;
      }
      .text-uppercase {
      font:uppercase;
      }
      .d1 {
      font-size:2rem;
      }
      .img {
      
      }
      .link {
      font-style:underline;
      }
      .text-desc {
      font-size:14px;
      }
      .text-bold {
      font-style:bold;
      }
      .underline {
      text-decoration:underline;
      }
      
      table {
      font-family: Arial, Helvetica, sans-serif;
      color: #666;
      text-shadow: 1px 1px 0px #fff;
      background: #eaebec;
      border: #ccc 1px solid;
      }
      table th {
      padding: 10px 15px;
      border-left:1px solid #e0e0e0;
      border-bottom: 1px solid #e0e0e0;
      background: #ededed;
      }  
      table tr {
      text-align: center;
            padding-left: 20px;
      }
      table td {
            padding: 10px 15px;
            border-top: 1px solid #ffffff;
            border-bottom: 1px solid #e0e0e0;
            border-left: 1px solid #e0e0e0;
            background: #fafafa;
            background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
            background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
      }
      .table-center {
      margin-left:auto;
      margin-right:auto;
      }
      .mb-1 {
      margin-bottom:1rem;
      }
</style>

<!-- header -->
<div class="text-center">
      <img src="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png') }}" class="img" alt="logo.png" width="90">
      <div>
            <span class="text-header text-bold text-danger">
                  PEMERINTAH KABUPATEN BOGOR KECAMATAN TAJURHALANG <br>
                  <span class="size2">DESA SASAKPANJANG</span> <br> 
            </span>
            <span class="text-desc">Alamat: Jl. AMD RT. 01/08 Desa Sasakpanjang Kec. Tajurhalang Kab. Bogor Telp. (0251) 8584850</span>
      </div>
<div>
<!-- /header -->

<hr class="border">

<!-- content -->

<div class="size2 text-center mb-1" style="margin-top:30px;">-Laporan Pembuatan Surat-</div>

<table class="table-center mb-1">
      <thead>
      <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>RT/RW</th>
            <th>NIK</th>
            <th>Nama Surat</th>
            <th>Nomor Surat</th>
            <th>Tanggal Buat</th>
      </tr>
      </thead>
      <tbody>
            @php $i=1 @endphp
            @foreach($cetak as $val)
            <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ $val->user->warga->nm_warga }}</td>
                  <td>{{ $val->user->warga->rt }}/{{ $val->user->warga->rw }}</td>
                  <td>{{ $val->user->warga->nik }}</td>
                  <td>{{ $val->surat->nm_surat }}</td>
                  <td>{{ $val->surat->kd_surat }}/0{{ $val->id }}/{{ $val->created_at->format('m') }}/{{ $val->created_at->format('Y') }}</td>
                  <td>{{ $val->created_at->format('d M, Y') }}</td>
            </tr>
            @endforeach
      </tbody>


</table>
<!-- /content -->
<script type="text/javascript">
      window.print();
</script>

</body>
</html>