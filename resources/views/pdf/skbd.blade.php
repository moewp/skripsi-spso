<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Surat Keterangan Bersih Diri</title>
      <link rel="shortcut icon" href="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png') }}">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 10; padding: 0;">
@foreach($cetak as $val)
      <table align="center" cellpadding="0" cellspacing="0">
            <tr>
                  <td><img src="{{ asset('/adminlte/dist/img/Kabupaten Bogor 160x160.png') }}" width="70" height="70" alt="img"></td>
                  <td align="center" width="500px">
                        <font size="3">PEMERINTAH KABUPATEN BOGOR</font><br>
                        <font size="3">KECAMATAN TAJURHALANG</font><br>
                        <font size="5">DESA SASAKPANJANG</font><br>
                        <font size="1">Alamat: Jl. AMD RT. 01/08 Desa Sasakpanjang Kec. Tajurhalang Kab. Bogor Telp. (0251) 8584850</font>
                  </td>
            </tr>
            <tr>
                  <td colspan="2"><hr style="height: 2px; background: black;"></td>
            </tr>
            <tr>
                  <td align="center" colspan="2" style="padding: 10px 0px 20px 0px;">
                        <u>SURAT KETERANGAN BERSIH DIRI</u><br>
                        <font size="2">Nomor: {{ $val->surat->kd_surat }}/0{{ $val->id }}/{{ $val->created_at->format('m') }}/{{ $val->created_at->format('Y') }} </font>
                  </td>
            </tr>
            <tr>
                  <td align="left" colspan="2" style="padding: 0px 30px 0px 30px;">
                        &emsp;&emsp;&emsp;Yang bertanda tangan dibawah ini Kepala Desa Sasakpanjang Kecamatan<br> 
                        Tajurhalang Kabupaten Bogor, menerangkan dengan sebenarnya bahwa:
                  </td>
            </tr>
            <tr>
                  <td  colspan="2" style="padding: 40px 30px 40px 30px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                    <td>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      Nama
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->nm_warga }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      Jenis Kelamin
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->jk }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      Tempat, Tanggal Lahir
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->tmp_lahir }}, {{ $val->user->warga->tgl_lahir->format('d M Y') }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      Kewarganegaraan
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->kwn }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      No. KTP
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->nik }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      Agama
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->agama->nm_agama }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 5px 20px;">
                                                      Alamat
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      :
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      {{ $val->user->warga->alamat }}
                                                </td>
                                          </tr>
                                          <tr>
                                                <td width="120" valign="top" style="padding: 0px 0px 0px 20px;">
                                                      
                                                </td>
                                                <td style="font-size: 1; line-height: 0;" width="1">
                                                      
                                                </td>
                                                <td width="260" valign="top" style="padding: 0px 10px 0px 5px;">
                                                      RT{{ $val->user->warga->rt }} RW{{ $val->user->warga->rw }}
                                                </td>
                                          </tr>
                                    </td>
                              </tr>
                        </table>
                  </td>
            </tr>
            <tr width="100%">
                  <td align="left" colspan="2" style="padding: 0px 30px 0px 30px;">
                        &emsp;&emsp;&emsp;Nama tersebut di atas adalah benar penduduk Desa Sasakpanjang Kec.<br> Tajurhalang Kab. Bogor berdasarkan pengantar RT/RW serta menurut pengakuan<br> yang bersangkutan, hingga saat dikeluarkan surat keterangan ini belum pernah<br> terlibat dalam perkara Pidana/Justisi/Kepolisian dan tidak pernah memasuki partai<br> atau organisasi yang dilarang oleh Pemerintah/Negara. Demikian surat keterangan <br>ini dibuat untuk dapat digunakan sebagaimana mestinya.
                  </td>
            </tr>
            <tr>
                  <td align="right" colspan="2" style="padding: 40px 30px 0px 30px;">
                        Sasakpanjang, {{$date->format('d M Y') }}<br>
                  </td>
            </tr>
            <tr>
                  <td colspan="2" style="padding: 0px 0px 0px 0px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                    <td width="260" valign="top"><center>
                                          <p style="font-size: 12pt; padding: 0px 0px 40px 0px;">Camat Tajurhalang</p>
                                          <p style="font-size: 12pt; padding: 0px 0px 0px 0px;">(Fikri Ikhsani, S.STP., SH., M.Si.)</p>
                                    </td>
                                    <td style="font-size: 0; line-height: 0;" width="20">
                                          &nbsp;
                                    </td>
                                    <td width="260" valign="top"><center>
                                          <p style="font-size: 12pt; padding: 0px 0px 40px 0px;">Kepala Desa Sasakpanjang</p>
                                          <p style="font-size: 12pt; padding: 0px 0px 0px 0px;">(Andi Umi Yulaikah, S.Pd.)</p>
                                    </td>
                              </tr>
                        </table>
                  </td>
            </tr>
            <tr>
                  <td colspan="2" style="padding: 0px 0px 0px 0px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                    <td width="260" valign="top"><center>
                                          <p style="font-size: 12pt; padding: 10px 0px 40px 0px;">(Mengetahui)<br>Kapolsek Tajurhalang</p>
                                          <p style="font-size: 12pt; padding: 0px 0px 0px 0px;">(IPTU Dwi Yulianto, S.H.)</p>
                                    </td>
                              </tr>
                        </table>
                  </td>
            </tr>
      </table>
@endforeach
</body>
<script type="text/javascript">
      window.print();
</script>
</html>