@extends('partials.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  @if (session('mohon'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('mohon') }}
                        </div>
                  @endif
                  <!-- Keterangan Status -->
                  <div class="card-header">
                        <label class="card-title">Keterangan Status</label>
                  </div>
                  <div class="card-body">
                        <div class="form-group">
                              <ul>
                                    <li>
                                          <label class="badge badge-warning">Menunggu</label> 
                                          <p> Pesanan sedang kami cek, apakah bisa diterima atau tidak.</p>
                                    </li>
                                    <li>
                                          <label class="badge badge-info">Diterima</label>  
                                          <p>Pesanan sudah kami terima, dan kamu bisa langsung mendapatkannya di kantor desa.</p>
                                    </li>
                                    <li>
                                          <label class="badge badge-danger">Ditolak</label>  
                                          <p>Pesanan kamu ditolak karena tidak sesuai dengan format pembuatan surat. Silakan isi dengan benar data yang kamu masukan dan ulangi kembali.</p>
                                    </li>
                              </ul>
                        </div>
                  </div>

                  <div class="card-footer">
                        
                  </div>
            </div>

            <br>

            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-secondary">
                              <div class="card-header">
                              <h3 class="card-title">Daftar Buat</h3>
                              <div class="card-tools">
                                    <!-- SEARCH -->
                                    <!-- <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div> -->
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Tanggal</th>
                                                <th>Surat</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $buats as $buat )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $buat->created_at->format('d M, Y') }}</td>
                                                <td>{{ $buat->surat->nm_surat }}</td>
                                                <td>
                                                      @if ($buat->status == 0)
                                                      <label class="badge badge-warning">Menunggu</label>
                                                      @elseif ($buat->status == 1)
                                                            <label class="badge badge-info">Diterima</label>
                                                      @else
                                                            <label class="badge badge-danger">Ditolak</label>
                                                      @endif
                                                </td>
                                                <td>
                                                      @if ($buat->status == 0)
                                                            <button class="btn btn-primary btn-xs btn-detail" data-toggle="modal" data-target="#modal-detail" data-route="{{ route('detailBuat', $buat->id) }}">
                                                            <i class="fa fa-eye"></i> DETAIL
                                                            </button>
                                                            <button class="btn btn-danger btn-xs" onclick="document.getElementById('cancelBuat-{{ $buat->id }}').submit();"><i class="fa fa-times"></i> BATAL</button>
                                                            <form id="cancelBuat-{{ $buat->id }}" method="POST" action="{{ route('cancelBuat', $buat->id) }}">
                                                            {{ csrf_field() }}
                                                            </form>
                                                      @else
                                                            <button class="btn btn-primary btn-xs btn-detail" data-toggle="modal" data-target="#modal-detail" data-route="{{ route('detailBuat', $buat->id) }}">
                                                            <i class="fa fa-eye"></i> DETAIL
                                                            </button>
                                                            <button class="btn btn-danger btn-xs" disabled="disabled"><i class="fa fa-times"></i> BATAL</button>
                                                      @endif
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="5" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data 
                                                </td>
                                          </tr>
                                          @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

            <div class="row">
                  <!-- Modal -->
                  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                    <div class="modal-header bg-warning">
                                          <h5 class="modal-title text-bold">Detail</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                          </button>
                                    </div>
                                    <div class="modal-body">
                                    {{-- Content --}}
                                    </div>
                                    <div class="modal-footer">
                                          <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>

      </div>
</div>

<script src="{{ asset('/js/app.js')}}"></script>
<script type="text/javascript">
      $('.btn-detail').click(function(event) {
            $.get($(this).data('route'), function(data) {
                  $('#modal-detail .modal-body').html(data)
            });
      });
</script>
@endsection