<div class="container-fluid">
      <div class="row">
            <div class="col-md-12"><h6>No. Surat: {{ $buat->surat->kd_surat }}/0{{ $buat->id }}/{{ $buat->created_at->format('m') }}/{{ $buat->created_at->format('Y') }} </h6></div>
      </div>
      <hr>
      <div class="row">
            <div class="col-md-12"><h4><strong>{{ $buat->user->warga->nm_warga }}</strong></h4></div>
      </div>
      <div class="row">
            <div class="col-md-5">NIK: {{ $buat->user->warga->nik }}</div>
            <div class="col-md-5 ml-auto">No. KK: {{ $buat->user->warga->nkk }}</div>
      </div>
      <div class="row">
            <div class="col-md-12">Alamat: {{ $buat->user->warga->alamat }}</div>
      </div>
      <div class="row">
            <div class="col-md-2">RT: {{ $buat->user->warga->rt }}</div>
            <div class="col-md-2">RW: {{ $buat->user->warga->rw }}</div>
      </div>
      <hr>
      <div class="row">
            <div class="col-md"><b>File KK</b>:</div>
            <div class="col-md-12">
                  <img src="/images/kk/{{ $buat->user->warga->file_kk }}" width="100%px">
            </div>
      </div>
      <hr>
      <div class="row">
            <div class="col-md"><b>File Pengantar dari RT/RW</b>:</div>
            <div class="col-md-12">
                  <img src="/images/rtrw/{{ $buat->file_rtrw }}" width="100%px">
            </div>
      </div>
      <hr>
      @if (($buat->surat->id) == 1)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      @endif
      
      @if (($buat->surat->id) == 3)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>Buku Nikah</b>:</div>
            <div class="col-md-12">
                  <img src="/images/bn/{{ $buat->user->sutri->file_bn }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>File SK. Kelahiran RS.</b>:</div>
            <div class="col-md-12">
                  <img src="/images/sklrs/{{ $buat->user->anak->file_sklrs }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 4)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>Buku Nikah</b>:</div>
            <div class="col-md-12">
                  <img src="/images/bn/{{ $buat->user->sutri->file_bn }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 5)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>File SK. Kelahiran RS.</b>:</div>
            <div class="col-md-12">
                  <img src="/images/skmt/{{ $buat->user->sutri->file_skmt }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 6)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>File SK. Kematian RS.</b>:</div>
            <div class="col-md-12">
                  <img src="/images/skmrs/{{ $buat->user->kematian->file_skmrs }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 7)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 8)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 9)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>KTP Ayah</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktpay/{{ $buat->user->ayah->file_ktpay }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>KTP Ibu</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktpib/{{ $buat->user->ibu->file_ktpib }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 10)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 11)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      <div class="row">
            <div class="col-md"><b>Profil Perusahaan</b>:</div>
            <div class="col-md-12">
                  <img src="/images/pp/{{ $buat->user->perusahaan->file_pp }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 12)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      @endif

      @if (($buat->surat->id) == 13)
      <div class="row">
            <div class="col-md"><b>KTP</b>:</div>
            <div class="col-md-12">
                  <img src="/images/ktp/{{ $buat->user->warga->file_ktp }}" width="100%px">
            </div>
      </div>
      @endif
</div>
