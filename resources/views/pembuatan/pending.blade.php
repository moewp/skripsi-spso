@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Permohonan Surat Pending</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            
            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        @if (session('del'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-gray">
                              <div class="card-header">
                              <h3 class="card-title"></h3>
                              <div class="card-tools">
                                    <!-- SEARCH FORM -->
                                    <!-- <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div> -->
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Tanggal Masuk</th>
                                                <th>Nama Warga</th>
                                                <th>NIK</th>
                                                <th>Nama Surat</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $buats as $buat )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $buat->created_at->format('d M, Y')}}</td>
                                                <td>{{ $buat->user->warga->nm_warga }}</td>
                                                <td>{{ $buat->user->warga->nik }}</td>
                                                <td>{{ $buat->surat->nm_surat }}</td>
                                                <td>
                                                      @if ($buat->status == 0)
                                                      <label class="badge badge-warning">Menunggu</label>
                                                      @elseif ($buat->status == 1)
                                                            <label class="badge badge-primary">Diterima</label>
                                                      @else
                                                            <label class="badge badge-danger">Ditolak</label>
                                                      @endif
                                                <td>
                                                      <button class="btn btn-default btn-xs btn-detail" data-toggle="modal" data-target="#modal-detail" data-route="{{ route('detailBuat', $buat->id) }}">
                                                            <i class="fa fa-eye"></i> <strong>DETAIL</strong>
                                                      </button>
                                                      @if ($buat->status == 0)
                                                            <button class="btn btn-primary btn-xs btn-verify" data-form="acc-order-{{ $buat->id }}">
                                                                  <i class="fa fa-check"></i> <strong>TERIMA</strong>
                                                            </button>
                                                            <button class="btn btn-danger btn-xs btn-verify" data-form="cancel-order-{{ $buat->id }}">
                                                                  <i class="fa fa-times"></i> <strong>BATAL</strong>
                                                            </button>
                                                      @elseif ($buat->status == 1)
                                                            <button class="btn btn-success btn-xs btn-verify" data-form="complete-order-{{ $buat->id }}">
                                                                  <strong>TANDAI SELESAI</strong>
                                                            </button>
                                                      @else
                                                            
                                                      @endif
                                                      <form id="complete-order-{{ $buat->id }}" method="POST" action="{{ route('verifyBuat', $buat->id) }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="method" value="complete">
                                                      </form>
                                                      <form id="acc-order-{{ $buat->id }}" method="POST" action="{{ route('verifyBuat', $buat->id) }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="method" value="accept">
                                                      </form>
                                                      <form id="cancel-order-{{ $buat->id }}" method="POST" action="{{ route('verifyBuat', $buat->id) }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="method" value="cancel">
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="7" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data 
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

            <div class="row">
                  <!-- Modal -->
                  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                    <div class="modal-header">
                                          <small>Detail Surat</small>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                          </button>
                                    </div>
                                    <div class="modal-body">
                                          <div class="container-fluid">
                                                <div class="row">
                                                      <div class="col-md-5">NIK: {{ $buat->user->warga->nik }}</div>
                                                      <div class="col-md-5 ml-auto">No. KK: {{ $buat->user->warga->nkk }}</div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                      <div class="col-md-12">Alamat: {{ $buat->user->warga->alamat }}</div>
                                                </div>
                                                <div class="row">
                                                      <div class="col-md-2">RT: {{ $buat->user->warga->rt }}</div>
                                                      <div class="col-md-2">RW: {{ $buat->user->warga->rw }}</div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                      <div class="col-md"><b>File KK</b>:</div>
                                                      <div class="col-md-12">
                                                            <img src="/images/kk/{{ $buat->file_kk }}" width="100%px">
                                                      </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                      <div class="col-md"><b>File RT/RW</b>:</div>
                                                      <div class="col-md-12">
                                                            <img src="/images/rtrw/{{ $buat->file_rtrw }}" width="100%px">
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="modal-footer">
                                          <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>

<script src="{{ asset ('/js/app.js')}}"></script>
<script>      
      $('.btn-detail').click(function(event) {
            $.get($(this).data('route'), function(data) {
                  $('#modal-detail .modal-body').html(data);
            });
      });

      $('.btn-verify').click(function(event) {
            document.getElementById($(this).data('form')).submit();
      });
</script>

@endsection