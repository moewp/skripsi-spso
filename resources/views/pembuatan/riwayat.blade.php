@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h5>Riwayat Pembuatan</h5>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-info">
                              
                              <div class="card-header">
                              <h3 class="card-title">Riwayat</h3>

                              <div class="card-tools">
                                    <!-- SEARCH -->
                                    <!-- <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div> -->
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Tanggal</th>
                                                <th>Surat</th>
                                                <th>Nomor Surat</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @foreach ($buats as $buat)
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $buat->created_at->format('d M, Y') }}</td>
                                                <td>{{ $buat->surat->nm_surat }}</td>
                                                <td>{{ $buat->surat->kd_surat }}/0{{ $buat->id }}/{{ $buat->created_at->format('m') }}/{{ $buat->created_at->format('Y') }}</td>
                                                <td><label class="badge badge-success">Selesai</label></td>
                                                <td>
                                                      <button class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target="#modal-detail" data-route="{{ route('detailBuat', $buat->id) }}">
                                                                  <i class="fa fa-eye"></i> DETAIL
                                                      </button>
                                                </td>
                                          </tr>
                                    @endforeach
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

            <div class="row">
                  <!-- Modal -->
                  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                    <div class="modal-header bg-warning">
                                          <h5 class="modal-title text-bold">Detail</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                          </button>
                                    </div>
                                    <div class="modal-body">
                                    {{-- Content --}}
                                    </div>
                                    <div class="modal-footer">
                                          <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>

      </div>
</div>

<script src="{{ asset('/js/app.js')}}"></script>
<script type="text/javascript">
      $('.btn-detail').click(function(event) {
            $.get($(this).data('route'), function(data) {
                  $('#modal-detail .modal-body').html(data)
            });
      });
</script>

@endsection