@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Perusahaan</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Warga -->
                  <div class="card-header">
                        <h3 class="card-title">Isi Data Perusahaanmu dengan Benar!</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/psh') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::id()}}">
                        <div class="card-body">
                              <!-- Nama Perusahaan -->
                              <div class="form-group row">
                                    <label for="nm_ush" class="col-sm-2 col-form-label">Nama Perusahaan</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('nm_ush') }}" class="form-control @error ('nm_ush') is-invalid @enderror" id="nm_ush" placeholder="Nama Perusahaan" name="nm_ush" required>
                                          @error('nm_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat_ush" class="col-sm-2 col-form-label">Alamat Perusahaan</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat_ush') }}" class="form-control @error ('alamat_ush') is-invalid @enderror" id="alamat_ush" placeholder="Alamat" name="alamat_ush" required>
                                          @error('alamat_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <label for="rt_ush" class="col-sm-2 col-form-label">RT</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_ush') }}" class="form-control @error ('rt_ush') is-invalid @enderror" id="rt_ush" name="rt_ush" placeholder="RT" required>
                                          @error('rt_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- RW -->
                                    <label for="rw_ush" class="col-sm-2 col-form-label">RW</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_ush') }}" class="form-control @error ('rw_ush') is-invalid @enderror" id="rw_ush" name="rw_ush" placeholder="RW" required>
                                          @error('rw_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              
                              <!-- JU PJ -->
                              <div class="form-group row">
                                    <!-- Jenis Usaha -->
                                    <label for="jns_ush" class="col-sm-2 col-form-label">Jenis Usaha</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('jns_ush') }}" class="form-control @error ('jns_ush') is-invalid @enderror" id="jns_ush" name="jns_ush" placeholder="Jenis Usaha" required>
                                          @error('jns_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Penanggung Jawab -->
                                    <label for="pj_ush" class="col-sm-2 col-form-label">Penanggung Jawab</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('pj_ush') }}" class="form-control @error ('pj_ush') is-invalid @enderror" id="pj_ush" name="pj_ush" placeholder="Penanggung Jawab" required>
                                          @error('pj_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Akta SB -->
                              <div class="form-group row">
                                    <!-- Akta Usaha -->
                                    <label for="akta_ush" class="col-sm-2 col-form-label">Akta Usaha</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('akta_ush') }}" class="form-control @error ('akta_ush') is-invalid @enderror" id="akta_ush" name="akta_ush" placeholder="Akta Usaha" required>
                                          @error('akta_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Status Bangunan -->
                                    <label for="sb_ush" class="col-sm-2 col-form-label">Status Bangunan</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('sb_ush') }}" class="form-control @error ('sb_ush') is-invalid @enderror" id="sb_ush" name="sb_ush" placeholder="Status Bangunan" required>
                                          @error('sb_ush')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <!-- Jumlah Karyawan -->
                              <div class="form-group row">
                                    <label for="jml_kry" class="col-sm-2 col-form-label">Jumlah Karyawan</label>
                                    <div class="col-sm-9">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('jml_kry') }}" class="form-control @error ('jml_kry') is-invalid @enderror" id="jml_kry" placeholder="Jumlah Karyawan" name="jml_kry" required>
                                          @error('jml_kry')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <hr>
                              <!-- Profil Perusahaan -->
                              <div class="form-group row">
                                    <label for="file_pp" class="col-sm-2 col-form-label">Profil Perusahaan</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_pp') is-invalid @enderror" id="file_pp" name="file_pp" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_pp')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                  </form>
            </div>
      </div>
</div>

@endsection