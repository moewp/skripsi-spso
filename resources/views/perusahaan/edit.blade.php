@extends('partials.master')

@section('content')

<div class="row mb-4"></div>
<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Profile -->
                  <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Data Perusahaan</h3>
                  </div>
                  <form method="POST" action="/psh/{{ $perusahaan->id }}" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                        <div class="card-body">
                              <!-- Nama Perusahaan -->
                              <div class="form-group">
                                    <label for="nm_ush">Nama Perusahaan</label>
                                    <input type="text" value="{{ old('nm_ush', $perusahaan->nm_ush) }}" class="form-control @error('nm_ush') is-invalid @enderror" name="nm_ush" id="nm_ush" required>
                                    @error('nm_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat Perusahaan -->
                              <div class="form-group">
                                    <label for="alamat_ush">Alamat Perusahaan</label>
                                    <input type="text" value="{{ old('alamat_ush', $perusahaan->alamat_ush) }}" class="form-control @error('alamat_ush') is-invalid @enderror" name="alamat_ush" id="alamat_ush" required>
                                    @error('alamat_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT Perusahaan -->
                              <div class="form-group">
                                    <label for="rt_ush">RT Perusahaan</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_ush', $perusahaan->rt_ush) }}" class="form-control @error('rt_ush') is-invalid @enderror" name="rt_ush" id="rt_ush" required>
                                    @error('rt_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW Perusahaan -->
                              <div class="form-group">
                                    <label for="rw_ush">RW Perusahaan</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_ush', $perusahaan->rw_ush) }}" class="form-control @error('rw_ush') is-invalid @enderror" name="rw_ush" id="rw_ush" required>
                                    @error('rw_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jumlah Karyawan -->
                              <div class="form-group">
                                    <label for="jml_kry">Jumlah Karyawan</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('jml_kry', $perusahaan->jml_kry) }}" class="form-control @error('jml_kry') is-invalid @enderror" name="jml_kry" id="jml_kry" required>
                                    @error('jml_kry')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jenis Usaha -->
                              <div class="form-group">
                                    <label for="jns_ush">Jenis Usaha</label>
                                    <input type="text" value="{{ old('jns_ush', $perusahaan->jns_ush) }}" class="form-control @error('jns_ush') is-invalid @enderror" name="jns_ush" id="jns_ush" required>
                                    @error('jns_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Penanggung Jawab -->
                              <div class="form-group">
                                    <label for="pj_ush">Penanggung Jawab</label>
                                    <input type="text" value="{{ old('pj_ush', $perusahaan->pj_ush) }}" class="form-control @error('pj_ush') is-invalid @enderror" name="pj_ush" id="pj_ush" required>
                                    @error('pj_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Akta Usaha -->
                              <div class="form-group">
                                    <label for="akta_ush">Akta Usaha</label>
                                    <input type="text" value="{{ old('akta_ush', $perusahaan->akta_ush) }}" class="form-control @error('akta_ush') is-invalid @enderror" name="akta_ush" id="akta_ush" required>
                                    @error('akta_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Status Bangunan -->
                              <div class="form-group">
                                    <label for="sb_ush">Status Bangunan</label>
                                    <input type="text" value="{{ old('sb_ush', $perusahaan->sb_ush) }}" class="form-control @error('sb_ush') is-invalid @enderror" name="sb_ush" id="sb_ush" required>
                                    @error('sb_ush')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <hr>
                              <!-- Profil Perusahaan -->
                              <div class="form-group row mt-5">
                                    <label for="file_pp" class="col-md-3 col-form-label text-md-right">Profil Perusahaan</label>
                                    <div class="col-md-9">
                                          <img src="/images/pp/{{ $perusahaan->file_pp }}" width="70px" alt="no file||">
                                          File Name : {{ $perusahaan->file_pp }}
                                          <input id="file_pp" type="file" class="form-control" name="file_pp" autofocus>
                                          @error('file_pp')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/psh/{{$perusahaan->id}}">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection