@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-10">
                        <h3>Data Admin</h3>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Surat -->
                  <div class="card-header">
                        <h3 class="card-title">Tambah Admin</h3>
                  </div>
                  
                  
                  <form method="POST" action="{{ url('/petugas') }}" enctype="multipart/form-data">
                  @csrf
                        <div class="card-body">
                              <!-- LEVEL -->
                              <!-- <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                          <label class="input-group-text">		 	
                                                Level	
                                          </label>
                                    </div>
                                    <select name="level" class="form-control">
                                          @if(count($name) == 0)							
                                                <option>Pilihan tidak ada</option>
                                          @else											
                                                <option value="">--Silakan Pilih--</option>				
                                                @foreach($name as $value) 			
                                                      <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                @endforeach
                                          @endif
                                    </select>
                              </div> -->
                              <!-- Nama Petugas -->
                              <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Masukan nama admin">
                                    @error('name')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Email -->
                              <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Masukan email">
                                    @error('email')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Password -->
                              <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password minimal 8 karakter">
                                    @error('password')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Confirm Password -->
                              <div class="form-group">
                                    <label for="password-confirm">Konfirmasi Password</label>
                                    <input type="password" class="form-control @error('password-confirm') is-invalid @enderror" name="password-confirm" id="password-confirm" required autocomplete="new-password" placeholder="Masukan konfirmasi password">
                                    @error('password-confirm')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                        </div>

                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">submit</button>
                        </div>
                  </form>
            </div>
      </div>
</div>


@endsection