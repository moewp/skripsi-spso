@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-10">
                        <h3>Data Admin</h3>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Surat -->
                  <div class="card-header">
                        <h3 class="card-title">Edit Admin</h3>
                        <a href="{{ url('/petugas') }}" class="btn btn-dark btn-rounded float-right">
                              <i class="mdi mdi-chevron-left"></i> Kembali
                        </a>
                  </div>
                  
                  
                  <form method="POST" action="{{ url('petugas', $edit->id) }}" enctype="multipart/form-data">
                  @csrf
                  @method('put')
                        <div class="card-body">
                              <!-- Nama Petugas -->
                              <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" value="{{ $edit->name}}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Masukan nama admin">
                                    @error('name')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Email -->
                              <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" value="{{ $edit->email }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Masukan email">
                                    @error('email')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Password -->
                              <div class="form-group">
                                    <label for="password">Password Baru (Opsional)</label>
                                    <input type="password" value="" class="form-control @error('password_baru') is-invalid @enderror" name="password" id="new_pass">
                                    @error('password_baru')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Confirm Password -->
                              <div class="form-group">
                                    <label>Konfirmasi Password</label>
                                    <input type="password" class="form-control @error('password-confirm') is-invalid @enderror" id="confirm_new_pass">
                                    @error('password_baru')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>

                              <input type="hidden" id="old_pass" name="old_pass" value="">
                        </div>

                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">submit</button>
                        </div>
                  </form>
            </div>
      </div>
</div>


@endsection