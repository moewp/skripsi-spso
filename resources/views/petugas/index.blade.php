@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row">
                  <div class="col-sm-12">
                        <h3>Data Admin</h3>
                        <a href="{{ url('petugas/create') }}" class="btn btn-outline-primary btn-rounded float-right mb-1">
					<i class="mdi mdi-plus-circle"></i> {{ __('Tambah Admin') }}
                        </a>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              <div class="card-header">
                              <h3 class="card-title"></h3>
                              <div class="card-tools">
                                    <!-- SEARCH -->
                                    <!-- <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div> -->
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Admin</th>
                                                <th>Email</th>
                                                <th>Dibuat</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $admin as $admin )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ optional($admin->user)->name }}</td>
                                                <td>{{ optional($admin->user)->email }}</td>
                                                <td>{{ optional($admin->user)->created_at->format('d M, Y')}}</td>
                                                
                                                <td>
                                                      <a href="/petugas/{{optional($admin->user)->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/petugas/{{optional($admin->user)->id}}" method="POST" id="delete{{ $admin->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $admin->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="4" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection