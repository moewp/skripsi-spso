<tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $surats as $surat )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $surat->nm_surat }}</td>
                                                <td>{{ $surat->kd_surat }}</td>
                                                <td>{{ $surat->created_at->format('d M, Y')}}</td>
                                                
                                                <td>
                                                      <a href="/surat/{{$surat->id}}/edit" class="btn btn-info btn-sm">Edit</a>

                                                      <form class="d-inline" action="/surat/{{$surat->id}}" method="POST" id="delete{{ $surat->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $surat->id }})" type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="4" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Surat
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>