<!-- daftar -->
<div class="row">
      <!-- Modal -->
      <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                  <div class="modal-content">
                        <div class="modal-header">
                              <h5 class="modal-title text-bold">Detail</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                        <div class="modal-body">
                        {{-- Content --}}
                        </div>
                        <div class="modal-footer">
                              <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                        </div>
                  </div>
            </div>
      </div>
</div>

@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row">
                  <div class="col-sm-6 mt-3">
                        <h6>Nama: <h1>{{ $buats->user->warga->nm_warga }}</h1></h6>
                  </div>
            </div>
            <div class="row">
                  <div class="col-sm-6 mt-3">
                        <h6>Nama: <h1>{{ $buats->user->warga->nm_warga }}</h1></h6>
                  </div>
            </div>
      </div>
</section>

<!-- START -->
<div class="container-fluid">
      <div class="col-md">
            <div class="card">
                  <div class="card-header">
                        <h3 class="card-title">
                              <i class="fas fa-info"></i> Detail Pembuatan Surat
                        </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                        <dl>
                              <dt>Description lists</dt>
                              <dd>A description list is perfect for defining terms.</dd>
                              <dt>Euismod</dt>
                              <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                              <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                              <dt>Malesuada porta</dt>
                              <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                        </dl>
                  </div>
                  <!-- /.card-body -->
            </div>
            <!-- /.card -->
      </div>
</div>
@endsection