@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Pasangan</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Warga -->
                  <div class="card-header">
                        <h3 class="card-title">Isi Data Pasanganmu dengan Benar!</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/sutri') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::id()}}">
                  
                        <div class="card-body">
                              <!-- Nama Lengkap -->
                              <div class="form-group row">
                                    <label for="nm_sutri" class="col-sm-2 col-form-label">Nama Pasangan</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('nm_sutri') }}" class="form-control @error ('nm_sutri') is-invalid @enderror" id="nm_sutri" placeholder="Nama Lengkap Pasangan" name="nm_sutri" required>
                                          @error('nm_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- NIK BIN -->
                              <div class="form-group row">
                                    <!-- NIK -->
                                    <label for="nik_sutri" class="col-sm-2 col-form-label">NIK Pasangan</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik_sutri') }}" class="form-control @error ('nik_sutri') is-invalid @enderror" id="nik_sutri" placeholder="NIK" name="nik_sutri" required>
                                          @error('nik_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- BIN -->
                                    <label for="bin_sutri" class="col-sm-2 col-form-label">Bin Pasangan</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="{{ old('bin_sutri') }}" class="form-control @error ('bin_sutri') is-invalid @enderror" id="bin_sutri" placeholder="Bin dari Pasangan" name="bin_sutri" required>
                                          @error('bin_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              
                              <!-- TTL -->
                              <div class="form-group row">
                                    <!-- Tempat Lahir -->
                                    <label for="tplahir_sutri" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('tplahir_sutri') }}" class="form-control @error ('tplahir_sutri') is-invalid @enderror" id="tplahir_sutri" name="tplahir_sutri" placeholder="Tempat Lahir" required>
                                          @error('tplahir_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Tanggal Lahir -->
                                    <label for="tglahir_sutri" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="date" class="form-control @error ('tglahir_sutri') is-invalid @enderror"  name="tglahir_sutri" id="tglahir_sutri" placeholder="Tanggal Lahir" required/>
                                          @error('tglahir_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Kewarganegaraan -->
                              <div class="form-group row">
                                    <label for="kwn_sutri" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="Indonesia" class="form-control @error ('kwn_sutri') is-invalid @enderror" name="kwn_sutri" id="kwn_sutri" placeholder="Kewarganegaraan" required>
                                          @error('kwn_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Pekerjaan -->
                                    <label for="nm_pkj" class="col-sm-2 col-form-label">Pekerjaan Pasangan</label>
                                    <div class="col-sm-3">
                                          <select name="nm_pkj" required class="form-control @error ('nm_pkj') is-invalid @enderror" {{ count($nm_pkj) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_pkj) == 0)						
                                                      <option>Pilihan tidak ada</option>
                                                @else										
                                                      <option value="">--Silakan Pilih--</option>			
                                                      @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                      @endforeach
                                                @endif
                                                @error('nm_pkj')
                                                      <p class="text-danger">{{ $message }}</p>
                                                @enderror	
                                          </select>
                                    </div>
                              </div>

                              <!-- JK Agama -->
                              <div class="form-group row">
                                    <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-3">
                                          <select class="form-control @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="">--Silakan Pilih--</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <label for="nm_agama" class="col-sm-2 col-form-label">Agama Pasangan</label>
                                    <div class="col-sm-3">
                                          <select name="nm_agama" required class="form-control @error('nm_agama') is-invalid @enderror" {{ count($nm_agama) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_agama) == 0)							
                                                      <option>Pilihan tidak ada</option>
                                                @else											
                                                      <option value="">--Silakan Pilih--</option>				
                                                      @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                      @endforeach
                                                @endif	
                                          </select>
                                          <span class="text-danger">@error('nm_agama') {{ $message }} @enderror</span>
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat_sutri" class="col-sm-2 col-form-label">Alamat Pasangan</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat_sutri') }}" class="form-control @error ('alamat_sutri') is-invalid @enderror" id="alamat_sutri" placeholder="Alamat" name="alamat_sutri" required>
                                          @error('alamat_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt_sutri" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_sutri') }}" class="form-control @error ('rt_sutri') is-invalid @enderror" id="rt_sutri" placeholder="contoh: 012" name="rt_sutri" required>
                                          @error('rt_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw_sutri" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_sutri') }}" class="form-control @error ('rw_sutri') is-invalid @enderror" id="rw_sutri" placeholder="contoh: 005" name="rw_sutri" required>
                                          @error('rw_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <!-- Tanggal Menikah -->
                              <div class="form-group row">
                                    <label for="tgl_nkh" class="col-sm-2 col-form-label">Tanggal Menikah</label>
                                    <div class="col-sm-3">
                                          <input type="date" class="form-control @error ('tgl_nkh') is-invalid @enderror"  name="tgl_nkh" id="tgl_nkh" placeholder="Tanggal Menikah" required/>
                                          @error('tgl_nkh')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <hr>
                              <!-- KK -->
                              <div class="form-group row">
                                    <label for="file_kkps" class="col-sm-2 col-form-label">KK</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_kkps') is-invalid @enderror" id="file_kkps" name="file_kkps" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_kkps')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file_ktpps" class="col-sm-2 col-form-label">KTP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_ktpps') is-invalid @enderror" id="file_ktpps" name="file_ktpps" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_ktpps')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- Buku Nikah -->
                              <div class="form-group row">
                                    <label for="file_bn" class="col-sm-2 col-form-label">Buku Nikah</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_bn') is-invalid @enderror" id="file_bn" name="file_bn" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_bn')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <hr>
                              <div class="col mb-2 mt-4">Tambahan</div>
                              <!-- TTM -->
                              <div class="form-group row">
                                    <!-- Tempat Meninggal -->
                                    <label for="tpmgl_sutri" class="col-sm-2 col-form-label">Tempat Meninggal (Opsional)</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('tpmgl_sutri') }}" class="form-control @error ('tpmgl_sutri') is-invalid @enderror" id="tpmgl_sutri" name="tpmgl_sutri" placeholder="Tempat Meninggal">
                                          @error('tpmgl_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Tanggal Meninggal -->
                                    <label for="tgmgl_sutri" class="col-sm-2 col-form-label">Tanggal Meninggal (Opsional)</label>
                                    <div class="col-sm-3">
                                          <input type="date" class="form-control @error ('tgmgl_sutri') is-invalid @enderror"  name="tgmgl_sutri" id="tgmgl_sutri" placeholder="Tanggal Meninggal"/>
                                          @error('tgmgl_sutri')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              <!-- SK. Kematian -->
                              <div class="form-group row">
                                    <label for="skmt" class="col-sm-2 col-form-label">SK. Kematian (Opsional)</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('skmt') is-invalid @enderror" id="skmt" name="skmt" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('skmt')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                  </form>
            </div>
      </div>
</div>

@endsection