@extends('partials.master')

@section('content')

<div class="row mb-4"></div>
<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Profile -->
                  <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Data Pasangan</h3>
                  </div>
                  <form method="POST" action="/sutri/{{ $sutri->id }}" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                        <div class="card-body">
                              <!-- Nama Pasangan -->
                              <div class="form-group">
                                    <label for="nm_sutri">Nama Pasangan</label>
                                    <input type="text" value="{{ old('nm_sutri', $sutri->nm_sutri) }}" class="form-control @error('nm_sutri') is-invalid @enderror" name="nm_sutri" id="nm_sutri" required>
                                    @error('nm_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- NIK -->
                              <div class="form-group">
                                    <label for="nik_sutri">NIK</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik_sutri', $sutri->nik_sutri) }}" class="form-control @error('nik_sutri') is-invalid @enderror" name="nik_sutri" id="nik_sutri" required>
                                    @error('nik_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- BIN -->
                              <div class="form-group">
                                    <label for="bin_sutri">Bin dari Pasangan</label>
                                    <input type ="text" maxlength="16" value="{{ old('bin_sutri', $sutri->bin_sutri) }}" class="form-control @error('bin_sutri') is-invalid @enderror" name="bin_sutri" id="bin_sutri" required>
                                    @error('bin_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir -->
                              <div class="form-group">
                                    <label for="tplahir_sutri">Tempat Lahir</label>
                                    <input type="text" value="{{ old('tplahir_sutri', $sutri->tplahir_sutri) }}" class="form-control @error('tplahir_sutri') is-invalid @enderror" name="tplahir_sutri" id="tplahir_sutri" required>
                                    @error('tplahir_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir -->
                              <div class="form-group">
                                    <label for="tglahir_sutri">Tanggal Lahir</label>
                                    <input class="form-control col-sm-2 @error('tglahir_sutri') is-invalid @enderror" name="tglahir_sutri" id="tglahir_sutri" value="{{ old('tglahir_sutri', $sutri->tglahir_sutri->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tglahir_sutri', $sutri->tglahir_sutri) }}" class="form-control @error('tglahir_sutri') is-invalid @enderror" name="tglahir_sutri" id="tglahir_sutri" required>
                                    @error('tglahir_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Kewarganegaraan -->
                              <div class="form-group">
                                    <label for="kwn_sutri">Kewarganegaraan</label>
                                    <input type="text" value="{{ old('kwn_sutri', $sutri->kwn_sutri) }}" class="form-control @error('kwn_sutri') is-invalid @enderror" name="kwn_sutri" id="kwn_sutri" required>
                                    @error('kwn_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Pekerjaan -->
                              <div class="form-group">
                                    <label for="nm_pkj">Pekerjaan</label>
                                    <select class="form-control @error ('nm_pkj') is-invalid @enderror" id="nm_pkj" name="nm_pkj" required>
                                                <option value="{{ old('nm_pkj', $sutri->pekerjaan->id) }}">{{ old('nm_pkj', $sutri->pekerjaan->nm_pkj) }}</option>
                                                @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_pkj')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Agama -->
                              <div class="form-group">
                                    <label for="nm_agama">Agama</label>
                                    <select class="form-control @error ('nm_agama') is-invalid @enderror" id="nm_agama" name="nm_agama" required>
                                                <option value="{{ old('nm_agama', $sutri->agama->id) }}">{{ old('nm_agama', $sutri->agama->nm_agama) }}</option>
                                                @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_agama')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat -->
                              <div class="form-group">
                                    <label for="alamat_sutri">Alamat Pasangan</label>
                                    <input type="text" value="{{ old('alamat_sutri', $sutri->alamat_sutri) }}" class="form-control @error('alamat_sutri') is-invalid @enderror" name="alamat_sutri" id="alamat_sutri" required>
                                    @error('alamat_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT -->
                              <div class="form-group">
                                    <label for="rt_sutri">RT</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_sutri', $sutri->rt_sutri) }}" class="form-control @error('rt_sutri') is-invalid @enderror" name="rt_sutri" id="rt_sutri" maxlength="3" required>
                                    @error('rt_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW -->
                              <div class="form-group">
                                    <label for="rw_sutri">RW</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_sutri', $sutri->rw_sutri) }}" class="form-control @error('rw_sutri') is-invalid @enderror" name="rw_sutri" id="rw_sutri" maxlength="3" required>
                                    @error('rw_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <hr><hr>
                              <!-- Tanggal Menikah -->
                              <div class="form-group">
                                    <label for="tgl_nkh">Tanggal Menikah</label>
                                    <input class="form-control col-sm-2 @error('tgl_nkh') is-invalid @enderror" name="tgl_nkh" id="tgl_nkh" value="{{ old('tgl_nkh', $sutri->tgl_nkh->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tgl_nkh', $sutri->tgl_nkh) }}" class="form-control @error('tgl_nkh') is-invalid @enderror" name="tgl_nkh" id="tgl_nkh" required>
                                    @error('tgl_nkh')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Meninggal -->
                              <div class="form-group">
                                    <label for="tpmgl_sutri">Tempat Meninggal</label>
                              @if (Auth::user()->sutri->tpmgl_sutri != NULL)
                                    <input type="text" value="{{ old('tpmgl_sutri', $sutri->tpmgl_sutri) }}" class="form-control @error('tpmgl_sutri') is-invalid @enderror" name="tpmgl_sutri" id="tpmgl_sutri">
                                    @error('tpmgl_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              @else
                                    <input type="text" class="form-control @error ('tpmgl_sutri') is-invalid @enderror"  name="tpmgl_sutri" id="tpmgl_sutri" placeholder="Tanggal Meninggal"/>
                                    @error('tpmgl_sutri')
                                          <p class="text-danger">{{ $message }}</p>
                                    @enderror
                              @endif
                              </div>
                              <!-- Tanggal Meninggal -->
                              <div class="form-group">
                                    <label for="tgmgl_sutri">Tanggal Meninggal</label>
                              @if (Auth::user()->sutri->tgmgl_sutri != NULL)
                                    <input class="form-control col-sm-2 @error('tgmgl_sutri') is-invalid @enderror" name="tgmgl_sutri" id="tgmgl_sutri" value="{{ old('tgmgl_sutri', $sutri->tgmgl_sutri->format('d M Y')) }}">
                                    <input type="date" value="{{ old('tgmgl_sutri', $sutri->tgmgl_sutri) }}" class="form-control @error('tgmgl_sutri') is-invalid @enderror" name="tgmgl_sutri" id="tgmgl_sutri">
                                    @error('tgmgl_sutri')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              @else
                                    <input type="date" class="form-control @error ('tgmgl_sutri') is-invalid @enderror"  name="tgmgl_sutri" id="tgmgl_sutri" placeholder="Tanggal Meninggal"/>
                                    @error('tgmgl_sutri')
                                          <p class="text-danger">{{ $message }}</p>
                                    @enderror
                              @endif
                              </div>
                              <hr>
                              <!-- KK -->
                              <div class="form-group row mt-5">
                                    <label for="file_kkps" class="col-md-3 col-form-label text-md-right">KK</label>
                                    <div class="col-md-9">
                                          <img src="/images/kkps/{{ $sutri->file_kkps }}" width="70px">
                                          File Name : {{ $sutri->file_kkps }}
                                          <input id="file_kkps" type="file" class="form-control" name="file_kkps" autofocus>
                                          @error('file_kkps')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- KTP -->
                              <div class="form-group row mt-5">
                                    <label for="file_ktpps" class="col-md-3 col-form-label text-md-right">KTP</label>
                                    <div class="col-md-9">
                                          <img src="/images/ktpps/{{ $sutri->file_ktpps }}" width="70px">
                                          File Name : {{ $sutri->file_ktpps }}
                                          <input id="file_ktpps" type="file" class="form-control" name="file_ktpps" autofocus>
                                          @error('file_ktpps')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- Buku Nikah -->
                              <div class="form-group row mt-5">
                                    <label for="file_bn" class="col-md-3 col-form-label text-md-right">Buku Nikah</label>
                                    <div class="col-md-9">
                                          <img src="/images/bn/{{ $sutri->file_bn }}" width="70px">
                                          File Name : {{ $sutri->file_bn }}
                                          <input id="file_bn" type="file" class="form-control" name="file_bn" autofocus>
                                          @error('file_bn')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- SK. Kematian -->
                              <div class="form-group row mt-5">
                                    <label for="file_skmt" class="col-md-3 col-form-label text-md-right">SK. Kematian</label>
                                    <div class="col-md-9">
                                          <img src="/images/skmt/{{ $sutri->file_skmt }}" width="70px">
                                          File Name : {{ $sutri->file_skmt }}
                                          <input id="file_skmt" type="file" class="form-control" name="file_skmt" autofocus>
                                          @error('file_skmt')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/sutri/{{$sutri->id}}">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection