@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="col-sm-6">
                  @if (Auth::user()->sutri != NULL)
                  
                  @else
                  <a href="{{ url('sutri/create') }}" class="btn btn-outline-primary btn-sm btn-rounded">
                        <i class="mdi mdi-plus-circle"></i> {{ __('Tambah Data') }}
                  </a>
                  @endif
            </div>
      </div>
</section>

<div class="container-fluid">
      <div class="form-group row">
            <div class="col-6 ml-3">
                  @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                  @endif
                  <div class="card">
                        <div class="card-header bg-secondary">
                              <h3 class="card-title text-white">Data Pasangan</h3>
                        </div>
                        @if (Auth::user()->sutri != NULL)
                        <div class="card-body">
                              <h3 class="card-title text-muted mr-2">NIK: </h3>
                              <h3 class="card-title mr-5">{{ $sutri->nik_sutri }}</h3>
                              <h3 class="card-text"><strong>{{ $sutri->nm_sutri }}</strong></h3>
                              @if (Auth::user()->sutri->jk != 'Laki-Laki')
                              <p class="card-title">Binti, {{ $sutri->bin_sutri }}</p>
                              @else
                              <p class="card-title">Bin, {{ $sutri->bin_sutri }}</p>
                              @endif
                              <p class="card-text">{{ $sutri->tplahir_sutri }}, {{ $sutri->tglahir_sutri->format('d M Y') }}</p>
                              <hr>
                              <h3 class="card-title text-muted mr-2">agama: </h3>
                              <h3 class="card-title mr-5">{{ $sutri->agama->nm_agama }}</h3>
                              <h3 class="card-title text-muted mr-2">pekerjaan: </h3>
                              <h3 class="card-title">{{ $sutri->pekerjaan->nm_pkj }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">kewarganegaraan: </h3>
                              <h3 class="card-title">{{ $sutri->kwn_sutri }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">alamat: </h3>
                              <h3 class="card-title">{{ $sutri->alamat_sutri }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">rt: </h3>
                              <h3 class="card-title mr-5">{{ $sutri->rt_sutri }}</h3>
                              <h3 class="card-title text-muted mr-2">rw: </h3>
                              <h3 class="card-title">{{ $sutri->rw_sutri }}</h3>
                              <p class="card-text"></p>
                              <hr>
                              <h3 class="card-title text-muted mr-2">tanggal menikah: </h3>
                              <h3 class="card-title">{{ $sutri->tgl_nkh->format('d M Y') }}</h3>
                              <p class="card-text"></p>
                              <hr>
                              <h3 class="card-title text-muted mr-2">tempat meninggal: </h3>
                              @if (Auth::user()->sutri->tpmgl_sutri != NULL)
                              <h3 class="card-title">{{ $sutri->tpmgl_sutri }}</h3>
                              @else
                              <h3 class="card-title">tidak ada data.</h3>
                              @endif
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">tanggal meninggal: </h3>
                              @if (Auth::user()->sutri->tgmgl_sutri != NULL)
                              <h3 class="card-title">{{ $sutri->tgmgl_sutri->format('d M Y') }}</h3>
                              @else
                              <h3 class="card-title">tidak ada data.</h3>
                              @endif
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                              <a href="/sutri/{{$sutri->id}}/edit" class="btn btn-dark mr-2">edit</a>
                              <!-- <a class="btn btn-light shadow-sm " href="/choose">Batal</a> -->
                        </div>
                        @else
                        <h5 class="card-text text-center"><strong>Tidak ada data</strong></h5>
                        <p class="card-text text-center">Silakan pilih tambah data</p>
                        @endif
                        <!-- /.card-footer-->
                  </div>
                  <div class="col-12">
                        <div class="card mt-md-5">
                        </div>
                  </div>
            </div>
      </div>
</div>

@endsection