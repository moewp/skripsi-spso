@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Data Majlis Taklim</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Warga -->
                  <div class="card-header">
                        <h3 class="card-title">Isi Data Majlis Taklimmu dengan Benar!</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/mjt') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::id()}}">
                  
                        <div class="card-body">
                              <!-- Nama Majlis Taklim -->
                              <div class="form-group row">
                                    <label for="nm_mt" class="col-sm-2 col-form-label">Nama Majlis Taklim</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('nm_mt') }}" class="form-control @error ('nm_mt') is-invalid @enderror" id="nm_mt" placeholder="Nama Majlis Taklim" name="nm_mt" required>
                                          @error('nm_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat_mt" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat_mt') }}" class="form-control @error ('alamat_mt') is-invalid @enderror" id="alamat_mt" placeholder="Alamat" name="alamat_mt" required>
                                          @error('alamat_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <label for="rt_mt" class="col-sm-2 col-form-label">RT</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_mt') }}" class="form-control @error ('rt_mt') is-invalid @enderror" id="rt_mt" name="rt_mt" placeholder="RT" required>
                                          @error('rt_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- RW -->
                                    <label for="rw_mt" class="col-sm-2 col-form-label">RW</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_mt') }}" class="form-control @error ('rw_mt') is-invalid @enderror" id="rw_mt" name="rw_mt" placeholder="RW" required>
                                          @error('rw_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Akta SB -->
                              <div class="form-group row">
                                    <!-- Akta Majlis Taklim -->
                                    <label for="akta_mt" class="col-sm-2 col-form-label">Akta Majlis Taklim</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('akta_mt') }}" class="form-control @error ('akta_mt') is-invalid @enderror" id="akta_mt" name="akta_mt" placeholder="Akta Majlis Taklim" required>
                                          @error('akta_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Status Bangunan -->
                                    <label for="sb_mt" class="col-sm-2 col-form-label">Status Bangunan</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('sb_mt') }}" class="form-control @error ('sb_mt') is-invalid @enderror" id="sb_mt" name="sb_mt" placeholder="Status Bangunan" required>
                                          @error('sb_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Jenis -->
                              <div class="form-group row">
                                    <!-- Jenis Majlis Taklim -->
                                    <label for="jns_mt" class="col-sm-2 col-form-label">Jenis Majlis Taklim</label>
                                    <div class="col-sm-9" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('jns_mt') }}" class="form-control @error ('jns_mt') is-invalid @enderror" id="jns_mt" name="jns_mt" placeholder="Jenis Majlis Taklim" required>
                                          @error('jns_mt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                  </form>
            </div>
      </div>
</div>

@endsection