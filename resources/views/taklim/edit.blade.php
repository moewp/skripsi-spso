@extends('partials.master')

@section('content')

<div class="row mb-4"></div>
<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Profile -->
                  <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Data Majlis Taklim</h3>
                  </div>
                  <form method="POST" action="/mjt/{{ $taklim->id }}" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                        <div class="card-body">
                              <!-- Nama Majlis Taklim -->
                              <div class="form-group">
                                    <label for="nm_mt">Nama Majlis Taklim</label>
                                    <input type="text" value="{{ old('nm_mt', $taklim->nm_mt) }}" class="form-control @error('nm_mt') is-invalid @enderror" name="nm_mt" id="nm_mt" required>
                                    @error('nm_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat Majlis Taklim -->
                              <div class="form-group">
                                    <label for="alamat_mt">Alamat Majlis Taklim</label>
                                    <input type="text" value="{{ old('alamat_mt', $taklim->alamat_mt) }}" class="form-control @error('alamat_mt') is-invalid @enderror" name="alamat_mt" id="alamat_mt" required>
                                    @error('alamat_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT Majlis Taklim -->
                              <div class="form-group">
                                    <label for="rt_mt">RT Majlis Taklim</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt_mt', $taklim->rt_mt) }}" class="form-control @error('rt_mt') is-invalid @enderror" name="rt_mt" id="rt_mt" required>
                                    @error('rt_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW Majlis Taklim -->
                              <div class="form-group">
                                    <label for="rw_mt">RW Majlis Taklim</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw_mt', $taklim->rw_mt) }}" class="form-control @error('rw_mt') is-invalid @enderror" name="rw_mt" id="rw_mt" required>
                                    @error('rw_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jenis Majlis Taklim -->
                              <div class="form-group">
                                    <label for="jns_mt">Jenis Majlis Taklim</label>
                                    <input type="text" value="{{ old('jns_mt', $taklim->jns_mt) }}" class="form-control @error('jns_mt') is-invalid @enderror" name="jns_mt" id="jns_mt" required>
                                    @error('jns_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Akta Majlis Taklim -->
                              <div class="form-group">
                                    <label for="akta_mt">Akta Majlis Taklim</label>
                                    <input type="text" value="{{ old('akta_mt', $taklim->akta_mt) }}" class="form-control @error('akta_mt') is-invalid @enderror" name="akta_mt" id="akta_mt" required>
                                    @error('akta_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Status Bangunan -->
                              <div class="form-group">
                                    <label for="sb_mt">Status Bangunan</label>
                                    <input type="text" value="{{ old('sb_mt', $taklim->sb_mt) }}" class="form-control @error('sb_mt') is-invalid @enderror" name="sb_mt" id="sb_mt" required>
                                    @error('sb_mt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>                              

                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/mjt/{{$taklim->id}}">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection