@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="col-sm-6">
                  @if (Auth::user()->taklim != NULL)
                  
                  @else
                  <a href="{{ url('mjt/create') }}" class="btn btn-outline-primary btn-sm btn-rounded">
                        <i class="mdi mdi-plus-circle"></i> {{ __('Tambah Data') }}
                  </a>
                  @endif
            </div>
      </div>
</section>

<div class="container-fluid">
      <div class="form-group row">
            <div class="col-6 ml-3">
                  @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                  @endif
                  <div class="card">
                        <div class="card-header bg-secondary">
                              <h3 class="card-title text-white">Data Majlis Taklim</h3>
                        </div>
                        @if (Auth::user()->taklim != NULL)
                        <div class="card-body">
                              <h3 class="card-title text-muted mr-2">akta: </h3>
                              <h3 class="card-title mr-5">{{ $mjt->akta_mt }}</h3>
                              <h3 class="card-text"><strong>{{ $mjt->nm_mt }}</strong></h3>
                              <hr>
                              <h3 class="card-title text-muted mr-2">jenis majlis taklim: </h3>
                              <h3 class="card-title mr-5">{{ $mjt->jns_mt }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">status bangunan: </h3>
                              <h3 class="card-title mr-5">{{ $mjt->sb_mt }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">alamat: </h3>
                              <h3 class="card-title mr-5">{{ $mjt->alamat_mt }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">rt: </h3>
                              <h3 class="card-title mr-5">{{ $mjt->rt_mt }}</h3>
                              <h3 class="card-title text-muted mr-2">rw: </h3>
                              <h3 class="card-title">{{ $mjt->rw_mt }}</h3>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                              <a href="/mjt/{{$mjt->id}}/edit" class="btn btn-dark mr-2">edit</a>
                              <!-- <a class="btn btn-light shadow-sm " href="/choose">Batal</a> -->
                        </div>
                        @else
                        <h5 class="card-text text-center"><strong>Tidak ada data</strong></h5>
                        <p class="card-text text-center">Silakan pilih tambah data</p>
                        @endif
                        <!-- /.card-footer-->
                  </div>
                  <div class="col-12">
                        <div class="card mt-md-5">
                        </div>
                  </div>
            </div>
      </div>
</div>

@endsection