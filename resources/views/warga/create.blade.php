@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Profile Warga</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Tambah Warga -->
                  <div class="card-header">
                        <h3 class="card-title">Warga</h3>
                  </div>
                  
                  <form method="POST" action="{{ url('/warga') }}" enctype="multipart/form-data">
                  @csrf
                        <div class="card-body">
                              <!-- Nama Lengkap -->
                              <div class="form-group row">
                                    <label for="nm_warga" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('nm_warga') }}" class="form-control @error ('nm_warga') is-invalid @enderror" id="nm_warga" placeholder="Nama Lengkap" name="nm_warga" required>
                                          @error('nm_warga')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- NIK NO KK -->
                              <div class="form-group row">
                                    <!-- NIK -->
                                    <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik') }}" class="form-control @error ('nik') is-invalid @enderror" id="nik" placeholder="NIK" name="nik" required>
                                          @error('nik')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- NO KK -->
                                    <label for="nkk" class="col-sm-2 col-form-label">No.KK</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nkk') }}" class="form-control @error ('nkk') is-invalid @enderror" id="nkk" placeholder="No KK" name="nkk" required>
                                          @error('nkk')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>
                              
                              <!-- TTL -->
                              <div class="form-group row">
                                    <!-- Tempat Lahir -->
                                    <label for="tmp_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3" style="padding-bottom: 5px;">
                                          <input type="text" value="{{ old('tmp_lahir') }}" class="form-control @error ('tmp_lahir') is-invalid @enderror" id="tmp_lahir" name="tmp_lahir" placeholder="Tempat Lahir" required>
                                          @error('tmp_lahir')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Tanggal Lahir -->
                                    <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                          <input type="date" class="form-control @error ('tgl_lahir') is-invalid @enderror"  name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir" required/>
                                          @error('tgl_lahir')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- Jenis Kelamin -->
                              <div class="form-group row">
                                    <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-3">
                                          <select class="form-control @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="">Silakan Pilih</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Pendidikan -->
                                    <label for="nm_pdd" class="col-sm-2 col-form-label">Pendidikan Terakhir</label>
                                    <div class="col-sm-3">
                                          <select name="nm_pdd" required class="form-control @error('nm_pdd') is-invalid @enderror" {{ count($nm_pdd) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_pdd) == 0)						
                                                      <option>Pilihan tidak ada</option>
                                                @else										
                                                      <option value="" required autofocus>Silakan Pilih</option>			
                                                      @foreach($nm_pdd as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pdd }}</option>
                                                      @endforeach
                                                @endif	
                                          </select>
                                    </div>
                              </div>

                              <!-- Kewarganegaraan -->
                              <div class="form-group row">
                                    <label for="kwn" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                    <div class="col-sm-3">
                                          <input type="text" value="Indonesia" class="form-control @error ('kwn') is-invalid @enderror" name="kwn" id="kwn" placeholder="Kewarganegaraan" required>
                                          @error('kwn')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Pekerjaan -->
                                    <label for="nm_pkj" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-3">
                                          <select name="nm_pkj" required class="form-control @error ('nm_pkj') is-invalid @enderror" {{ count($nm_pkj) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_pkj) == 0)						
                                                      <option>Pilihan tidak ada</option>
                                                @else										
                                                      <option value="">Silakan Pilih</option>			
                                                      @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                      @endforeach
                                                @endif
                                                @error('nm_pkj')
                                                      <p class="text-danger">{{ $message }}</p>
                                                @enderror	
                                          </select>
                                    </div>
                              </div>

                              <!-- Agama -->
                              <div class="form-group row">
                                    <label for="nm_agama" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                          <select name="nm_agama" required class="form-control @error('nm_agama') is-invalid @enderror" {{ count($nm_agama) == 0 ? 'disabled' : '' }}>
                                                @if(count($nm_agama) == 0)							
                                                      <option>Pilihan tidak ada</option>
                                                @else											
                                                      <option value="">Silakan Pilih</option>				
                                                      @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                      @endforeach
                                                @endif	
                                          </select>
                                          <span class="text-danger">@error('nm_agama') {{ $message }} @enderror</span>
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <!-- Status Pernikahan -->
                                    <label for="status_nkh" class="col-sm-2 col-form-label">Status Pernikahan</label>
                                    <div class="col-sm-3">
                                          <select class="form-control @error ('status_nkh') is-invalid @enderror" id="status_nkh" name="status_nkh" required>
                                                <option value="">Silakan Pilih</option>
                                                <option value="Jejaka">Jejaka</option>
                                                <option value="Perawan">Perawan</option>
                                                <option value="Duda">Duda</option>
                                                <option value="Janda">Janda</option>
                                          </select>
                                    </div>
                              </div>

                              <!-- Alamat -->
                              <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                          <input type="text" value="{{ old('alamat') }}" class="form-control @error ('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat" required>
                                          @error('alamat')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <!-- RT RW -->
                              <div class="form-group row">
                                    <!-- RT -->
                                    <div class="col-sm-2"></div>
                                    <label for="rt" class="col-form-label">RT</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt') }}" class="form-control @error ('rt') is-invalid @enderror" id="rt" placeholder="contoh: 012" name="rt" required>
                                          @error('rt')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                                    <!-- RW -->
                                    <div class="col-sm-1"></div>                                    
                                    <label for="rw" class="col-form-label">RW</label>
                                    <div class="col-sm-3">
                                          <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw') }}" class="form-control @error ('rw') is-invalid @enderror" id="rw" placeholder="contoh: 005" name="rw" required>
                                          @error('rw')
                                                <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                    </div>
                              </div>

                              <hr>
                              <!-- Image -->
                              <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Profile Image</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('image') is-invalid @enderror" id="image" name="image" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- KK -->
                              <div class="form-group row">
                                    <label for="file_kk" class="col-sm-2 col-form-label">KK</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_kk') is-invalid @enderror" id="file_kk" name="file_kk" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_kk')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file_ktp" class="col-sm-2 col-form-label">KTP</label>
                                    <div class="custom-file col-sm-7">
                                          <input type="file" class="form-control custom-file-input @error ('file_ktp') is-invalid @enderror" id="file_ktp" name="file_ktp" required autofocus>
                                          <label class="custom-file-label">Pilih file</label>
                                          @error('file_ktp')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                  </form>
            </div>
      </div>
</div>

@endsection