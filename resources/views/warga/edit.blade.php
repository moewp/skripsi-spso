@extends('partials.master')

@section('content')

<div class="row mb-4"></div>
<div class="container">
      <div class="container">
            <div class="card">
                  <!-- Edit Profile -->
                  <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Profile</h3>
                  </div>
                  <form method="POST" action="/warga/{{ $warga->id }}" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                        <div class="card-body">
                              <!-- Nama Warga -->
                              <div class="form-group">
                                    <label for="nm_warga">Nama Lengkap</label>
                                    <input type="text" value="{{ old('nm_warga', $warga->nm_warga) }}" class="form-control @error('nm_warga') is-invalid @enderror" name="nm_warga" id="nm_warga" placeholder="Masukan nama surat" required>
                                    @error('nm_warga')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- NIK -->
                              <div class="form-group">
                                    <label for="nik">NIK</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nik', $warga->nik) }}" class="form-control @error('nik') is-invalid @enderror" name="nik" id="nik" placeholder="Masukan nama surat" required>
                                    @error('nik')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- NKK -->
                              <div class="form-group">
                                    <label for="nkk">No KK</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="16" value="{{ old('nkk', $warga->nkk) }}" class="form-control @error('nkk') is-invalid @enderror" name="nkk" id="nkk" placeholder="Masukan nama surat" required>
                                    @error('nkk')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tempat Lahir -->
                              <div class="form-group">
                                    <label for="tmp_lahir">Tempat Lahir</label>
                                    <input type="text" value="{{ old('tmp_lahir', $warga->tmp_lahir) }}" class="form-control @error('tmp_lahir') is-invalid @enderror" name="tmp_lahir" id="tmp_lahir" placeholder="Masukan nama surat" required>
                                    @error('tmp_lahir')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Tanggal Lahir -->
                              <div class="form-group">
                                    <label for="tgl_lahir">Tanggal Lahir</label>
                                    <input class="form-control col-sm-2 @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" id="tgl_lahir" value="{{ old('tgl_lahir', $warga->tgl_lahir->format('d M Y')) }}" required>
                                    <input type="date" value="{{ old('tgl_lahir', $warga->tgl_lahir) }}" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" id="tgl_lahir" required>
                                    @error('tgl_lahir')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Jenis Kelamin -->
                              <div class="form-group">
                                    <label for="jk">Jenis Kelamin</label>
                                    <select class="form-control @error ('jk') is-invalid @enderror" id="jk" name="jk" required>
                                                <option value="{{ old('jk', $warga->jk) }}">{{ old('jk', $warga->jk) }}</option>
                                                <option value="Laki-laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                    @error('jk')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Status Pernikahan -->
                              <div class="form-group">
                                    <label for="status_nkh">Status Pernikahan</label>
                                    <select class="form-control @error ('status_nkh') is-invalid @enderror" id="status_nkh" name="status_nkh" required>
                                                <option value="{{ old('status_nkh', $warga->status_nkh) }}">{{ old('status_nkh', $warga->status_nkh) }}</option>
                                                <option value="Jejaka">Jejaka</option>
                                                <option value="Perawan">Perawan</option>
                                                <option value="Duda">Duda</option>
                                                <option value="Janda">Janda</option>
                                          </select>
                                    @error('status_nkh')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Kewarganegaraan -->
                              <div class="form-group">
                                    <label for="kwn">Kewarganegaraan</label>
                                    <input type="text" value="{{ old('kwn', $warga->kwn) }}" class="form-control @error('kwn') is-invalid @enderror" name="kwn" id="kwn" required>
                                    @error('kwn')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Pekerjaan -->
                              <div class="form-group">
                                    <label for="nm_pkj">Pekerjaan</label>
                                    <select class="form-control @error ('nm_pkj') is-invalid @enderror" id="nm_pkj" name="nm_pkj" required>
                                                <option value="{{ old('nm_pkj', $warga->pekerjaan->id) }}">{{ old('nm_pkj', $warga->pekerjaan->nm_pkj) }}</option>
                                                @foreach($nm_pkj as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pkj }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_pkj')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Agama -->
                              <div class="form-group">
                                    <label for="nm_agama">Agama</label>
                                    <select class="form-control @error ('nm_agama') is-invalid @enderror" id="nm_agama" name="nm_agama" required>
                                                <option value="{{ old('nm_agama', $warga->agama->id) }}">{{ old('nm_agama', $warga->agama->nm_agama) }}</option>
                                                @foreach($nm_agama as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_agama }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_agama')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Pendidikan -->
                              <div class="form-group">
                                    <label for="nm_pdd">Pendidikan</label>
                                    <select class="form-control @error ('nm_pdd') is-invalid @enderror" id="nm_pdd" name="nm_pdd" required>
                                                <option value="{{ old('nm_pdd', $warga->pendidikan->id) }}">{{ old('nm_pdd', $warga->pendidikan->nm_pdd) }}</option>
                                                @foreach($nm_pdd as $value) 			
                                                            <option value="{{ $value->id }}">{{ $value->nm_pdd }}</option>
                                                @endforeach
                                          </select>
                                    @error('nm_pdd')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- Alamat -->
                              <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <input type="text" value="{{ old('alamat', $warga->alamat) }}" class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" required>
                                    @error('alamat')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RT -->
                              <div class="form-group">
                                    <label for="rt">RT</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rt', $warga->rt) }}" class="form-control @error('rt') is-invalid @enderror" name="rt" id="rt" maxlength="3" required>
                                    @error('rt')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <!-- RW -->
                              <div class="form-group">
                                    <label for="rw">RW</label>
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type ="number" maxlength="3" value="{{ old('rw', $warga->rw) }}" class="form-control @error('rw') is-invalid @enderror" name="rw" id="rw" maxlength="3" required>
                                    @error('rw')
                                          <p style= "color: red">{{ $message }}</p style= "color: red">
                                    @enderror
                              </div>
                              <hr>
                              <!-- Image -->
                              <div class="form-group row">
                                    <label for="image" class="col-md-3 col-form-label text-md-right">Profile Image</label>
                                    <div class="col-md-9">
                                          <img src="/images/{{ $warga->image }}" width="70px">
                                          File Name : {{ $warga->image }}
                                          <input id="image" type="file" class="form-control" name="image" autofocus>
                                          @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- KK -->
                              <div class="form-group row">
                                    <label for="file_kk" class="col-md-3 col-form-label text-md-right">KK</label>
                                    <div class="col-md-9">
                                          <img src="/images/kk/{{ $warga->file_kk }}" width="70px">
                                          File Name : {{ $warga->file_kk }}
                                          <input id="file_kk" type="file" class="form-control @error('file_kk') is-invalid @enderror" name="file_kk" autofocus>
                                          @error('file_kk')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>
                              <!-- KTP -->
                              <div class="form-group row">
                                    <label for="file_ktp" class="col-md-3 col-form-label text-md-right">KTP</label>
                                    <div class="col-md-9">
                                          <img src="/images/ktp/{{ $warga->file_ktp }}" width="70px">
                                          File Name : {{ $warga->file_ktp }}
                                          <input id="file_ktp" type="file" class="form-control @error('file_ktp') is-invalid @enderror" name="file_ktp" autofocus>
                                          @error('file_ktp')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                </span>
                                          @enderror
                                    </div>
                              </div>

                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                              <a class="btn btn-light shadow-sm" href="/warga/{{$warga->id}}">Batal</a>
                        </div>
                  </form>
            </div>

      </div>
</div>


@endsection