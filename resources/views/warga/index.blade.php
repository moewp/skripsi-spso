@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2">
                  <div class="col-sm-6">
                        <h1>Warga</h1>
                  </div>
            </div>
      </div>
</section>

<div class="container">
      <div class="container">
            <!-- Tabel index -->
            <div class="row">
                  <div class="col-12">
                        @if (session('add'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('add') }}
                        </div>
                        @endif
                        @if (session('delete'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('delete') }}
                        </div>
                        @endif
                        @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                        @endif
                        <div class="card card-primary">
                              <div class="card-header">
                              <h3 class="card-title">Data Warga</h3>
                              <div class="card-tools">
                                    <!-- SEARCH -->
                                    <!-- <div class="input-group input-group-sm" style="width: 150px;">
                                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                          <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                      <i class="fas fa-search"></i>
                                                </button>
                                          </div>
                                    </div> -->
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-bordered text-nowrap">
                                    <thead>
                                          <tr>
                                                <th style="width: 5px">#</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th>Tanggal Gabung</th>
                                                <th>Total Buat</th>
                                                <th>Aksi</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1; 
                                    @endphp
                                    @forelse( $wargas as $warga )
                                          <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $warga->nm_warga }}</td>
                                                <td>{{ $warga->nik }}</td>
                                                <td>{{ $warga->created_at->format('d M, Y')}}</td>
                                                <td>{{ $warga->kwn }}</td>
                                                <td>
                                                      <a href="/warga/{{$warga->id}}" class="btn btn-dark btn-xs">Show</a>
                                                      <a href="/warga/{{$warga->id}}/edit" class="btn btn-info btn-xs">Edit</a>
                                                      <form class="d-inline" action="/warga/{{$warga->id}}" method="POST" id="delete{{ $warga->id }}">
                                                      @csrf
                                                      @method('delete')
                                                      <button onclick="deleteData({{ $warga->id }})" type="submit" class="btn btn-danger btn-xs">Delete</button>
                                                      </form>
                                                </td>
                                          </tr>
                                          @empty
                                          <tr>
                                                <td colspan="6" style="text-align: center" class="text-primary">
                                                      Tidak Ada Data Warga
                                                </td>
                                          </tr>
                                    @endforelse
                                    </tbody>
                              </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                  </div>
            </div>

      </div>
</div>


@endsection