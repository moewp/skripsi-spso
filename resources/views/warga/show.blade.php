@extends('partials.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
            <div class="col-sm-6">
                  
            </div>
      </div>
</section>

<div class="container-fluid ml-3">
      <div class="form-group row">
            <div class="col-7">
                  @if (session('edit'))
                        <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fas fa-check"></i>
                              {{ session('edit') }}
                        </div>
                  @endif
                  <div class="card">
                        <div class="card-header bg-info">
                              <h3 class="card-title text-white">Profil</h3>
                        </div>
                        <div class="card-body">
                              <h3 class="card-title text-muted mr-2">NIK: </h3>
                              <h3 class="card-title mr-5">{{ $warga->nik }}</h3>
                              <h3 class="card-title text-muted mr-2">No KK: </h3>
                              <h3 class="card-title mb-3">{{ $warga->nkk }}</h3>
                              <h3 class="card-text"><strong>{{ $warga->nm_warga }}</strong></h3>
                              <p class="card-title">{{ $warga->jk }}</p>
                              <p class="card-text">{{ $warga->tmp_lahir }}, {{ $warga->tgl_lahir->format('d M Y') }}</p>
                              <hr>
                              <h3 class="card-title text-muted mr-2">agama: </h3>
                              <h3 class="card-title mr-5">{{ $warga->agama->nm_agama }}</h3>
                              <h3 class="card-title text-muted mr-2">pekerjaan: </h3>
                              <h3 class="card-title">{{ $warga->pekerjaan->nm_pkj }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">status: </h3>
                              <h3 class="card-title mr-5">{{ $warga->status_nkh }}</h3>
                              <h3 class="card-title text-muted mr-2">pendidikan: </h3>
                              <h3 class="card-title">{{ $warga->pendidikan->nm_pdd }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">kewarganegaraan: </h3>
                              <h3 class="card-title">{{ $warga->kwn }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">alamat: </h3>
                              <h3 class="card-title">{{ $warga->alamat }}</h3>
                              <p class="card-text"></p>
                              <h3 class="card-title text-muted mr-2">rt: </h3>
                              <h3 class="card-title mr-5">{{ $warga->rt }}</h3>
                              <h3 class="card-title text-muted mr-2">rw: </h3>
                              <h3 class="card-title">{{ $warga->rw }}</h3>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                              <a href="/warga/{{$warga->id}}/edit" class="btn btn-dark mr-2">edit</a>
                              <!-- <a class="btn btn-light shadow-sm " href="/choose">Batal</a> -->
                        </div>
                        <!-- /.card-footer-->
                        <div class="card mt-md-3">
                        <div class="card-footer bg-light">
                              <strong>Lainnya</strong>
                        </div>
                        <div class="card-body">
                              @if (Auth::user()->ibu != NULL)
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('ibu/'.Auth::user()->ibu->id) }}">Ibu</a>
                              @else
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('ibu/create') }}">Ibu</a>
                              @endif

                              @if (Auth::user()->ayah != NULL)
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('ayah/'.Auth::user()->ayah->id) }}">Ayah</a>
                              @else
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('ayah/create') }}">Ayah</a>
                              @endif

                              @if (Auth::user()->sutri != NULL)
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('sutri/'.Auth::user()->sutri->id) }}">Pasangan</a>
                              @else
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('sutri/create') }}">Pasangan</a>
                              @endif

                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('anak') }}">Anak</a>

                              @if (Auth::user()->perusahaan != NULL)
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('psh/'.Auth::user()->perusahaan->id) }}">Perusahaan</a>
                              @else
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('psh/create') }}">Perusahaan</a>
                              @endif
                              
                              @if (Auth::user()->taklim != NULL)
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('mjt/'.Auth::user()->taklim->id) }}">Majlis Taklim</a>
                              @else
                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('mjt/create') }}">Majlis Taklim</a>
                              @endif

                              <a class="btn btn-outline-info btn-sm mr-1 " href="{{ url('kmt') }}">Kematian</a>
                        </div>                  
                  </div>
                  </div>
                  <div class="col-6">
                  
            </div>
            </div>
            
      </div>
      <div class="form-group row">
            <div class="col-3 bg-info">
                  <div class="card mt-md-3">
                        <div class="card-footer">
                              
                        </div>
                        <div class="card-body">
                              <img src="/images/{{ $warga->image }}" width="100%px">
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer text-center" style="color: black;">
                        Foto Profil
                        </div>
                        <!-- /.card-footer-->
                  </div>
            </div>
            <div class="col-3 bg-info">
                  <div class="card mt-md-3">
                        <div class="card-footer">
                              
                        </div>
                        <div class="card-body">
                              <img src="/images/ktp/{{ $warga->file_ktp }}" width="100%px" alt="Tidak ada data file KTP">
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer text-center" style="color: black;">
                        <a href="{{ asset('images/ktp/'. $warga->file_ktp ) }}" target="_blank" rel="noopener noreferrer">KTP</a>
                        </div>
                        <!-- /.card-footer-->
                  </div>
            </div>
            <div class="col-3 bg-info">
                  <div class="card mt-md-3">
                        <div class="card-footer">
                              
                        </div>
                        <div class="card-body">
                              <img src="/images/kk/{{ $warga->file_kk }}" width="100%px">
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer text-center" style="color: black;">
                        <a href="{{ asset('images/kk/'. $warga->file_kk ) }}" target="_blank" rel="noopener noreferrer">KK</a>
                        </div>
                        <!-- /.card-footer-->
                  </div>
            </div>
      </div>
</div>



@endsection