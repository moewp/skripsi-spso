<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/landing', 'PagesController@landing');
Route::get('/dashboard', 'PagesController@dashboard');
Route::get('/dashboarda', 'PagesController@dashboarda');
Route::get('/dashboardw', 'PagesController@dashboardw')->name('dashboardw');
Route::get('/choose', 'PagesController@choose')->name('pilihSurat')->middleware('auth');

// SURAT
Route::resource('/surat', 'SuratController');
//WARGA
Route::resource('/warga', 'WargaController');
Route::get('/indexwarga', 'WargaController@indexwarga')->name('indexWarga');
//AGAMA
Route::resource('/agama', 'AgamaController');
//PEKERJAAN
Route::resource('/pkj', 'PekerjaanController');
//PENDIDIKAN
Route::resource('/pdd', 'PendidikanController');
//PETUGAS atau ADMIN
Route::resource('/petugas', 'PetugasController');
//IBU
Route::resource('/ibu', 'IbuController');
//AYAH
Route::resource('/ayah', 'AyahController');
//SUTRI
Route::resource('/sutri', 'SutriController');
//ANAK
Route::resource('/anak', 'AnakController');
//PERUSAHAAN
Route::resource('/psh', 'PerusahaanController');
//TAKLIM
Route::resource('/mjt', 'TaklimController');
//KEMATIAN
Route::resource('/kmt', 'KematianController');

//PEMBUATAN
//admin 
Route::post('/buat/{id}/verify', 'PembuatanController@verify')->name('verifyBuat');
Route::post('/buat/{id}/remove', 'PembuatanController@remove')->name('removeBuat');
Route::get('/pending', 'PembuatanController@indexPending')->name('indexPending');
//complete
Route::get('/complete/skbd', 'PembuatanController@completeSkbd')->name('completeSkbd');
Route::get('/complete/pktp', 'PembuatanController@completePktp')->name('completePktp');
Route::get('/complete/skl', 'PembuatanController@completeSkl')->name('completeSkl');
Route::get('/complete/sksm', 'PembuatanController@completeSksm')->name('completeSksm');
Route::get('/complete/skksi', 'PembuatanController@completeSkksi')->name('completeSkksi');
Route::get('/complete/skk', 'PembuatanController@completeSkk')->name('completeSkk');
Route::get('/complete/skbm', 'PembuatanController@completeSkbm')->name('completeSkbm');
Route::get('/complete/skd', 'PembuatanController@completeSkd')->name('completeSkd');
Route::get('/complete/spp', 'PembuatanController@completeSpp')->name('completeSpp');
Route::get('/complete/sku', 'PembuatanController@completeSku')->name('completeSku');
Route::get('/complete/skdu', 'PembuatanController@completeSkdu')->name('completeSkdu');
Route::get('/complete/skdmt', 'PembuatanController@completeSkdmt')->name('completeSkdmt');
Route::get('/complete/sktm', 'PembuatanController@completeSktm')->name('completeSktm');
//cetak surat
Route::get('/buat/cetak/skbd/{id}', 'PembuatanController@cetakSkbd')->name('cetakSkbd');
Route::get('/buat/cetak/pktp/{id}', 'PembuatanController@cetakPktp')->name('cetakPktp');
Route::get('/buat/cetak/skl/{id}', 'PembuatanController@cetakSkl')->name('cetakSkl');
Route::get('/buat/cetak/sksm/{id}', 'PembuatanController@cetakSksm')->name('cetakSksm');
Route::get('/buat/cetak/skksi/{id}', 'PembuatanController@cetakSkksi')->name('cetakSkksi');
Route::get('/buat/cetak/skk/{id}', 'PembuatanController@cetakSkk')->name('cetakSkk');
Route::get('/buat/cetak/skbm/{id}', 'PembuatanController@cetakSkbm')->name('cetakSkbm');
Route::get('/buat/cetak/skd/{id}', 'PembuatanController@cetakSkd')->name('cetakSkd');
Route::get('/buat/cetak/spp/{id}', 'PembuatanController@cetakSpp')->name('cetakSpp');
Route::get('/buat/cetak/sku/{id}', 'PembuatanController@cetakSku')->name('cetakSku');
Route::get('/buat/cetak/skdu/{id}', 'PembuatanController@cetakSkdu')->name('cetakSkdu');
Route::get('/buat/cetak/skdmt/{id}', 'PembuatanController@cetakSkdmt')->name('cetakSkdmt');
Route::get('/buat/cetak/sktm/{id}', 'PembuatanController@cetakSktm')->name('cetakSktm');
//cetak laporan
Route::get('/cetak', 'PembuatanController@completeCetak')->name('completeCetak');
//warga
Route::get('/buat/{id}', 'PembuatanController@getBuat')->name('getBuat');
Route::post('/buat/{id}/surat', 'PembuatanController@postBuat')->name('postBuat');
Route::get('/riwayat', 'PembuatanController@riwayatBuat')->name('riwayatBuat');
Route::get('/daftarbuat', 'PembuatanController@listBuat')->name('listBuat');
Route::get('/daftarbuat/{id}/detail', 'PembuatanController@detailBuat')->name('detailBuat');
Route::post('/daftarbuat/{id}/cancel', 'PembuatanController@cancelBuat')->name('cancelBuat');

Route::group(['middleware' => ['auth', 'role:Warga']], function () {
	//
});

Route::get('/tes', function () {
    return view('pdf.tostes');
});

//LAPORAN
Route::get('/laporan', 'LaporanController@index');
Route::get('/laporan/create', 'LaporanController@create');

Route::get('/lap-fpktp', function () {
    return view('laporan.fpktp');
});
Route::get('/lap-skbd', function () {
    return view('laporan.skbd');
});
Route::get('/lap-skbm', function () {
    return view('laporan.skbm');
});
Route::get('/lap-skd', function () {
    return view('laporan.skd');
});
Route::get('/lap-skdmt', function () {
    return view('laporan.skdmt');
});
Route::get('/lap-skdu', function () {
    return view('laporan.skdu');
});
Route::get('/lap-skk', function () {
    return view('laporan.skk');
});
Route::get('/lap-skksi', function () {
    return view('laporan.skksi');
});
Route::get('/lap-skl', function () {
    return view('laporan.skl');
});
Route::get('/lap-sksm', function () {
    return view('laporan.sksm');
});
Route::get('/lap-sktm', function () {
    return view('laporan.sktm');
});
Route::get('/lap-sku', function () {
    return view('laporan.sku');
});
Route::get('/lap-spp', function () {
    return view('laporan.spp');
});

// FORMS
Route::get('/fpktp', function () {
    return view('forms.f-permohonanKTP');
});

Route::get('/skk', function () {
    return view('forms.s-kematian');
});

Route::get('/skbm', function () {
    return view('forms.sk-belumMenikah');
});

Route::get('/skd', function () {
    return view('forms.sk-domisili');
});

Route::get('/skdmt', function () {
    return view('forms.sk-domisiliMT');
});

Route::get('/skdu', function () {
    return view('forms.sk-domisiliUsaha');
});

Route::get('/skl', function () {
    return view('forms.sk-kelahiran');
});

Route::get('/skksi', function () {
    return view('forms.sk-kematianSuamiOrIstri');
});

Route::get('/sksm', function () {
    return view('forms.sk-sudahMenikah');
});

Route::get('/sku', function () {
    return view('forms.sk-usaha');
});

Route::get('/skbd', function () {
    return view('forms.skbd');
});

Route::get('/sktm', function () {
    return view('forms.sktm');
});

Route::get('/spp', function () {
    return view('forms.sp-perkawinan');
});